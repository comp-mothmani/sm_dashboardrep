package com.nogroup.SMGis.data.daos ;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.nogroup.SMGis.data.utils.Logger;
import com.nogroup.SMGis.data.utils.HUtils;
import com.nogroup.SMGis.data.entities.KpiE;

public class KpiD{
    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public Integer create(KpiE e) {
	    /*
		 * Insert new Kpi record in the database.
		 */
		e.setDCreated(new Date());
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.save(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<Kpi> Successfully created " + e.toString());
		return e.getId();
	}

	public List<KpiE> read() {
		/*
		 * Select all Kpi records from the database.
		 */
		List<KpiE> tmps = new ArrayList<KpiE>();
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   tmps = session.createQuery("FROM KpiE").list();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<Kpi> Found " + tmps.size());
		return tmps;
	}

	public Long count() {
	    /*
		 * Return Kpi record count.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		Long count = (long) 0 ;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery(
			        "select count(*) from KpiE");
		   count = (Long)query.uniqueResult();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiE> Found " + count);
		return count;
	}


	public void delete(Integer id) {
		/*
		 * Delete Kpi record by id.
		 */
		KpiE e = findByID(id);

		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiE>Successfully deleted " + e.toString());
	}

	public KpiE findByID(Integer id) {
		/*
		 * Find Kpi by id.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   KpiE e = (KpiE) session.load(KpiE.class, id);

		   tx.commit();
		   return e ;
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}

		return null;
	}

	public void deleteAll() {
		/*
		 * Purge all Kpi records.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery("DELETE FROM KpiE ");
		   query.executeUpdate();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiE>Successfully deleted all records.");
	}

	public boolean checkName(String name) {
		/*
		 * Check if name exists.
		 */
		if (count() == 0) {
			return false ;
		}else {
			for(KpiE tmp : read()) {
				if(tmp.getName().equals(name)) {
					return true ;
				}
			}
			Logger.print("<KpiE>Successfully checked all records.");
			return false ;
		}

	}

	public void hard_update(KpiE e) {
		/*
		 * Update Kpi record.
		 */
		e.update() ;
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.update(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiE>Successfully updated " + e.toString());
	}

	public void delete(KpiE e) {
		/*
		 * Delete Kpi record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiE>Successfully deleted " + e.toString());
	}

	public KpiE findByName(String item) {
		/*
		 * Find Kpi record by name.
		 */
		for(KpiE tmp : read()) {
			if(tmp.getName().equals(item)) {
				return tmp ;
			}
		}
		Logger.print("<KpiE>Successfully searched all records.");
		return null ;
	}
}