package com.nogroup.SMGis.data.daos ;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.nogroup.SMGis.data.utils.Logger;
import com.nogroup.SMGis.data.utils.HUtils;
import com.nogroup.SMGis.data.entities.KpiValE;

public class KpiValD{
    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public Integer create(KpiValE e) {
	    /*
		 * Insert new KpiVal record in the database.
		 */
		e.setDCreated(new Date());
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.save(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiVal> Successfully created " + e.toString());
		return e.getId();
	}

	public List<KpiValE> read() {
		/*
		 * Select all KpiVal records from the database.
		 */
		List<KpiValE> tmps = new ArrayList<KpiValE>();
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   tmps = session.createQuery("FROM KpiValE").list();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiVal> Found " + tmps.size());
		return tmps;
	}

	public Long count() {
	    /*
		 * Return KpiVal record count.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		Long count = (long) 0 ;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery(
			        "select count(*) from KpiValE");
		   count = (Long)query.uniqueResult();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiValE> Found " + count);
		return count;
	}


	public void delete(Integer id) {
		/*
		 * Delete KpiVal record by id.
		 */
		KpiValE e = findByID(id);

		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiValE>Successfully deleted " + e.toString());
	}

	public KpiValE findByID(Integer id) {
		/*
		 * Find KpiVal by id.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   KpiValE e = (KpiValE) session.load(KpiValE.class, id);

		   tx.commit();
		   return e ;
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}

		return null;
	}

	public void deleteAll() {
		/*
		 * Purge all KpiVal records.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery("DELETE FROM KpiValE ");
		   query.executeUpdate();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiValE>Successfully deleted all records.");
	}

	public boolean checkName(String name) {
		/*
		 * Check if name exists.
		 */
		if (count() == 0) {
			return false ;
		}else {
			for(KpiValE tmp : read()) {
				if(tmp.getName().equals(name)) {
					return true ;
				}
			}
			Logger.print("<KpiValE>Successfully checked all records.");
			return false ;
		}

	}

	public void hard_update(KpiValE e) {
		/*
		 * Update KpiVal record.
		 */
		e.update() ;
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.update(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiValE>Successfully updated " + e.toString());
	}

	public void delete(KpiValE e) {
		/*
		 * Delete KpiVal record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<KpiValE>Successfully deleted " + e.toString());
	}

	public KpiValE findByName(String item) {
		/*
		 * Find KpiVal record by name.
		 */
		for(KpiValE tmp : read()) {
			if(tmp.getName().equals(item)) {
				return tmp ;
			}
		}
		Logger.print("<KpiValE>Successfully searched all records.");
		return null ;
	}
}