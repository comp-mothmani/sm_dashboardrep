package com.nogroup.SMGis.data.daos ;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.nogroup.SMGis.data.utils.Logger;
import com.nogroup.SMGis.data.utils.HUtils;
import com.nogroup.SMGis.data.entities.SimKpiValE;

public class SimKpiValD{
    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public Integer create(SimKpiValE e) {
	    /*
		 * Insert new SimKpiVal record in the database.
		 */
		e.setDCreated(new Date());
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.save(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiVal> Successfully created " + e.toString());
		return e.getId();
	}

	public List<SimKpiValE> read() {
		/*
		 * Select all SimKpiVal records from the database.
		 */
		List<SimKpiValE> tmps = new ArrayList<SimKpiValE>();
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   tmps = session.createQuery("FROM SimKpiValE").list();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiVal> Found " + tmps.size());
		return tmps;
	}

	public Long count() {
	    /*
		 * Return SimKpiVal record count.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		Long count = (long) 0 ;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery(
			        "select count(*) from SimKpiValE");
		   count = (Long)query.uniqueResult();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiValE> Found " + count);
		return count;
	}


	public void delete(Integer id) {
		/*
		 * Delete SimKpiVal record by id.
		 */
		SimKpiValE e = findByID(id);

		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiValE>Successfully deleted " + e.toString());
	}

	public SimKpiValE findByID(Integer id) {
		/*
		 * Find SimKpiVal by id.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   SimKpiValE e = (SimKpiValE) session.load(SimKpiValE.class, id);

		   tx.commit();
		   return e ;
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}

		return null;
	}

	public void deleteAll() {
		/*
		 * Purge all SimKpiVal records.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery("DELETE FROM SimKpiValE ");
		   query.executeUpdate();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiValE>Successfully deleted all records.");
	}

	public boolean checkName(String name) {
		/*
		 * Check if name exists.
		 */
		if (count() == 0) {
			return false ;
		}else {
			for(SimKpiValE tmp : read()) {
				if(tmp.getName().equals(name)) {
					return true ;
				}
			}
			Logger.print("<SimKpiValE>Successfully checked all records.");
			return false ;
		}

	}

	public void hard_update(SimKpiValE e) {
		/*
		 * Update SimKpiVal record.
		 */
		e.update() ;
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.update(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiValE>Successfully updated " + e.toString());
	}

	public void delete(SimKpiValE e) {
		/*
		 * Delete SimKpiVal record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<SimKpiValE>Successfully deleted " + e.toString());
	}

	public SimKpiValE findByName(String item) {
		/*
		 * Find SimKpiVal record by name.
		 */
		for(SimKpiValE tmp : read()) {
			if(tmp.getName().equals(item)) {
				return tmp ;
			}
		}
		Logger.print("<SimKpiValE>Successfully searched all records.");
		return null ;
	}
}