package com.nogroup.SMGis.data.daos ;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.nogroup.SMGis.data.utils.Logger;
import com.nogroup.SMGis.data.utils.HUtils;
import com.nogroup.SMGis.data.entities.CityE;

public class CityD{
    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public Integer create(CityE e) {
	    /*
		 * Insert new City record in the database.
		 */
		e.setDCreated(new Date());
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.save(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<City> Successfully created " + e.toString());
		return e.getId();
	}

	public List<CityE> read() {
		/*
		 * Select all City records from the database.
		 */
		List<CityE> tmps = new ArrayList<CityE>();
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   tmps = session.createQuery("FROM CityE").list();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<City> Found " + tmps.size());
		return tmps;
	}

	public Long count() {
	    /*
		 * Return City record count.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		Long count = (long) 0 ;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery(
			        "select count(*) from CityE");
		   count = (Long)query.uniqueResult();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<CityE> Found " + count);
		return count;
	}


	public void delete(Integer id) {
		/*
		 * Delete City record by id.
		 */
		CityE e = findByID(id);

		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<CityE>Successfully deleted " + e.toString());
	}

	public CityE findByID(Integer id) {
		/*
		 * Find City by id.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   CityE e = (CityE) session.load(CityE.class, id);

		   tx.commit();
		   return e ;
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}

		return null;
	}

	public void deleteAll() {
		/*
		 * Purge all City records.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   Query query = session.createQuery("DELETE FROM CityE ");
		   query.executeUpdate();

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<CityE>Successfully deleted all records.");
	}

	public boolean checkName(String name) {
		/*
		 * Check if name exists.
		 */
		if (count() == 0) {
			return false ;
		}else {
			for(CityE tmp : read()) {
				if(tmp.getName().equals(name)) {
					return true ;
				}
			}
			Logger.print("<CityE>Successfully checked all records.");
			return false ;
		}

	}

	public void hard_update(CityE e) {
		/*
		 * Update City record.
		 */
		e.update() ;
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.update(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<CityE>Successfully updated " + e.toString());
	}

	public void delete(CityE e) {
		/*
		 * Delete City record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession() ;
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
		   session.delete(e);

		   tx.commit();
		}
		catch (Exception ex) {
		   if (tx!=null) tx.rollback();
		   ex.printStackTrace();
		}
		Logger.print("<CityE>Successfully deleted " + e.toString());
	}

	public CityE findByName(String item) {
		/*
		 * Find City record by name.
		 */
		for(CityE tmp : read()) {
			if(tmp.getName().equals(item)) {
				return tmp ;
			}
		}
		Logger.print("<CityE>Successfully searched all records.");
		return null ;
	}
}