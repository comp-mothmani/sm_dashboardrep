package com.nogroup.SMGis.data.serialization.pojos.benchmark;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "city1_val", "city2_val", "name", "description", "date", "dimension", "subDimension", "category",
		"unit", "methodology", "source", "sdg" })
public class KpiSet implements Serializable {

	@JsonProperty("city1_val")
	private String city1Val;
	@JsonProperty("city2_val")
	private String city2Val;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("date")
	private String date;
	@JsonProperty("dimension")
	private String dimension;
	@JsonProperty("subDimension")
	private String subDimension;
	@JsonProperty("category")
	private String category;
	@JsonProperty("unit")
	private String unit;
	@JsonProperty("methodology")
	private String methodology;
	@JsonProperty("source")
	private String source;
	@JsonProperty("sdg")
	private String sdg;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 3520043091352269948L;

	@JsonProperty("city1_val")
	public String getCity1Val() {
		return city1Val;
	}

	@JsonProperty("city1_val")
	public void setCity1Val(String city1Val) {
		this.city1Val = city1Val;
	}

	@JsonProperty("city2_val")
	public String getCity2Val() {
		return city2Val;
	}

	@JsonProperty("city2_val")
	public void setCity2Val(String city2Val) {
		this.city2Val = city2Val;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("date")
	public String getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}

	@JsonProperty("dimension")
	public String getDimension() {
		return dimension;
	}

	@JsonProperty("dimension")
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	@JsonProperty("subDimension")
	public String getSubDimension() {
		return subDimension;
	}

	@JsonProperty("subDimension")
	public void setSubDimension(String subDimension) {
		this.subDimension = subDimension;
	}

	@JsonProperty("category")
	public String getCategory() {
		return category;
	}

	@JsonProperty("category")
	public void setCategory(String category) {
		this.category = category;
	}

	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("unit")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@JsonProperty("methodology")
	public String getMethodology() {
		return methodology;
	}

	@JsonProperty("methodology")
	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("sdg")
	public String getSdg() {
		return sdg;
	}

	@JsonProperty("sdg")
	public void setSdg(String sdg) {
		this.sdg = sdg;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}