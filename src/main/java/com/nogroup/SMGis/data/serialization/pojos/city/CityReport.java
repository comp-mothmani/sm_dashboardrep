package com.nogroup.SMGis.data.serialization.pojos.city;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "date", "reportName", "userName ", "qrCode", "logo", "cityName", "description", "kpiSet", "pSet",
		"chartsList" })
public class CityReport implements Serializable {

	@JsonProperty("date")
	private String date;
	@JsonProperty("reportName")
	private String reportName;
	@JsonProperty("userName ")
	private String userName;
	@JsonProperty("qrCode")
	private String qrCode;
	@JsonProperty("logo")
	private String logo;
	@JsonProperty("cityName")
	private String cityName;
	@JsonProperty("description")
	private String description;
	@JsonProperty("kpiSet")
	private List<KpiSet> kpiSet = new ArrayList<KpiSet>();
	@JsonProperty("pSet")
	private List<PSet> pSet = new ArrayList<PSet>();
	@JsonProperty("chartsList")
	private List<ChartsList> chartsList = new ArrayList<ChartsList>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 1962084340457482689L;

	@JsonProperty("date")
	public String getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}

	@JsonProperty("reportName")
	public String getReportName() {
		return reportName;
	}

	@JsonProperty("reportName")
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	@JsonProperty("userName ")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("userName ")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonProperty("qrCode")
	public String getQrCode() {
		return qrCode;
	}

	@JsonProperty("qrCode")
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	@JsonProperty("logo")
	public String getLogo() {
		return logo;
	}

	@JsonProperty("logo")
	public void setLogo(String logo) {
		this.logo = logo;
	}

	@JsonProperty("cityName")
	public String getCityName() {
		return cityName;
	}

	@JsonProperty("cityName")
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("kpiSet")
	public List<KpiSet> getKpiSet() {
		return kpiSet;
	}

	@JsonProperty("kpiSet")
	public void setKpiSet(List<KpiSet> kpiSet) {
		this.kpiSet = kpiSet;
	}

	@JsonProperty("pSet")
	public List<PSet> getPSet() {
		return pSet;
	}

	@JsonProperty("pSet")
	public void setPSet(List<PSet> pSet) {
		this.pSet = pSet;
	}

	@JsonProperty("chartsList")
	public List<ChartsList> getChartsList() {
		return chartsList;
	}

	@JsonProperty("chartsList")
	public void setChartsList(List<ChartsList> chartsList) {
		this.chartsList = chartsList;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}