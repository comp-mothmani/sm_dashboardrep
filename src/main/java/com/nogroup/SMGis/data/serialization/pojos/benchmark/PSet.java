package com.nogroup.SMGis.data.serialization.pojos.benchmark;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "parameter", "city1Val", "city2Val", "description", "date" })
public class PSet implements Serializable {

	@JsonProperty("parameter")
	private String parameter;
	@JsonProperty("city1Val")
	private String city1Val;
	@JsonProperty("city2Val")
	private String city2Val;
	@JsonProperty("description")
	private String description;
	@JsonProperty("date")
	private String date;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 3907093079724504401L;

	@JsonProperty("parameter")
	public String getParameter() {
		return parameter;
	}

	@JsonProperty("parameter")
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	@JsonProperty("city1Val")
	public String getCity1Val() {
		return city1Val;
	}

	@JsonProperty("city1Val")
	public void setCity1Val(String city1Val) {
		this.city1Val = city1Val;
	}

	@JsonProperty("city2Val")
	public String getCity2Val() {
		return city2Val;
	}

	@JsonProperty("city2Val")
	public void setCity2Val(String city2Val) {
		this.city2Val = city2Val;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("date")
	public String getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}