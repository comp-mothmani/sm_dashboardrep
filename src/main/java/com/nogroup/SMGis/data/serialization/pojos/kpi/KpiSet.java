
package com.nogroup.SMGis.data.serialization.pojos.kpi;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "description", "dimension", "subDimension", "category", "unit", "methodology", "source",
		"sdg" })
public class KpiSet implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("dimension")
	private String dimension;
	@JsonProperty("subDimension")
	private String subDimension;
	@JsonProperty("category")
	private String category;
	@JsonProperty("unit")
	private String unit;
	@JsonProperty("methodology")
	private String methodology;
	@JsonProperty("source")
	private String source;
	@JsonProperty("sdg")
	private String sdg;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 2650672767064930187L;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("dimension")
	public String getDimension() {
		return dimension;
	}

	@JsonProperty("dimension")
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	@JsonProperty("subDimension")
	public String getSubDimension() {
		return subDimension;
	}

	@JsonProperty("subDimension")
	public void setSubDimension(String subDimension) {
		this.subDimension = subDimension;
	}

	@JsonProperty("category")
	public String getCategory() {
		return category;
	}

	@JsonProperty("category")
	public void setCategory(String category) {
		this.category = category;
	}

	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("unit")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@JsonProperty("methodology")
	public String getMethodology() {
		return methodology;
	}

	@JsonProperty("methodology")
	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("sdg")
	public String getSdg() {
		return sdg;
	}

	@JsonProperty("sdg")
	public void setSdg(String sdg) {
		this.sdg = sdg;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}