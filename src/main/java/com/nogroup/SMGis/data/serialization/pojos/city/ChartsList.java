package com.nogroup.SMGis.data.serialization.pojos.city;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "city1Name", "city2Name", "name", "kpiName", "img" })
public class ChartsList implements Serializable {

	@JsonProperty("city1Name")
	private String city1Name;
	@JsonProperty("city2Name")
	private String city2Name;
	@JsonProperty("name")
	private String name;
	@JsonProperty("kpiName")
	private String kpiName;
	@JsonProperty("img")
	private String img;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 1228508845314458550L;

	@JsonProperty("city1Name")
	public String getCity1Name() {
		return city1Name;
	}

	@JsonProperty("city1Name")
	public void setCity1Name(String city1Name) {
		this.city1Name = city1Name;
	}

	@JsonProperty("city2Name")
	public String getCity2Name() {
		return city2Name;
	}

	@JsonProperty("city2Name")
	public void setCity2Name(String city2Name) {
		this.city2Name = city2Name;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("kpiName")
	public String getKpiName() {
		return kpiName;
	}

	@JsonProperty("kpiName")
	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	@JsonProperty("img")
	public String getImg() {
		return img;
	}

	@JsonProperty("img")
	public void setImg(String img) {
		this.img = img;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}