package com.nogroup.SMGis.data.utils;

import java.util.Date;

import com.nogroup.SMGis.data.daos.UserD;
import com.nogroup.SMGis.data.entities.AdminE;
import com.nogroup.SMGis.data.entities.ManagerE;
import com.nogroup.SMGis.data.entities.UserE;

public class InitDB {

	public static void initUsers() {
		
		if(new UserD().count() == 0) {
			UserE usr = new UserE() ;
			usr.setFName("Med Zied");
			usr.setLName("Arbi");
			usr.setUName("medzied.arbi@user.com");
			usr.setPwd("000");
			usr.setDCreated(new Date());
			new UserD().create(usr) ;
			
			ManagerE man = new ManagerE() ;
			man.setFName("Man Zied");
			man.setLName("Arbi");
			man.setUName("medzied.arbi@manager.com");
			man.setPwd("000");
			man.setDCreated(new Date());
			new UserD().create(man) ;
			
			AdminE adm = new AdminE() ;
			adm.setFName("Admin Zied");
			adm.setLName("Arbi");
			adm.setUName("medzied.arbi@admin.com");
			adm.setPwd("000");
			adm.setDCreated(new Date());
			new UserD().create(adm) ;
			
			AdminE adm1 = new AdminE() ;
			adm1.setFName("Admin Mahdi");
			adm1.setLName("othmani");
			adm1.setUName("mothmani@manager.com");
			adm1.setPwd("000");
			adm1.setDCreated(new Date());
			new UserD().create(adm1) ;
			
			AdminE adm2 = new AdminE() ;
			adm2.setFName("Admin Sami");
			adm2.setLName("Baraketi");
			adm2.setUName("sbaraketi@manager.com");
			adm2.setPwd("000");
			adm2.setDCreated(new Date());
			new UserD().create(adm2) ;
			
		}
	}
}
