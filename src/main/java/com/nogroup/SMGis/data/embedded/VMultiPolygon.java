
package com.nogroup.SMGis.data.embedded;


public class VMultiPolygon {

    private VPolygon[] polygon;

    /**
     * Returns : polygon
     * 
     */
    public VPolygon[] getPolygon() {
        return polygon;
    }

    /**
     * Sets : polygon
     * 
     */
    public void setPolygon(VPolygon[] polygon) {
        this.polygon = polygon;
    }

}
