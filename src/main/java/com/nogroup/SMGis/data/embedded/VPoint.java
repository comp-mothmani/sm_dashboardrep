
package com.nogroup.SMGis.data.embedded;


public class VPoint {

    private Double lat;
    private Double lon;

    public VPoint() {
	}

    public VPoint(double d, double e) {
    	this.lat = d;
    	this.lon = e ;
    }

	/**
     * Returns : lat
     * 
     */
    public Double getLat() {
        return lat;
    }

    /**
     * Sets : lat
     * 
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * Returns : lon
     * 
     */
    public Double getLon() {
        return lon;
    }

    /**
     * Sets : lon
     * 
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

}
