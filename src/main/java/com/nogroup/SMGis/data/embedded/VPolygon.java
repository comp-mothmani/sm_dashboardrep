
package com.nogroup.SMGis.data.embedded;

import java.util.List;

import org.vaadin.addon.vol3.client.OLCoordinate;
import org.vaadin.addon.vol3.feature.OLPolygon;

public class VPolygon {

    private VPoint[] points;

    /**
     * Returns : points
     * 
     */
    public VPoint[] getPoints() {
        return points;
    }

    /**
     * Sets : points
     * 
     */
    public void setPoints(VPoint[] points) {
        this.points = points;
    }

	public VPolygon fromOLPolygon(OLPolygon poly) {
		

		points = new VPoint[poly.getElements().get(1).size()] ;
		int i = 0 ;
		for(OLCoordinate tmp : poly.getElements().get(1)) {
			VPoint vp = new VPoint() ;
			vp.setLat(tmp.x);
			vp.setLon(tmp.y);
			points[i] = vp ;
			i++ ;
		}
		return this;
	}

	public VPoint center() {
		VPoint vp = new VPoint() ;
		double lats = 0 ;
		double lons = 0 ;
		
		for(VPoint tmp : points) {
			lats += tmp.getLat() ;
			lons += tmp.getLon() ;
		}
		
		vp.setLat(lats/points.length);
		vp.setLon(lons/points.length);
		return vp;
	}

	public VPolygon random() {
		points = new VPoint[4] ;
		for(int i = 0 ; i < 4 ;i++) {
			points[i] = new VPoint(14 + 0.1*i,14 + 0.1*i) ;
		}
		return this;
	}

}
