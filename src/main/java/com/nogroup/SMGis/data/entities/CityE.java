
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.CityParamC;
import com.nogroup.SMGis.data.collections.KpiValC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.PointConverter;
import com.nogroup.SMGis.data.converters.PolygonConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;

@Entity
@Table(name = "CITY_")
public class CityE implements TreeBean{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "country")
    private String country;
    @Column(name = "gov")
    private String gov;
    @JsonManagedReference
    @OneToMany(mappedBy = "city", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<KpiValE> kpis = new HashSet<KpiValE>();
    @JsonManagedReference
    @OneToMany(mappedBy = "city", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CityParamE> params = new HashSet<CityParamE>();
    @Column(name = "centroid")
    @Convert(converter = PointConverter.class)
    private VPoint centroid;
    @Column(name = "geometry", length = 5000)
    @Convert(converter = PolygonConverter.class)
    private VPolygon geometry;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : country
     * 
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets : country
     * 
     */
    public void setCountry(String country) {
        this.country = country;
        update() ;
    }

    /**
     * Returns : gov
     * 
     */
    public String getGov() {
        return gov;
    }

    /**
     * Sets : gov
     * 
     */
    public void setGov(String gov) {
        this.gov = gov;
        update() ;
    }

    /**
     * Returns : kpis
     * 
     */
    public Set<KpiValE> getKpis() {
        return kpis;
    }

    /**
     * Sets : kpis
     * 
     */
    public void setKpis(Set<KpiValE> kpis) {
        this.kpis = kpis;
        update() ;
    }

    /**
     * Returns : params
     * 
     */
    public Set<CityParamE> getParams() {
        return params;
    }

    /**
     * Sets : params
     * 
     */
    public void setParams(Set<CityParamE> params) {
        this.params = params;
        update() ;
    }

    /**
     * Returns : centroid
     * 
     */
    public VPoint getCentroid() {
        return centroid;
    }

    /**
     * Sets : centroid
     * 
     */
    public void setCentroid(VPoint centroid) {
        this.centroid = centroid;
        update() ;
    }

    /**
     * Returns : geometry
     * 
     */
    public VPolygon getGeometry() {
        return geometry;
    }

    /**
     * Sets : geometry
     * 
     */
    public void setGeometry(VPolygon geometry) {
        this.geometry = geometry;
        this.centroid = geometry.center() ;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an kpis.
     * 
     * @param kpis
     *     the new kpis
     */
    public void addKpis(KpiValE val) {
        this.kpis.add(val) ;
        val.setCity(this) ;
        update() ;
    }

    /**
     * Remove an kpis.
     * 
     */
    public void removeKpis(KpiValE val) {
        this.kpis.remove(val) ;
        val.setCity(null) ;
        update() ;
    }

    /**
     * Remove all kpis.
     * 
     */
    public void removeAllKpis() {
        this.kpis.clear() ;
        update() ;
    }

    /**
     * Add an params.
     * 
     * @param params
     *     the new params
     */
    public void addParams(CityParamE val) {
        this.params.add(val) ;
        val.setCity(this) ;
        update() ;
    }

    /**
     * Remove an params.
     * 
     */
    public void removeParams(CityParamE val) {
        this.params.remove(val) ;
        val.setCity(null) ;
        update() ;
    }

    /**
     * Remove all params.
     * 
     */
    public void removeAllParams() {
        this.params.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        //return ((((((((((((((((((((((((((((((((("CityE ["+"id = ")+ id)+", ")+"name = ")+ name)+", ")+"country = ")+ country)+", ")+"gov = ")+ gov)+", ")+"kpis = ")+ kpis)+", ")+"params = ")+ params)+", ")+"centroid = ")+ centroid)+", ")+"geometry = ")+ geometry)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"other = ")+ other)+"]");
    	return "" ;
    }

    public KpiValC kpis() {
        return new KpiValC(kpis) ;
    }

    public CityParamC params() {
        return new CityParamC(params) ;
    }

	@Override
	public String fetchName() {
		return name;
	}

}
