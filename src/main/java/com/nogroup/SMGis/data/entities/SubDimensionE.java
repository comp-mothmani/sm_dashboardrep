
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.CategoryC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;

@Entity
@Table(name = "SUBDIMENSION")
public class SubDimensionE implements TreeBean{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description",length=5000)
    private String description;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "dimension", nullable = false)
    private DimensionE dimension;
    @JsonManagedReference
    @OneToMany(mappedBy = "subDimension", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CategoryE> categories = new HashSet<CategoryE>();
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : description
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets : description
     * 
     */
    public void setDescription(String description) {
        this.description = description;
        update() ;
    }

    /**
     * Returns : dimension
     * 
     */
    public DimensionE getDimension() {
        return dimension;
    }

    /**
     * Sets : dimension
     * 
     */
    public void setDimension(DimensionE dimension) {
        this.dimension = dimension;
        update() ;
    }

    /**
     * Returns : categories
     * 
     */
    public Set<CategoryE> getCategories() {
        return categories;
    }

    /**
     * Sets : categories
     * 
     */
    public void setCategories(Set<CategoryE> categories) {
        this.categories = categories;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an categories.
     * 
     * @param categories
     *     the new categories
     */
    public void addCategories(CategoryE val) {
        this.categories.add(val) ;
        val.setSubDimension(this) ;
        update() ;
    }

    /**
     * Remove an categories.
     * 
     */
    public void removeCategories(CategoryE val) {
        this.categories.remove(val) ;
        val.setSubDimension(null) ;
        update() ;
    }

    /**
     * Remove all categories.
     * 
     */
    public void removeAllCategories() {
        this.categories.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        //return (((((((((((((((((((((((("SubDimensionE ["+"id = ")+ id)+", ")+"name = ")+ name)+", ")+"description = ")+ description)+", ")+"dimension = ")+ dimension)+", ")+"categories = ")+ categories)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"other = ")+ other)+"]");
    	return "";
    }

    public CategoryC categories() {
        return new CategoryC(categories) ;
    }

	@Override
	public String fetchName() {
		return name;
	}

}
