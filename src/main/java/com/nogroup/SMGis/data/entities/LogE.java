
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;

@Entity
@Table(name = "LOG")
public class LogE {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "val")
    private String val;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user", nullable = false)
    private UserE user;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : val
     * 
     */
    public String getVal() {
        return val;
    }

    /**
     * Sets : val
     * 
     */
    public void setVal(String val) {
        this.val = val;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : user
     * 
     */
    public UserE getUser() {
        return user;
    }

    /**
     * Sets : user
     * 
     */
    public void setUser(UserE user) {
        this.user = user;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    @Override
    public String toString() {
        return (((((((((((((((((("LogE ["+"id = ")+ id)+", ")+"val = ")+ val)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"user = ")+ user)+", ")+"other = ")+ other)+"]");
    }

}
