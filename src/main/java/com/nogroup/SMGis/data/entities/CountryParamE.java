
package com.nogroup.SMGis.data.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "COUNTRYPARAM")
@DiscriminatorValue("COUNTRYPARAM")
public class CountryParamE
    extends ParamE
{

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "country", nullable = false)
    private CountryE country;

    /**
     * Returns : country
     * 
     */
    public CountryE getCountry() {
        return country;
    }

    /**
     * Sets : country
     * 
     */
    public void setCountry(CountryE country) {
        this.country = country;
        update() ;
    }

    @Override
    public String toString() {
        return ((("CountryParamE ["+"country = ")+ country)+"]");
    }

}
