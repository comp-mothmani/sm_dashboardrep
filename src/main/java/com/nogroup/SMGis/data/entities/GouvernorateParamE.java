
package com.nogroup.SMGis.data.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "GOUVERNORATEPARAM")
@DiscriminatorValue("GOUVERNORATEPARAM")
public class GouvernorateParamE
    extends ParamE
{

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "gouvernorate", nullable = false)
    private GouvernorateE gouvernorate;

    /**
     * Returns : gouvernorate
     * 
     */
    public GouvernorateE getGouvernorate() {
        return gouvernorate;
    }

    /**
     * Sets : gouvernorate
     * 
     */
    public void setGouvernorate(GouvernorateE gouvernorate) {
        this.gouvernorate = gouvernorate;
        update() ;
    }

    @Override
    public String toString() {
        return ((("GouvernorateParamE ["+"gouvernorate = ")+ gouvernorate)+"]");
    }

}
