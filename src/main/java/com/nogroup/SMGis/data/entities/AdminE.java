
package com.nogroup.SMGis.data.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ADMIN")
@DiscriminatorValue("ADMIN")
public class AdminE
    extends UserE
{


    @Override
    public String toString() {
        return ("AdminE ["+"]");
    }

}
