
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.CountryParamC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.MultiPolygonConverter;
import com.nogroup.SMGis.data.converters.PointConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;

@Entity
@Table(name = "COUNTRY")
public class CountryE {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "centroid")
    @Convert(converter = PointConverter.class)
    private VPoint centroid;
    @Column(name = "geometry")
    @Convert(converter = MultiPolygonConverter.class)
    private VPolygon geometry;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @JsonManagedReference
    @OneToMany(mappedBy = "country", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CountryParamE> params = new HashSet<CountryParamE>();
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public CountryE() {
	}
    
    public CountryE(String string) {
		this.name = string ;
	}

	public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : centroid
     * 
     */
    public VPoint getCentroid() {
        return centroid;
    }

    /**
     * Sets : centroid
     * 
     */
    public void setCentroid(VPoint centroid) {
        this.centroid = centroid;
        update() ;
    }

    /**
     * Returns : geometry
     * 
     */
    public VPolygon getGeometry() {
        return geometry;
    }

    /**
     * Sets : geometry
     * 
     */
    public void setGeometry(VPolygon geometry) {
        this.geometry = geometry;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : params
     * 
     */
    public Set<CountryParamE> getParams() {
        return params;
    }

    /**
     * Sets : params
     * 
     */
    public void setParams(Set<CountryParamE> params) {
        this.params = params;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an params.
     * 
     * @param params
     *     the new params
     */
    public void addParams(CountryParamE val) {
        this.params.add(val) ;
        val.setCountry(this) ;
        update() ;
    }

    /**
     * Remove an params.
     * 
     */
    public void removeParams(CountryParamE val) {
        this.params.remove(val) ;
        val.setCountry(null) ;
        update() ;
    }

    /**
     * Remove all params.
     * 
     */
    public void removeAllParams() {
        this.params.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        return (((((((((((((((((((((((("CountryE ["+"id = ")+ id)+", ")+"name = ")+ name)+", ")+"centroid = ")+ centroid)+", ")+"geometry = ")+ geometry)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"params = ")+ params)+", ")+"other = ")+ other)+"]");
    }

    public CountryParamC params() {
        return new CountryParamC(params) ;
    }

}
