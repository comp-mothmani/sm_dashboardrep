
package com.nogroup.SMGis.data.entities;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.views.ccmp.flds.CityGridBean;

@Entity
@Table(name = "KPIVAL")
public class KpiValE implements CityGridBean{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "value")
    private String value;
    @Column(name = "kpiName")
    private String kpiName;
    @Column(name = "category")
    private String category;
    @Column(name = "subDimension")
    private String subDimension;
    @Column(name = "dimension")
    private String dimension;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "dSample")
    @Convert(converter = ShortDateConverter.class)
    private Date dSample;
    @Column(name = "unit")
    private String unit;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "city", nullable = false)
    private CityE city;

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : value
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets : value
     * 
     */
    public void setValue(String value) {
        this.value = value;
        update() ;
    }

    /**
     * Returns : kpiName
     * 
     */
    public String getKpiName() {
        return kpiName;
    }

    /**
     * Sets : kpiName
     * 
     */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
        update() ;
    }

    /**
     * Returns : category
     * 
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets : category
     * 
     */
    public void setCategory(String category) {
        this.category = category;
        update() ;
    }

    /**
     * Returns : subDimension
     * 
     */
    public String getSubDimension() {
        return subDimension;
    }

    /**
     * Sets : subDimension
     * 
     */
    public void setSubDimension(String subDimension) {
        this.subDimension = subDimension;
        update() ;
    }

    /**
     * Returns : dimension
     * 
     */
    public String getDimension() {
        return dimension;
    }

    /**
     * Sets : dimension
     * 
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : dSample
     * 
     */
    public Date getDSample() {
        return dSample;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDSample(Date dSample) {
        this.dSample = dSample;
        update() ;
    }
    /**
     * Returns : unit
     * 
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets : unit
     * 
     */
    public void setUnit(String unit) {
        this.unit = unit;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Returns : city
     * 
     */
    public CityE getCity() {
        return city;
    }

    /**
     * Sets : city
     * 
     */
    public void setCity(CityE city) {
        this.city = city;
        update() ;
    }

    @Override
    public String toString() {
        //return ((((((((((((((((((((((((((((((((("KpiValE ["+"id = ")+ id)+", ")+"value = ")+ value)+", ")+"kpiName = ")+ kpiName)+", ")+"category = ")+ category)+", ")+"subDimension = ")+ subDimension)+", ")+"dimension = ")+ dimension)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"unit = ")+ unit)+", ")+"other = ")+ other)+", ")+"city = ")+ city)+"]");
    	return "" ;
    }

	@Override
	public String fetchName() {
		return kpiName;
	}

	@Override
	public String fetchSubDimension() {
		return subDimension;
	}

	@Override
	public String fetchCategory() {
		return category;
	}

	@Override
	public String fetchUnit() {
		return unit;
	}

	@Override
	public String fetchValue() {
		return value;
	}

	public void copy(KpiValE c) {
		
		value = c.getValue();
	    kpiName = c.getKpiName();
	    category = c.getCategory();
	    subDimension = c.getSubDimension();
	    dimension = c.getDimension();
	    dCreated = new Date();
	    dSample = new Date();
	    //dSample = Date.from(new Date().toInstant().plus(10, ChronoUnit.DAYS));
	    
	    unit = c.getUnit();
	    other = c.getOther() ;
		
	    update();
	}

}
