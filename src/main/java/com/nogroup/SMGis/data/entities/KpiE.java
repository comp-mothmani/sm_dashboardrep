
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;

@Entity
@Table(name = "KPI")
public class KpiE implements TreeBean{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description",length=5000)
    private String description;
    @Column(name = "unit")
    private String unit;
    @Column(name = "methodology",length=5000)
    private String methodology;
    @Column(name = "sdg",length=5000)
    private String sdg;
    @Column(name = "dSource",length=5000)
    private String dSource;
    @Column(name = "code")
    private String code;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "category_", nullable = false)
    private CategoryE category;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : description
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets : description
     * 
     */
    public void setDescription(String description) {
        this.description = description;
        update() ;
    }

    /**
     * Returns : unit
     * 
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets : unit
     * 
     */
    public void setUnit(String unit) {
        this.unit = unit;
        update() ;
    }

    /**
     * Returns : category
     * 
     */
    public CategoryE getCategory() {
        return category;
    }

    /**
     * Sets : category
     * 
     */
    public void setCategory(CategoryE category) {
        this.category = category;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }
    
    public String getMethodology() {
		return methodology;
	}

	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	public String getSdg() {
		return sdg;
	}

	public void setSdg(String sdg) {
		this.sdg = sdg;
	}

	public String getdSource() {
		return dSource;
	}

	public void setdSource(String dSource) {
		this.dSource = dSource;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
    public String toString() {
    	return "";
    }

	@Override
	public String fetchName() {
		return name;
	}
	
	

}
