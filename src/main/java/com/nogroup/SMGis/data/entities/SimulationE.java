
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.SimKpiValC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;

@Entity
@Table(name = "SIMULATION")
public class SimulationE {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "cityName")
    private String cityName;
    @JsonManagedReference
    @OneToMany(mappedBy = "simulation", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SimKpiValE> kpis = new HashSet<SimKpiValE>();
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "manager", nullable = false)
    private UserE manager;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : description
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets : description
     * 
     */
    public void setDescription(String description) {
        this.description = description;
        update() ;
    }

    /**
     * Returns : cityName
     * 
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Sets : cityName
     * 
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
        update() ;
    }

    /**
     * Returns : kpis
     * 
     */
    public Set<SimKpiValE> getKpis() {
        return kpis;
    }

    /**
     * Sets : kpis
     * 
     */
    public void setKpis(Set<SimKpiValE> kpis) {
        this.kpis = kpis;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : manager
     * 
     */
    public UserE getManager() {
        return manager;
    }

    /**
     * Sets : manager
     * 
     */
    public void setManager(UserE manager) {
        this.manager = manager;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an kpis.
     * 
     * @param kpis
     *     the new kpis
     */
    public void addKpis(SimKpiValE val) {
        this.kpis.add(val) ;
        val.setSimulation(this) ;
        update() ;
    }

    /**
     * Remove an kpis.
     * 
     */
    public void removeKpis(SimKpiValE val) {
        this.kpis.remove(val) ;
        val.setSimulation(null) ;
        update() ;
    }

    /**
     * Remove all kpis.
     * 
     */
    public void removeAllKpis() {
        this.kpis.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        return ((((((((((((((((((((((((((("SimulationE ["+"id = ")+ id)+", ")+"name = ")+ name)+", ")+"description = ")+ description)+", ")+"cityName = ")+ cityName)+", ")+"kpis = ")+ kpis)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"manager = ")+ manager)+", ")+"other = ")+ other)+"]");
    }

    public SimKpiValC kpis() {
        return new SimKpiValC(kpis) ;
    }

}
