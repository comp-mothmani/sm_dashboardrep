
package com.nogroup.SMGis.data.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "CITYPARAM")
@DiscriminatorValue("CITYPARAM")
public class CityParamE
    extends ParamE
{

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "city", nullable = false)
    private CityE city;

    /**
     * Returns : city
     * 
     */
    public CityE getCity() {
        return city;
    }

    /**
     * Sets : city
     * 
     */
    public void setCity(CityE city) {
        this.city = city;
        update() ;
    }

    @Override
    public String toString() {
        return ((("CityParamE ["+"city = ")+ city)+"]");
    }

}
