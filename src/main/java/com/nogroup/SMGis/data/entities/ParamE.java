
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;

@Entity
@Table(name = "PARAM")
public class ParamE {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "value")
    private String value;
    @Column(name = "kpiName")
    private String kpiName;
    @Column(name = "cat")
    private String cat;
    @Column(name = "subDim")
    private String subDim;
    @Column(name = "dim")
    private String dim;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "unit")
    private String unit;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : value
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets : value
     * 
     */
    public void setValue(String value) {
        this.value = value;
        update() ;
    }

    /**
     * Returns : kpiName
     * 
     */
    public String getKpiName() {
        return kpiName;
    }

    /**
     * Sets : kpiName
     * 
     */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
        update() ;
    }

    /**
     * Returns : cat
     * 
     */
    public String getCat() {
        return cat;
    }

    /**
     * Sets : cat
     * 
     */
    public void setCat(String cat) {
        this.cat = cat;
        update() ;
    }

    /**
     * Returns : subDim
     * 
     */
    public String getSubDim() {
        return subDim;
    }

    /**
     * Sets : subDim
     * 
     */
    public void setSubDim(String subDim) {
        this.subDim = subDim;
        update() ;
    }

    /**
     * Returns : dim
     * 
     */
    public String getDim() {
        return dim;
    }

    /**
     * Sets : dim
     * 
     */
    public void setDim(String dim) {
        this.dim = dim;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : unit
     * 
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets : unit
     * 
     */
    public void setUnit(String unit) {
        this.unit = unit;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    @Override
    public String toString() {
        return (((((((((((((((((((((((((((((("ParamE ["+"id = ")+ id)+", ")+"value = ")+ value)+", ")+"kpiName = ")+ kpiName)+", ")+"cat = ")+ cat)+", ")+"subDim = ")+ subDim)+", ")+"dim = ")+ dim)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"unit = ")+ unit)+", ")+"other = ")+ other)+"]");
    }

}
