
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.KpiC;
import com.nogroup.SMGis.data.collections.SubDimensionC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.views.ccmp.flds.CityGridBean;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;

@Entity
@Table(name = "DIMENSION")
public class DimensionE implements TreeBean,CityGridBean{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description",length=5000)
    private String description;
    @JsonManagedReference
    @OneToMany(mappedBy = "dimension", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SubDimensionE> subDimenstions = new HashSet<SubDimensionE>();
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : description
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets : description
     * 
     */
    public void setDescription(String description) {
        this.description = description;
        update() ;
    }

    /**
     * Returns : subDimenstions
     * 
     */
    public Set<SubDimensionE> getSubDimenstions() {
        return subDimenstions;
    }

    /**
     * Sets : subDimenstions
     * 
     */
    public void setSubDimenstions(Set<SubDimensionE> subDimenstions) {
        this.subDimenstions = subDimenstions;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an subDimenstions.
     * 
     * @param subDimenstions
     *     the new subDimenstions
     */
    public void addSubDimenstions(SubDimensionE val) {
        this.subDimenstions.add(val) ;
        val.setDimension(this) ;
        update() ;
    }

    /**
     * Remove an subDimenstions.
     * 
     */
    public void removeSubDimenstions(SubDimensionE val) {
        this.subDimenstions.remove(val) ;
        val.setDimension(null) ;
        update() ;
    }

    /**
     * Remove all subDimenstions.
     * 
     */
    public void removeAllSubDimenstions() {
        this.subDimenstions.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        //return ((((((((((((((((((((("DimensionE ["+"id = ")+ id)+", ")+"name = ")+ name)+", ")+"description = ")+ description)+", ")+"subDimenstions = ")+ subDimenstions)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"other = ")+ other)+"]");
        return "" ;
    }

    public SubDimensionC subDimenstions() {
        return new SubDimensionC(subDimenstions) ;
    }

	@Override
	public String fetchName() {
		return getName();
	}

	public KpiC kpis() {
		KpiC tmps = new KpiC() ;
		for(SubDimensionE tmp : subDimenstions) {
			for(CategoryE tmp2 : tmp.getCategories()) {
				tmps.addAll(tmp2.getKpis()) ;
			}
		}
		return tmps ;
	}

	@Override
	public String fetchSubDimension() {
		return "";
	}

	@Override
	public String fetchCategory() {
		return "";
	}

	@Override
	public String fetchUnit() {
		return "";
	}

	@Override
	public String fetchValue() {
		return "";
	}

}
