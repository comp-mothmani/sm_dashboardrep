
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;

@Entity
@Table(name = "SIMKPIVAL")
public class SimKpiValE {

	@Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private Integer id;
    @Column(name = "value")
    private String value;
    @Column(name = "kpiName")
    private String kpiName;
    @Column(name = "category")
    private String category;
    @Column(name = "subDimension")
    private String subDimension;
    @Column(name = "dimension")
    private String dimension;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "unit")
    private String unit;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "simulation", nullable = true)
    private SimulationE simulation;
    
    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : value
     * 
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets : value
     * 
     */
    public void setValue(String value) {
        this.value = value;
        update() ;
    }

    /**
     * Returns : kpiName
     * 
     */
    public String getKpiName() {
        return kpiName;
    }

    /**
     * Sets : kpiName
     * 
     */
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
        update() ;
    }

    /**
     * Returns : category
     * 
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets : category
     * 
     */
    public void setCategory(String category) {
        this.category = category;
        update() ;
    }

    /**
     * Returns : subDimension
     * 
     */
    public String getSubDimension() {
        return subDimension;
    }

    /**
     * Sets : subDimension
     * 
     */
    public void setSubDimension(String subDimension) {
        this.subDimension = subDimension;
        update() ;
    }

    /**
     * Returns : dimension
     * 
     */
    public String getDimension() {
        return dimension;
    }

    /**
     * Sets : dimension
     * 
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : unit
     * 
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets : unit
     * 
     */
    public void setUnit(String unit) {
        this.unit = unit;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Returns : simulation
     * 
     */
    public SimulationE getSimulation() {
        return simulation;
    }

    /**
     * Sets : simulation
     * 
     */
    public void setSimulation(SimulationE simulation) {
        this.simulation = simulation;
        update() ;
    }

    @Override
    public String toString() {
        return ((("SimKpiValE ["+"simulation = ")+ simulation)+"]");
    }

}
