
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;

@MappedSuperclass
public class PersonE {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fName")
    private String fName;
    @Column(name = "lName")
    private String lName;
    @Column(name = "uName")
    private String uName;
    @Column(name = "pwd")
    private String pwd;
    @Column(name = "slt")
    private String slt;
    @Column(name = "img",length=10000)
    private String img;
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : fName
     * 
     */
    public String getFName() {
        return fName;
    }

    /**
     * Sets : fName
     * 
     */
    public void setFName(String fName) {
        this.fName = fName;
        update() ;
    }

    /**
     * Returns : lName
     * 
     */
    public String getLName() {
        return lName;
    }

    /**
     * Sets : lName
     * 
     */
    public void setLName(String lName) {
        this.lName = lName;
        update() ;
    }

    /**
     * Returns : uName
     * 
     */
    public String getUName() {
        return uName;
    }

    /**
     * Sets : uName
     * 
     */
    public void setUName(String uName) {
        this.uName = uName;
        update() ;
    }

    /**
     * Returns : pwd
     * 
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * Sets : pwd
     * 
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
        update() ;
    }

    /**
     * Returns : slt
     * 
     */
    public String getSlt() {
        return slt;
    }

    /**
     * Sets : slt
     * 
     */
    public void setSlt(String slt) {
        this.slt = slt;
        update() ;
    }

    /**
     * Returns : img
     * 
     */
    public String getImg() {
        return img;
    }

    /**
     * Sets : img
     * 
     */
    public void setImg(String img) {
        this.img = img;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    @Override
    public String toString() {
        return (((((((((((((((((((((((((((((("PersonE ["+"id = ")+ id)+", ")+"fName = ")+ fName)+", ")+"lName = ")+ lName)+", ")+"uName = ")+ uName)+", ")+"pwd = ")+ pwd)+", ")+"slt = ")+ slt)+", ")+"img = ")+ img)+", ")+"dCreated = ")+ dCreated)+", ")+"dModified = ")+ dModified)+", ")+"other = ")+ other)+"]");
    }

}
