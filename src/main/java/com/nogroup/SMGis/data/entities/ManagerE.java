
package com.nogroup.SMGis.data.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.SimulationC;

@Entity
@Table(name = "MANAGER")
@DiscriminatorValue("MANAGER")
public class ManagerE
    extends UserE
{

    @JsonManagedReference
    @OneToMany(mappedBy = "manager", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SimulationE> sims = new HashSet<SimulationE>();

    /**
     * Returns : sims
     * 
     */
    public Set<SimulationE> getSims() {
        return sims;
    }

    /**
     * Sets : sims
     * 
     */
    public void setSims(Set<SimulationE> sims) {
        this.sims = sims;
        update() ;
    }

    /**
     * Add an sims.
     * 
     * @param sims
     *     the new sims
     */
    public void addSims(SimulationE val) {
        this.sims.add(val) ;
        val.setManager(this) ;
        update() ;
    }

    /**
     * Remove an sims.
     * 
     */
    public void removeSims(SimulationE val) {
        this.sims.remove(val) ;
        val.setManager(null) ;
        update() ;
    }

    /**
     * Remove all sims.
     * 
     */
    public void removeAllSims() {
        this.sims.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        return ((("ManagerE ["+"sims = ")+ sims)+"]");
    }

    public SimulationC sims() {
        return new SimulationC(sims) ;
    }

}
