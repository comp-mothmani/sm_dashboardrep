
package com.nogroup.SMGis.data.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nogroup.SMGis.data.collections.KpiC;
import com.nogroup.SMGis.data.converters.HashMapConverter;
import com.nogroup.SMGis.data.converters.LongDateConverter;
import com.nogroup.SMGis.data.converters.ShortDateConverter;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;

@Entity
@Table(name = "CATEGORY_")
public class CategoryE implements TreeBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description",length=5000)
    private String description;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "subDimension", nullable = false)
    private SubDimensionE subDimension;
    @JsonManagedReference
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<KpiE> kpis = new HashSet<KpiE>();
    @Column(name = "dCreated")
    @Convert(converter = LongDateConverter.class)
    private Date dCreated;
    @Column(name = "dModified")
    @Convert(converter = ShortDateConverter.class)
    private Date dModified;
    @Column(name = "other")
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Object> other = new HashMap<String, Object>();

    public void update() {
        dModified = new Date() ; 
    }

    /**
     * Returns : id
     * 
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets : id
     * 
     */
    public void setId(Integer id) {
        this.id = id;
        update() ;
    }

    /**
     * Returns : name
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * Sets : name
     * 
     */
    public void setName(String name) {
        this.name = name;
        update() ;
    }

    /**
     * Returns : description
     * 
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets : description
     * 
     */
    public void setDescription(String description) {
        this.description = description;
        update() ;
    }

    /**
     * Returns : subDimension
     * 
     */
    public SubDimensionE getSubDimension() {
        return subDimension;
    }

    /**
     * Sets : subDimension
     * 
     */
    public void setSubDimension(SubDimensionE subDimension) {
        this.subDimension = subDimension;
        update() ;
    }

    /**
     * Returns : kpis
     * 
     */
    public Set<KpiE> getKpis() {
        return kpis;
    }

    /**
     * Sets : kpis
     * 
     */
    public void setKpis(Set<KpiE> kpis) {
        this.kpis = kpis;
        update() ;
    }

    /**
     * Returns : dCreated
     * 
     */
    public Date getDCreated() {
        return dCreated;
    }

    /**
     * Sets : dCreated
     * 
     */
    public void setDCreated(Date dCreated) {
        this.dCreated = dCreated;
        update() ;
    }

    /**
     * Returns : dModified
     * 
     */
    public Date getDModified() {
        return dModified;
    }

    /**
     * Sets : dModified
     * 
     */
    public void setDModified(Date dModified) {
        this.dModified = dModified;
        update() ;
    }

    /**
     * Returns : other
     * 
     */
    public HashMap<String, Object> getOther() {
        return other;
    }

    /**
     * Sets : other
     * 
     */
    public void setOther(HashMap<String, Object> other) {
        this.other = other;
        update() ;
    }

    /**
     * Add an kpis.
     * 
     * @param kpis
     *     the new kpis
     */
    public void addKpis(KpiE val) {
        this.kpis.add(val) ;
        val.setCategory(this) ;
        update() ;
    }

    /**
     * Remove an kpis.
     * 
     */
    public void removeKpis(KpiE val) {
        this.kpis.remove(val) ;
        val.setCategory(null) ;
        update() ;
    }

    /**
     * Remove all kpis.
     * 
     */
    public void removeAllKpis() {
        this.kpis.clear() ;
        update() ;
    }

    @Override
    public String toString() {
        return "" ;
    }

    public KpiC kpis() {
        return new KpiC(kpis) ;
    }

	@Override
	public String fetchName() {
		return name;
	}

}
