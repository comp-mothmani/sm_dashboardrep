
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CountryE;
import com.nogroup.SMGis.data.entities.CountryParamE;


/**
 * This is a collection class.
 * 
 */
public class CountryParamC
    extends ArrayList<CountryParamE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public CountryParamC() {
    }

    public CountryParamC(Collection<CountryParamE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public CountryParamC filterByCountry(CountryE val) {
        CountryParamC tmps = new CountryParamC() ;
        for(CountryParamE tmp : this){
        	if(tmp.getCountry().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryParamE findByCountry(CountryE val) {
        if(this.filterByCountry(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<CountryE> projectOverCountry() {
        List<CountryE> ls = new ArrayList<>() ;
        for(CountryParamE tmp : this){
        	ls.add(tmp.getCountry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<CountryE> narrowProjectionOverCountry() {
        Set<CountryE> ls = new HashSet<CountryE>();
        for(CountryParamE tmp : this){
        	ls.add(tmp.getCountry()) ;
        }
        return (ls);
    }

}
