
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.PersonE;
import com.nogroup.SMGis.data.entities.UserE;


/**
 * This is a collection class.
 * 
 */
public class PersonC
    extends ArrayList<PersonE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public PersonC() {
    }

    public PersonC(Collection<PersonE> vals) {
        this.addAll(vals) ;
    }

    public PersonC(List<UserE> read) {
		this.addAll(read);
	}

	/**
     * Returns the id.
     * 
     */
    public PersonC filterById(Integer val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByFName(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getFName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByLName(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getLName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByUName(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getUName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByPwd(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getPwd().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterBySlt(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getSlt().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByImg(String val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getImg().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByDCreated(Date val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByDModified(Date val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonC filterByOther(HashMap<String, Object> val) {
        PersonC tmps = new PersonC() ;
        for(PersonE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByFName(String val) {
        if(this.filterByFName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByLName(String val) {
        if(this.filterByLName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByUName(String val) {
        if(this.filterByUName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByPwd(String val) {
        if(this.filterByPwd(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findBySlt(String val) {
        if(this.filterBySlt(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByImg(String val) {
        if(this.filterByImg(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public PersonE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverFName() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getFName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverLName() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getLName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverUName() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getUName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverPwd() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getPwd()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverSlt() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getSlt()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverImg() {
        List<String> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getImg()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(PersonE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(PersonE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverFName() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getFName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverLName() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getLName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverUName() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getUName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverPwd() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getPwd()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverSlt() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getSlt()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverImg() {
        Set<String> ls = new HashSet<String>();
        for(PersonE tmp : this){
        	ls.add(tmp.getImg()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(PersonE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(PersonE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(PersonE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
