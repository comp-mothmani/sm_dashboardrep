
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.SimKpiValE;
import com.nogroup.SMGis.data.entities.SimulationE;


/**
 * This is a collection class.
 * 
 */
public class SimKpiValC
    extends ArrayList<SimKpiValE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public SimKpiValC() {
    }

    public SimKpiValC(Collection<SimKpiValE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public SimKpiValC filterBySimulation(SimulationE val) {
        SimKpiValC tmps = new SimKpiValC() ;
        for(SimKpiValE tmp : this){
        	if(tmp.getSimulation().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimKpiValE findBySimulation(SimulationE val) {
        if(this.filterBySimulation(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<SimulationE> projectOverSimulation() {
        List<SimulationE> ls = new ArrayList<>() ;
        for(SimKpiValE tmp : this){
        	ls.add(tmp.getSimulation()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<SimulationE> narrowProjectionOverSimulation() {
        Set<SimulationE> ls = new HashSet<SimulationE>();
        for(SimKpiValE tmp : this){
        	ls.add(tmp.getSimulation()) ;
        }
        return (ls);
    }

}
