
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.data.entities.GouvernorateE;
import com.nogroup.SMGis.data.entities.GouvernorateParamE;


/**
 * This is a collection class.
 * 
 */
public class GouvernorateC
    extends ArrayList<GouvernorateE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public GouvernorateC() {
    }

    public GouvernorateC(Collection<GouvernorateE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterById(Integer val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByName(String val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByCentroid(VPoint val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getCentroid().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByGeometry(VPolygon val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getGeometry().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByDCreated(Date val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByDModified(Date val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByParams(Set<GouvernorateParamE> val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getParams().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateC filterByOther(HashMap<String, Object> val) {
        GouvernorateC tmps = new GouvernorateC() ;
        for(GouvernorateE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByCentroid(VPoint val) {
        if(this.filterByCentroid(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByGeometry(VPolygon val) {
        if(this.filterByGeometry(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByParams(Set<GouvernorateParamE> val) {
        if(this.filterByParams(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPoint> projectOverCentroid() {
        List<VPoint> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPolygon> projectOverGeometry() {
        List<VPolygon> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<GouvernorateParamE>> projectOverParams() {
        List<Set<GouvernorateParamE>> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPoint> narrowProjectionOverCentroid() {
        Set<VPoint> ls = new HashSet<VPoint>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPolygon> narrowProjectionOverGeometry() {
        Set<VPolygon> ls = new HashSet<VPolygon>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<GouvernorateParamE>> narrowProjectionOverParams() {
        Set<Set<GouvernorateParamE>> ls = new HashSet<Set<GouvernorateParamE>>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(GouvernorateE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
