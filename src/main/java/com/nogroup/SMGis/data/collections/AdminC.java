
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.AdminE;


/**
 * This is a collection class.
 * 
 */
public class AdminC
    extends ArrayList<AdminE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public AdminC() {
    }

    public AdminC(Collection<AdminE> vals) {
        this.addAll(vals) ;
    }

}
