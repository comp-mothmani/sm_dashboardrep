
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.ParamE;


/**
 * This is a collection class.
 * 
 */
public class ParamC
    extends ArrayList<ParamE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public ParamC() {
    }

    public ParamC(Collection<ParamE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterById(Integer val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByValue(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getValue().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByKpiName(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getKpiName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByCat(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getCat().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterBySubDim(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getSubDim().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByDim(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getDim().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByDCreated(Date val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByDModified(Date val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByUnit(String val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getUnit().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamC filterByOther(HashMap<String, Object> val) {
        ParamC tmps = new ParamC() ;
        for(ParamE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByValue(String val) {
        if(this.filterByValue(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByKpiName(String val) {
        if(this.filterByKpiName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByCat(String val) {
        if(this.filterByCat(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findBySubDim(String val) {
        if(this.filterBySubDim(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByDim(String val) {
        if(this.filterByDim(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByUnit(String val) {
        if(this.filterByUnit(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public ParamE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverValue() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getValue()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverKpiName() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getKpiName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverCat() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getCat()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverSubDim() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getSubDim()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDim() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getDim()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverUnit() {
        List<String> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(ParamE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(ParamE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverValue() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getValue()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverKpiName() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getKpiName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverCat() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getCat()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverSubDim() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getSubDim()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDim() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getDim()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(ParamE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(ParamE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverUnit() {
        Set<String> ls = new HashSet<String>();
        for(ParamE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(ParamE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
