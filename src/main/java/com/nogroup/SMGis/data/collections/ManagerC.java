
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.ManagerE;
import com.nogroup.SMGis.data.entities.SimulationE;


/**
 * This is a collection class.
 * 
 */
public class ManagerC
    extends ArrayList<ManagerE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public ManagerC() {
    }

    public ManagerC(Collection<ManagerE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public ManagerC filterBySims(Set<SimulationE> val) {
        ManagerC tmps = new ManagerC() ;
        for(ManagerE tmp : this){
        	if(tmp.getSims().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public ManagerE findBySims(Set<SimulationE> val) {
        if(this.filterBySims(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<SimulationE>> projectOverSims() {
        List<Set<SimulationE>> ls = new ArrayList<>() ;
        for(ManagerE tmp : this){
        	ls.add(tmp.getSims()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<SimulationE>> narrowProjectionOverSims() {
        Set<Set<SimulationE>> ls = new HashSet<Set<SimulationE>>();
        for(ManagerE tmp : this){
        	ls.add(tmp.getSims()) ;
        }
        return (ls);
    }

}
