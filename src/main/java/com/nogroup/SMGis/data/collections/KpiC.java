
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.KpiE;


/**
 * This is a collection class.
 * 
 */
public class KpiC
    extends ArrayList<KpiE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public KpiC() {
    }

    public KpiC(Collection<KpiE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterById(Integer val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByName(String val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByDescription(String val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getDescription().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByUnit(String val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getUnit().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByCategory(CategoryE val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getCategory().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByDCreated(Date val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByDModified(Date val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiC filterByOther(HashMap<String, Object> val) {
        KpiC tmps = new KpiC() ;
        for(KpiE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	return this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByDescription(String val) {
        if(this.filterByDescription(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByUnit(String val) {
        if(this.filterByUnit(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByCategory(CategoryE val) {
        if(this.filterByCategory(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDescription() {
        List<String> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverUnit() {
        List<String> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<CategoryE> projectOverCategory() {
        List<CategoryE> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getCategory()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(KpiE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(KpiE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(KpiE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDescription() {
        Set<String> ls = new HashSet<String>();
        for(KpiE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverUnit() {
        Set<String> ls = new HashSet<String>();
        for(KpiE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<CategoryE> narrowProjectionOverCategory() {
        Set<CategoryE> ls = new HashSet<CategoryE>();
        for(KpiE tmp : this){
        	ls.add(tmp.getCategory()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(KpiE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(KpiE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(KpiE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
