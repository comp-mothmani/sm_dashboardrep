
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.SubDimensionE;


/**
 * This is a collection class.
 * 
 */
public class SubDimensionC
    extends ArrayList<SubDimensionE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public SubDimensionC() {
    }

    public SubDimensionC(Collection<SubDimensionE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterById(Integer val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByName(String val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByDescription(String val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getDescription().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByDimension(DimensionE val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getDimension().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByCategories(Set<CategoryE> val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getCategories().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByDCreated(Date val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByDModified(Date val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionC filterByOther(HashMap<String, Object> val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByDescription(String val) {
        if(this.filterByDescription(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByDimension(DimensionE val) {
        if(this.filterByDimension(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByCategories(Set<CategoryE> val) {
        if(this.filterByCategories(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SubDimensionE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDescription() {
        List<String> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<DimensionE> projectOverDimension() {
        List<DimensionE> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<CategoryE>> projectOverCategories() {
        List<Set<CategoryE>> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getCategories()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDescription() {
        Set<String> ls = new HashSet<String>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<DimensionE> narrowProjectionOverDimension() {
        Set<DimensionE> ls = new HashSet<DimensionE>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<CategoryE>> narrowProjectionOverCategories() {
        Set<Set<CategoryE>> ls = new HashSet<Set<CategoryE>>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getCategories()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(SubDimensionE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    public SubDimensionC filterByDimension(String val) {
        SubDimensionC tmps = new SubDimensionC() ;
        for(SubDimensionE tmp : this){
        	if(tmp.getDimension().getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }
	

}
