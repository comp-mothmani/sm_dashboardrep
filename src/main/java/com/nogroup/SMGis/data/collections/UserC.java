
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.daos.UserD;
import com.nogroup.SMGis.data.entities.LogE;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.data.entities.SettingE;
import com.nogroup.SMGis.data.entities.UserE;


/**
 * This is a collection class.
 * 
 */
public class UserC
    extends ArrayList<UserE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public UserC() {
    }

    public UserC(Collection<UserE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterBySettings(Set<SettingE> val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getSettings().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByLogs(Set<LogE> val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getLogs().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findBySettings(Set<SettingE> val) {
        if(this.filterBySettings(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByLogs(Set<LogE> val) {
        if(this.filterByLogs(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<SettingE>> projectOverSettings() {
        List<Set<SettingE>> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getSettings()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<LogE>> projectOverLogs() {
        List<Set<LogE>> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getLogs()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<SettingE>> narrowProjectionOverSettings() {
        Set<Set<SettingE>> ls = new HashSet<Set<SettingE>>();
        for(UserE tmp : this){
        	ls.add(tmp.getSettings()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<LogE>> narrowProjectionOverLogs() {
        Set<Set<LogE>> ls = new HashSet<Set<LogE>>();
        for(UserE tmp : this){
        	ls.add(tmp.getLogs()) ;
        }
        return (ls);
    }

	public UserC fromDB() {
		this.clear();
		addAll(new UserD().read()) ;
		return this ;
	}

	public UserE auth(String userName, String password) {
		for(UserE tmp : this) {
			if(tmp.getUName().equals(userName) && tmp.getPwd().equals(password)) {
				return tmp ;
			}
		}
		return null;
		
	}
	
	/**
     * Returns the id.
     * 
     */
    public UserC filterById(Integer val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByFName(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getFName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByLName(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getLName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByUName(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getUName().equals(val)){
        		System.out.println(tmp.getUName() + " " + val);
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByPwd(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getPwd().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterBySlt(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getSlt().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByImg(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getImg().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByDCreated(Date val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByDModified(Date val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }
    
    /**
     * Returns the id.
     * 
     */
    public UserC filterByDiscrimatorValue(String val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getDecriminatorValue().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return tmps;
    }

    /**
     * Returns the id.
     * 
     */
    public UserC filterByOther(HashMap<String, Object> val) {
        UserC tmps = new UserC() ;
        for(UserE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByFName(String val) {
        if(this.filterByFName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByLName(String val) {
        if(this.filterByLName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByUName(String val) {
    	UserC found = this.filterByUName(val) ;
        if(found.size() != 0){
        	return found.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByPwd(String val) {
        if(this.filterByPwd(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findBySlt(String val) {
        if(this.filterBySlt(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByImg(String val) {
        if(this.filterByImg(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public UserE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverFName() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getFName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverLName() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getLName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverUName() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getUName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverPwd() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getPwd()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverSlt() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getSlt()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverImg() {
        List<String> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getImg()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(UserE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(UserE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverFName() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getFName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverLName() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getLName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverUName() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getUName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverPwd() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getPwd()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverSlt() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getSlt()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverImg() {
        Set<String> ls = new HashSet<String>();
        for(UserE tmp : this){
        	ls.add(tmp.getImg()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(UserE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(UserE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(UserE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }
}
