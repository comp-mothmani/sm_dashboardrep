
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.data.entities.CountryE;
import com.nogroup.SMGis.data.entities.CountryParamE;


/**
 * This is a collection class.
 * 
 */
public class CountryC
    extends ArrayList<CountryE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public CountryC() {
    }

    public CountryC(Collection<CountryE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterById(Integer val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByName(String val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByCentroid(VPoint val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getCentroid().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByGeometry(VPolygon val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getGeometry().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByDCreated(Date val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByDModified(Date val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByParams(Set<CountryParamE> val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getParams().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryC filterByOther(HashMap<String, Object> val) {
        CountryC tmps = new CountryC() ;
        for(CountryE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByCentroid(VPoint val) {
        if(this.filterByCentroid(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByGeometry(VPolygon val) {
        if(this.filterByGeometry(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByParams(Set<CountryParamE> val) {
        if(this.filterByParams(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CountryE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPoint> projectOverCentroid() {
        List<VPoint> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPolygon> projectOverGeometry() {
        List<VPolygon> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<CountryParamE>> projectOverParams() {
        List<Set<CountryParamE>> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(CountryE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(CountryE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(CountryE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPoint> narrowProjectionOverCentroid() {
        Set<VPoint> ls = new HashSet<VPoint>();
        for(CountryE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPolygon> narrowProjectionOverGeometry() {
        Set<VPolygon> ls = new HashSet<VPolygon>();
        for(CountryE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(CountryE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(CountryE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<CountryParamE>> narrowProjectionOverParams() {
        Set<Set<CountryParamE>> ls = new HashSet<Set<CountryParamE>>();
        for(CountryE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(CountryE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
