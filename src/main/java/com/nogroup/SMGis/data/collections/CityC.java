
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.CityParamE;
import com.nogroup.SMGis.data.entities.KpiValE;


/**
 * This is a collection class.
 * 
 */
public class CityC
    extends ArrayList<CityE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public CityC() {
    }

    public CityC(Collection<CityE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterById(Integer val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByName(String val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByCountry(String val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getCountry().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByGov(String val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getGov().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByKpis(Set<KpiValE> val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getKpis().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByParams(Set<CityParamE> val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getParams().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByCentroid(VPoint val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getCentroid().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByGeometry(VPolygon val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getGeometry().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByDCreated(Date val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByDModified(Date val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityC filterByOther(HashMap<String, Object> val) {
        CityC tmps = new CityC() ;
        for(CityE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByCountry(String val) {
        if(this.filterByCountry(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByGov(String val) {
        if(this.filterByGov(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByKpis(Set<KpiValE> val) {
        if(this.filterByKpis(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByParams(Set<CityParamE> val) {
        if(this.filterByParams(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByCentroid(VPoint val) {
        if(this.filterByCentroid(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByGeometry(VPolygon val) {
        if(this.filterByGeometry(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CityE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverCountry() {
        List<String> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getCountry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverGov() {
        List<String> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getGov()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<KpiValE>> projectOverKpis() {
        List<Set<KpiValE>> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<CityParamE>> projectOverParams() {
        List<Set<CityParamE>> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPoint> projectOverCentroid() {
        List<VPoint> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<VPolygon> projectOverGeometry() {
        List<VPolygon> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(CityE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(CityE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(CityE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverCountry() {
        Set<String> ls = new HashSet<String>();
        for(CityE tmp : this){
        	ls.add(tmp.getCountry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverGov() {
        Set<String> ls = new HashSet<String>();
        for(CityE tmp : this){
        	ls.add(tmp.getGov()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<KpiValE>> narrowProjectionOverKpis() {
        Set<Set<KpiValE>> ls = new HashSet<Set<KpiValE>>();
        for(CityE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<CityParamE>> narrowProjectionOverParams() {
        Set<Set<CityParamE>> ls = new HashSet<Set<CityParamE>>();
        for(CityE tmp : this){
        	ls.add(tmp.getParams()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPoint> narrowProjectionOverCentroid() {
        Set<VPoint> ls = new HashSet<VPoint>();
        for(CityE tmp : this){
        	ls.add(tmp.getCentroid()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<VPolygon> narrowProjectionOverGeometry() {
        Set<VPolygon> ls = new HashSet<VPolygon>();
        for(CityE tmp : this){
        	ls.add(tmp.getGeometry()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(CityE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(CityE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(CityE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
