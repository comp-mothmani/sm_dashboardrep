
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.SimKpiValE;
import com.nogroup.SMGis.data.entities.SimulationE;
import com.nogroup.SMGis.data.entities.UserE;


/**
 * This is a collection class.
 * 
 */
public class SimulationC
    extends ArrayList<SimulationE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public SimulationC() {
    }

    public SimulationC(Collection<SimulationE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterById(Integer val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByName(String val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByDescription(String val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getDescription().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByCityName(String val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getCityName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByKpis(Set<SimKpiValE> val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getKpis().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByDCreated(Date val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByDModified(Date val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByManager(UserE val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getManager().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationC filterByOther(HashMap<String, Object> val) {
        SimulationC tmps = new SimulationC() ;
        for(SimulationE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByDescription(String val) {
        if(this.filterByDescription(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByCityName(String val) {
        if(this.filterByCityName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByKpis(Set<SimKpiValE> val) {
        if(this.filterByKpis(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByManager(UserE val) {
        if(this.filterByManager(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SimulationE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDescription() {
        List<String> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverCityName() {
        List<String> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getCityName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<SimKpiValE>> projectOverKpis() {
        List<Set<SimKpiValE>> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<UserE> projectOverManager() {
        List<UserE> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getManager()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(SimulationE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDescription() {
        Set<String> ls = new HashSet<String>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverCityName() {
        Set<String> ls = new HashSet<String>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getCityName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<SimKpiValE>> narrowProjectionOverKpis() {
        Set<Set<SimKpiValE>> ls = new HashSet<Set<SimKpiValE>>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<UserE> narrowProjectionOverManager() {
        Set<UserE> ls = new HashSet<UserE>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getManager()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(SimulationE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
