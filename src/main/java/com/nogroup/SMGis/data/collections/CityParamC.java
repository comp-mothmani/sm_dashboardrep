
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.CityParamE;


/**
 * This is a collection class.
 * 
 */
public class CityParamC
    extends ArrayList<CityParamE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public CityParamC() {
    }

    public CityParamC(Collection<CityParamE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public CityParamC filterByCity(CityE val) {
        CityParamC tmps = new CityParamC() ;
        for(CityParamE tmp : this){
        	if(tmp.getCity().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CityParamE findByCity(CityE val) {
        if(this.filterByCity(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<CityE> projectOverCity() {
        List<CityE> ls = new ArrayList<>() ;
        for(CityParamE tmp : this){
        	ls.add(tmp.getCity()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<CityE> narrowProjectionOverCity() {
        Set<CityE> ls = new HashSet<CityE>();
        for(CityParamE tmp : this){
        	ls.add(tmp.getCity()) ;
        }
        return (ls);
    }

}
