
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.SettingE;
import com.nogroup.SMGis.data.entities.UserE;


/**
 * This is a collection class.
 * 
 */
public class SettingC
    extends ArrayList<SettingE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public SettingC() {
    }

    public SettingC(Collection<SettingE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public SettingC filterById(Integer val) {
        SettingC tmps = new SettingC() ;
        for(SettingE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingC filterByUser(UserE val) {
        SettingC tmps = new SettingC() ;
        for(SettingE tmp : this){
        	if(tmp.getUser().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingC filterByDCreated(Date val) {
        SettingC tmps = new SettingC() ;
        for(SettingE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingC filterByDModified(Date val) {
        SettingC tmps = new SettingC() ;
        for(SettingE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingC filterByOther(HashMap<String, Object> val) {
        SettingC tmps = new SettingC() ;
        for(SettingE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingE findByUser(UserE val) {
        if(this.filterByUser(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public SettingE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(SettingE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<UserE> projectOverUser() {
        List<UserE> ls = new ArrayList<>() ;
        for(SettingE tmp : this){
        	ls.add(tmp.getUser()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(SettingE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(SettingE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(SettingE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(SettingE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<UserE> narrowProjectionOverUser() {
        Set<UserE> ls = new HashSet<UserE>();
        for(SettingE tmp : this){
        	ls.add(tmp.getUser()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(SettingE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(SettingE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(SettingE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
