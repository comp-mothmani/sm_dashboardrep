
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.SubDimensionE;


/**
 * This is a collection class.
 * 
 */
public class CategoryC
    extends ArrayList<CategoryE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public CategoryC() {
    }

    public CategoryC(Collection<CategoryE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterById(Integer val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByName(String val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByDescription(String val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getDescription().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterBySubDimension(SubDimensionE val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getSubDimension().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByKpis(Set<KpiE> val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getKpis().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByDCreated(Date val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByDModified(Date val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryC filterByOther(HashMap<String, Object> val) {
        CategoryC tmps = new CategoryC() ;
        for(CategoryE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByName(String val) {
        if(this.filterByName(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByDescription(String val) {
        if(this.filterByDescription(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findBySubDimension(SubDimensionE val) {
        if(this.filterBySubDimension(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByKpis(Set<KpiE> val) {
        if(this.filterByKpis(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public CategoryE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverName() {
        List<String> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDescription() {
        List<String> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<SubDimensionE> projectOverSubDimension() {
        List<SubDimensionE> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getSubDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Set<KpiE>> projectOverKpis() {
        List<Set<KpiE>> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(CategoryE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverName() {
        Set<String> ls = new HashSet<String>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDescription() {
        Set<String> ls = new HashSet<String>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getDescription()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<SubDimensionE> narrowProjectionOverSubDimension() {
        Set<SubDimensionE> ls = new HashSet<SubDimensionE>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getSubDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Set<KpiE>> narrowProjectionOverKpis() {
        Set<Set<KpiE>> ls = new HashSet<Set<KpiE>>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getKpis()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(CategoryE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

}
