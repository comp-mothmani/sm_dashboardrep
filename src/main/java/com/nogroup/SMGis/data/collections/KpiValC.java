
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.KpiValE;
import com.nogroup.SMGis.views.ccmp.chrts.RDDataPoint;


/**
 * This is a collection class.
 * 
 */
public class KpiValC
    extends ArrayList<KpiValE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public KpiValC() {
    }

    public KpiValC(Collection<KpiValE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterById(Integer val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getId().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByValue(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getValue().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByKpiName(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getKpiName().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByCategory(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getCategory().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterBySubDimension(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getSubDimension().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByDimension(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getDimension().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByDCreated(Date val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getDCreated().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByDModified(Date val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getDModified().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByUnit(String val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getUnit().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByOther(HashMap<String, Object> val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getOther().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValC filterByCity(CityE val) {
        KpiValC tmps = new KpiValC() ;
        for(KpiValE tmp : this){
        	if(tmp.getCity().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findById(Integer val) {
        if(this.filterById(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByValue(String val) {
        if(this.filterByValue(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByKpiName(String val) {
        if(this.filterByKpiName(val).size() != 0){
        	return this.filterByKpiName(val).get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByCategory(String val) {
        if(this.filterByCategory(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findBySubDimension(String val) {
        if(this.filterBySubDimension(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByDimension(String val) {
        if(this.filterByDimension(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByDCreated(Date val) {
        if(this.filterByDCreated(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByDModified(Date val) {
        if(this.filterByDModified(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByUnit(String val) {
        if(this.filterByUnit(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByOther(HashMap<String, Object> val) {
        if(this.filterByOther(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public KpiValE findByCity(CityE val) {
        if(this.filterByCity(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Integer> projectOverId() {
        List<Integer> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverValue() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getValue()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverKpiName() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getKpiName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverCategory() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getCategory()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverSubDimension() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getSubDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverDimension() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDCreated() {
        List<Date> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<Date> projectOverDModified() {
        List<Date> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<String> projectOverUnit() {
        List<String> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<HashMap<String, Object>> projectOverOther() {
        List<HashMap<String,Object>> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public List<CityE> projectOverCity() {
        List<CityE> ls = new ArrayList<>() ;
        for(KpiValE tmp : this){
        	ls.add(tmp.getCity()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Integer> narrowProjectionOverId() {
        Set<Integer> ls = new HashSet<Integer>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getId()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverValue() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getValue()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverKpiName() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getKpiName()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverCategory() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getCategory()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverSubDimension() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getSubDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverDimension() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getDimension()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDCreated() {
        Set<Date> ls = new HashSet<Date>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getDCreated()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<Date> narrowProjectionOverDModified() {
        Set<Date> ls = new HashSet<Date>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getDModified()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<String> narrowProjectionOverUnit() {
        Set<String> ls = new HashSet<String>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getUnit()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<HashMap<String, Object>> narrowProjectionOverOther() {
        Set<HashMap<String, Object>> ls = new HashSet<HashMap<String, Object>>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getOther()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<CityE> narrowProjectionOverCity() {
        Set<CityE> ls = new HashSet<CityE>();
        for(KpiValE tmp : this){
        	ls.add(tmp.getCity()) ;
        }
        return (ls);
    }

	public KpiValC filterByLatest() {
		KpiValC tmps = new KpiValC() ;
		for(String tmp : narrowProjectionOverKpiName()) {
			KpiValC kpis = filterByKpiName(tmp) ;
			tmps.add(kpis.getLatest()) ;
		}
		return tmps;
	}

	public KpiValE getLatest() {
		Collections.sort(this, new Comparator<KpiValE>() {
	        @Override
	        public int compare(KpiValE o1, KpiValE o2) {
	        	return o1.getId().compareTo(o2.getId()) * (-1) ;
	        	//return o1.getDSample().compareTo(o2.getDSample()) * (-1)  ;
	        }
	    });
		return this.get(0) ;
	}

}
