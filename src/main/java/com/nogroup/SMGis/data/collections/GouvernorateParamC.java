
package com.nogroup.SMGis.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nogroup.SMGis.data.entities.GouvernorateE;
import com.nogroup.SMGis.data.entities.GouvernorateParamE;


/**
 * This is a collection class.
 * 
 */
public class GouvernorateParamC
    extends ArrayList<GouvernorateParamE>
    implements Serializable
{

    @JsonIgnore
    protected final static long serialVersionUID = 1L;

    public GouvernorateParamC() {
    }

    public GouvernorateParamC(Collection<GouvernorateParamE> vals) {
        this.addAll(vals) ;
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateParamC filterByGouvernorate(GouvernorateE val) {
        GouvernorateParamC tmps = new GouvernorateParamC() ;
        for(GouvernorateParamE tmp : this){
        	if(tmp.getGouvernorate().equals(val)){
        		tmps.add(tmp) ;
        	}
        }
        return (tmps);
    }

    /**
     * Returns the id.
     * 
     */
    public GouvernorateParamE findByGouvernorate(GouvernorateE val) {
        if(this.filterByGouvernorate(val).size() != 0){
        	this.get(0) ;
        }
        return (null);
    }

    /**
     * Returns the id.
     * 
     */
    public List<GouvernorateE> projectOverGouvernorate() {
        List<GouvernorateE> ls = new ArrayList<>() ;
        for(GouvernorateParamE tmp : this){
        	ls.add(tmp.getGouvernorate()) ;
        }
        return (ls);
    }

    /**
     * Returns the id.
     * 
     */
    public Set<GouvernorateE> narrowProjectionOverGouvernorate() {
        Set<GouvernorateE> ls = new HashSet<GouvernorateE>();
        for(GouvernorateParamE tmp : this){
        	ls.add(tmp.getGouvernorate()) ;
        }
        return (ls);
    }

}
