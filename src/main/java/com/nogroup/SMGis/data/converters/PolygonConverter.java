
package com.nogroup.SMGis.data.converters;

import java.io.IOException;
import javax.persistence.AttributeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.SMGis.data.embedded.VPolygon;

public class PolygonConverter
    implements AttributeConverter<VPolygon, String>
{


    @Override
    public String convertToDatabaseColumn(VPolygon obj) {
        ObjectMapper om = new ObjectMapper();
        String op = null;
        try {
            op = om.writeValueAsString(obj);
            return (op);
        } catch (JsonProcessingException _x) {
        }
        return op ;
    }

    @Override
    public VPolygon convertToEntityAttribute(String val) {
        ObjectMapper om = new ObjectMapper();
        VPolygon op = null;
        try {
            op = om.readValue(val, VPolygon.class);
            return (op);
        } catch (IOException _x) {
        }
        return op ;
    }

}
