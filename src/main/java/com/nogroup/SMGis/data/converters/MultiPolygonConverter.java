
package com.nogroup.SMGis.data.converters;

import java.io.IOException;
import javax.persistence.AttributeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.SMGis.data.embedded.VMultiPolygon;

public class MultiPolygonConverter
    implements AttributeConverter<VMultiPolygon, String>
{


    @Override
    public String convertToDatabaseColumn(VMultiPolygon obj) {
        ObjectMapper om = new ObjectMapper();
        String op = null;
        try {
            op = om.writeValueAsString(obj);
            return (op);
        } catch (JsonProcessingException _x) {
        }
        return op ;
    }

    @Override
    public VMultiPolygon convertToEntityAttribute(String val) {
        ObjectMapper om = new ObjectMapper();
        VMultiPolygon op = null;
        try {
            op = om.readValue(val, VMultiPolygon.class);
            return (op);
        } catch (IOException _x) {
        }
        return op ;
    }

}
