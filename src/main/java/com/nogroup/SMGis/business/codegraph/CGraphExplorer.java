package com.nogroup.SMGis.business.codegraph;

import java.util.HashSet;
import java.util.Set;

import com.nogroup.SMGis.business.events.DashboardEvent.GraphNodeToggled;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.vaadin.graph.GraphExplorer;
import com.vaadin.graph.GraphRepository;
import com.vaadin.graph.shared.NodeProxy;
import com.vaadin.graph.shared.NodeProxy.NodeKind;
import com.vaadin.graph.shared.NodeProxy.NodeState;

public class CGraphExplorer extends GraphExplorer<NodeImpl, ArcImpl>{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public CGraphExplorer(GraphRepository<NodeImpl, ArcImpl> repository) {
		super(repository);
		
		DashboardEventBus.register(this);
	}
	
	@Override
    public void toggleNode(String nodeId) {
		
		DashboardEventBus.post(new GraphNodeToggled(nodeId));
        Set<NodeProxy> lockedNodes = new HashSet<NodeProxy>();
        boolean lockExpanded = true;
    	NodeProxy toggledNode = getLayoutEngine().getModel().getNode(nodeId);
        if (toggledNode != null) {
            if (NodeKind.GROUP.equals(toggledNode.getKind())) {
                openMemberSelector(nodeId);
            } else {
                if (NodeState.COLLAPSED.equals(toggledNode.getState())) {
                    expand(toggledNode);
            		lockedNodes.add(toggledNode);
                    lockExpanded = false;
                } else {
                    collapse(toggledNode);
                }
            }
        }
        refreshLayout(lockedNodes, lockExpanded, null);
    }

}
