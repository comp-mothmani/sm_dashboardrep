package com.nogroup.SMGis.business.codegraph;

import com.vaadin.graph.Node;
import com.vaadin.server.Resource;

public class NodeImpl extends GraphElementImpl implements Node {

	private Resource icon;
	private String stereotype ;
	private Object data ;

	public NodeImpl(String id) {
		this(id, id);
	}

	public NodeImpl(String id, String label) {
		super(id, label);
	}

	public Resource getIcon() {
		return icon;
	}

	public void setIcon(Resource icon) {
		this.icon = icon;
	}

	public String getStereotype() {
		return stereotype;
	}

	public NodeImpl setStereotype(String stereotype) {
		this.stereotype = stereotype;
		return this ;
	}

	public Object getData() {
		return data;
	}

	public NodeImpl setData(Object data) {
		this.data = data;
		return this ;
	}
	
	public NodeImpl style(String s) {
		setStyle(s);
		return this ;
	}
	
}
