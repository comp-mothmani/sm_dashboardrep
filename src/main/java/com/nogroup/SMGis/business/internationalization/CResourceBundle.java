package com.nogroup.SMGis.business.internationalization;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

import org.json.JSONException;
import org.json.JSONObject;

import com.vaadin.server.VaadinSession;

public class CResourceBundle {

	private static JSONObject bundle;

	public void init() {
		String fileName = "LOCAL.json";
		ClassLoader classLoader = getClass().getClassLoader();

		File file = new File(classLoader.getResource(fileName).getFile());
		
		//File is found
		System.out.println("File Found : " + file.exists());
		
		//Read File Content
		String content = null;
		try {
			content = new String(Files.readAllBytes(file.toPath()),"UTF8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			bundle = new JSONObject(content) ;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static String getLocal(String key,String lang){
		try {
			return bundle.getJSONObject(key).getString(lang) ;
		} catch (JSONException e) {
			return "NOT FOUND" ;
		}
	}
	
	public static String getLocal(String key){
		String lang = VaadinSession.getCurrent().getAttribute("lang").toString() ;
		try {
			return bundle.getJSONObject(key).getString(lang) ;
		} catch (JSONException e) {
			return "NOT FOUND" ;
		}
	}
}
