package com.nogroup.SMGis.business.events;

/*
 * Event bus events used in Dashboard are listed here as inner classes.
 */
public abstract class DashboardEvent {

    public static final class UserLoginRequestedEvent {
        private final String userName, password;

        
        public UserLoginRequestedEvent(final String userName,
                final String password) {
            this.userName = userName;
            this.password = password;
        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class BrowserResizeEvent {

    }
    
    public static final class GraphNodeToggled {
    	private final String nodeId ;

    	
		public GraphNodeToggled(final String nodeId) {
			this.nodeId = nodeId;
		}

		public String getNodeId() {
			return nodeId;
		}
    }
    
    public static class UserLoggedOutEvent {

    }

    public static class NotificationsCountUpdatedEvent {
    }

    public static final class ReportsCountUpdatedEvent {
        private final int count;

        public ReportsCountUpdatedEvent(final int count) {
            this.count = count;
        }

        public int getCount() {
            return count;
        }

    }

    public static final class PostViewChangeEvent {
    }
    public static class CloseOpenWindowsEvent {
    }

    public static class ProfileUpdatedEvent {
    }
    
    public static final class MapViewVisited {
    }
    
    public static final class WrongCredentialsEvent {
    }

}
