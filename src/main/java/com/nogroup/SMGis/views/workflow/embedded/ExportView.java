package com.nogroup.SMGis.views.workflow.embedded;

import java.io.ByteArrayInputStream;
import java.util.Date;

import org.vaadin.simplefiledownloader.SimpleFileDownloader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.SMGis.data.collections.CountryC;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.data.entities.CountryE;
import com.vaadin.navigator.View;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class ExportView extends Panel implements View{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	
	public ExportView() {
		setSizeFull();
		VerticalLayout vl = new VerticalLayout() ;
		setContent(vl);
		

		SimpleFileDownloader downloader = new SimpleFileDownloader();
		addExtension(downloader);
		
		Button button = new Button("Click Me");
		button.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				final StreamResource resource = new StreamResource(() -> {
					
					CountryC cntrs = new CountryC() ;
					CountryE cn = new CountryE() ;
					VPolygon vpl = new VPolygon().random() ;
					cn.setCentroid(vpl.center());
					cn.setGeometry(vpl);
					cn.setName("ddd");
					cn.setDCreated(new Date());
					cn.setId(2);
					cntrs.add(cn) ;
					
					cn = new CountryE() ;
					vpl = new VPolygon().random() ;
					cn.setCentroid(vpl.center());
					cn.setGeometry(vpl);
					cn.setName("ddd");
					cn.setId(1);
					cn.setDCreated(new Date());
					cntrs.add(cn) ;
					
					String s = "" ;
					try {
						//s = new ObjectMapper().writeValueAsString(new DimensionD().read()) ;
						s = new ObjectMapper().writeValueAsString(cntrs) ;
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					return new ByteArrayInputStream(("" + s ) .getBytes());
				}, "dimesonsion.json");
				
				downloader.setFileDownloadResource(resource);
				downloader.download();
			}
		});
		vl.addComponent(button);
		
	}

}
