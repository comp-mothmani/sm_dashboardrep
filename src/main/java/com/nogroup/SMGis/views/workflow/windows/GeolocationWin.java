package com.nogroup.SMGis.views.workflow.windows;

import org.vaadin.addons.locationtextfield.GeocodedLocation;
import org.vaadin.addons.locationtextfield.LocationTextField;
import org.vaadin.addons.locationtextfield.OpenStreetMapGeocoder;

import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.nogroup.SMGis.views.workflow.embedded.MapViewer;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class GeolocationWin extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public GeolocationWin(MapViewer viewer) {
		caption("Nominatim");
		center();
		setWidth("50%");
		setHeight("50%");

		VerticalLayout vl = new VerticalLayout();
        vl.setSizeUndefined();
        vl.setWidth("100%");
        vl.setSpacing(true);
        vl.setMargin(true);

        final OpenStreetMapGeocoder geocoder = OpenStreetMapGeocoder.getInstance();
        geocoder.setLimit(25);
        //final LocationTextField<GeocodedLocation> ltf = new LocationTextField<GeocodedLocation>(geocoder, GeocodedLocation.class);
        LocationTextField<GeocodedLocation> ltf = new LocationTextField(geocoder) ;
        
        ltf.setCaption("Address: ");
        ltf.setWidth("100%");
        ltf.setWidth("100%");
        vl.addComponent(ltf);

        final TextField lat = new TextField("Latitude: ");
        lat.setWidth("100%");
        
        final TextField lon = new TextField("Longitude: ");
        lon.setWidth("100%");
        
        vl.addComponent(lat);
        vl.addComponent(lon);

        ltf.addLocationValueChangeListener(new ValueChangeListener<GeocodedLocation>() {
			
			@Override
			public void valueChange(ValueChangeEvent<GeocodedLocation> event) {
				GeocodedLocation loc = event.getValue();
                if (loc != null) {
                    lat.setValue("" + loc.getLat());
                    lon.setValue("" + loc.getLon());
                } else {
                    lat.setValue("");
                    lon.setValue("");
                }
				
			}
		});
        Button b = new Button("Go To", new ClickListener() {
            public void buttonClick(ClickEvent event) {
            	try {
            		viewer.center(Double.parseDouble(lat.getValue().toString()),
            			   Double.parseDouble(lon.getValue().toString()));
            	}catch (Exception e) {
            		UI.getCurrent().showNotification("Could not resolve address",Notification.TYPE_ERROR_MESSAGE);
            	}   
            }
        });
        vl.addComponent(b);

        addContent(vl);
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

}
