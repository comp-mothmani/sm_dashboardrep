package com.nogroup.SMGis.views.workflow.embedded;

import java.util.ArrayList;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.ChartJs.DataPointClickListener;
import com.byteowls.vaadin.chartjs.config.LineChartConfig;
import com.byteowls.vaadin.chartjs.data.Data;
import com.byteowls.vaadin.chartjs.data.LineDataset;
import com.byteowls.vaadin.chartjs.options.FillMode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.business.server.requests.ExportRequests;
import com.nogroup.SMGis.data.collections.DimensionC;
import com.nogroup.SMGis.data.collections.KpiValC;
import com.nogroup.SMGis.data.daos.CityD;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.daos.KpiD;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.KpiValE;
import com.nogroup.SMGis.views.ccmp.cbxs.CityCbx;
import com.nogroup.SMGis.views.ccmp.chrts.RDDataGroup;
import com.nogroup.SMGis.views.ccmp.chrts.RDDataPoint;
import com.nogroup.SMGis.views.ccmp.chrts.RDDataSet;
import com.nogroup.SMGis.views.ccmp.chrts.RadarChartFactory;
import com.nogroup.SMGis.views.ccmp.cntnrs.TimeSeriesWindow;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.CloseEvent;
import com.vaadin.ui.Notification.CloseListener;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class CityView extends Panel implements View{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private HorizontalLayout header;
	private CityE selectedItem ;
	private String selectedLabel;
	private ResponsiveRow row;
	
	public CityView() {
		
		addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        DashboardEventBus.register(this);
        
        
        VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setSpacing(false);
        root.addStyleName("dashboard-view");
        setContent(root);
        Responsive.makeResponsive(root);
        
        CityCbx cbx = new CityCbx().fromDB() ;
        
		cbx.addStyleNames(ValoTheme.COMBOBOX_BORDERLESS,ValoTheme.COMBOBOX_HUGE);
		cbx.setWidth("100%");
        root.addComponent(cbx);
        cbx.addSelectionListener(new SingleSelectionListener<CityE>() {
			
			@Override
			public void selectionChange(SingleSelectionEvent<CityE> event) {
				selectedItem = event.getValue() ;
				refresh();
			}
		});
        
        selectedItem = cbx.getValue() ;
        
        Label vll = new Label("<hr>",ContentMode.HTML) ;
        vll.setWidth("100%");
        vll.setHeight("10px");
        root.addComponent(vll);		
		
        ResponsiveLayout responsiveLayout = new ResponsiveLayout();
        row = responsiveLayout.addRow() ;
        try {
        	refresh();
        }catch (Exception e) {
			Notification n = Notification.show("No Cities are found in the database", Notification.TYPE_ERROR_MESSAGE) ;
			n.addCloseListener(new CloseListener() {
				
				@Override
				public void notificationClose(CloseEvent e) {
					UI.getCurrent().getNavigator().navigateTo("MapViewer");
				}
			});
        }
        
        root.addComponent(responsiveLayout);
        root.setExpandRatio(responsiveLayout, 1);
      
        ContextMenu contextMenu = new ContextMenu(this, true);
		MenuItem mn = contextMenu.addItem("Export");
		mn.addItem("Shapefile", e -> {
			ObjectMapper mapper = new ObjectMapper() ;
			
			ExportRequests.export2shp() ;
		});
	}
	
	private void refresh() {
		row.removeAllComponents(); 
		//row.addColumn().withDisplayRules(12,12,8,8).withComponent(EnvironmentChart());
        row.addColumn().withDisplayRules(12,12,12,12).withComponent(chart(0));
        row.addColumn().withDisplayRules(12,12,12,12).withComponent(chart(1));
        row.addColumn().withDisplayRules(12,12,12,12).withComponent(chart(2));
	}

	private Component toolbox() {
        header = new HorizontalLayout() ;
        //header.addStyleName(ValoTheme.LAYOUT_WELL);
		header.setWidth("100%");
		header.setHeight("30px");
		HorizontalLayout hznl = new HorizontalLayout();
		//hznl.setMargin(new MarginInfo(false, true));
		
		Button btn = new Button("New City") ;
		btn.addStyleNames(ValoTheme.BUTTON_FRIENDLY,ValoTheme.BUTTON_TINY);
		hznl.addComponent(btn);
		
		Button btn1 = new Button("Delete") ;
		btn1.addStyleNames(ValoTheme.BUTTON_DANGER,ValoTheme.BUTTON_TINY);
		hznl.addComponent(btn1);
		
		header.addComponent(hznl);
		header.setExpandRatio(hznl, 1);
		header.setComponentAlignment(hznl, Alignment.MIDDLE_LEFT);
		
		ComboBox<String> cbx = new ComboBox<>() ;
		cbx.setItems("Bizerte","Sousse","Sfax");
		cbx.setValue("Bizerte");
		cbx.addStyleNames(ValoTheme.COMBOBOX_BORDERLESS,ValoTheme.COMBOBOX_TINY);
		
		header.addComponent(cbx);
		header.setComponentAlignment(cbx, Alignment.MIDDLE_RIGHT);
		
		return header;
	}
	
	private Component buildCard(String caption) {
		
		HorizontalLayout hh = new HorizontalLayout() ;
		hh.setCaption(caption);
		
		hh.addStyleName(ValoTheme.LAYOUT_WELL);
		hh.setMargin(new MarginInfo(false, false, true, true));
		ResponsiveLayout responsiveLayout = new ResponsiveLayout();
		
		
        ResponsiveRow row = responsiveLayout.addRow();
        row.setHorizontalSpacing(true);
        
        VerticalLayout vl = new VerticalLayout() ;
        vl.setWidth("100%");
        vl.setMargin(false);
		
        row.addColumn().withDisplayRules(12,12,8,8).withComponent(vl);
        
        int i = 0 ;
        for(KpiE tmp : new KpiD().read()) {
        	if(i < 4) {
        		HorizontalLayout h = new HorizontalLayout() ;
        		Label l = new Label("" + tmp.getName()) ;
        		l.addStyleNames(ValoTheme.LABEL_TINY,ValoTheme.LABEL_BOLD);
        		h.addComponent(l);
        		h.setComponentAlignment(l, Alignment.MIDDLE_LEFT);
        		
        		Label l2 = new Label("0 %") ;
        		l2.addStyleName(ValoTheme.LABEL_TINY);
        		h.addComponent(l2);
        		h.setComponentAlignment(l2, Alignment.MIDDLE_RIGHT);
        		vl.addComponent(h);
        	}
        	i++ ;
        }
        
        hh.addComponent(responsiveLayout);
		return hh ;
	}
	private Component buildChart() {
		
		Panel pane = new Panel("Charts") ;
        VerticalLayout vl = new VerticalLayout() ;
        for(int i = 0 ; i < 4; i++) {
        	HorizontalLayout hznl = new HorizontalLayout() ;
        	hznl.setWidth("100%");
        	Label lbl = new Label("Parameter " + i) ;
        	//lbl.setWidth("100%");
        	lbl.addStyleName(ValoTheme.LABEL_TINY);
        	hznl.addComponent(lbl);
        	//hznl.setExpandRatio(lbl, 1);
        	hznl.setComponentAlignment(lbl, Alignment.MIDDLE_LEFT);
        	TextField tf = new TextField() ;
        	tf.setWidth("100%");
        	tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
        	hznl.addComponent(tf);
        	hznl.setExpandRatio(tf, 1);
        	hznl.setComponentAlignment(tf, Alignment.MIDDLE_RIGHT);
        	vl.addComponent(hznl);
        }
        
        Button btn = new Button("Show All") ;
        btn.addStyleNames(ValoTheme.BUTTON_PRIMARY,ValoTheme.BUTTON_TINY);
        vl.addComponent(btn);
        //vl.setSizeFull();
        pane.setContent(vl);
        return pane;
    }
	private Component chart(int i) {
		
        VerticalLayout vl = new VerticalLayout() ;
        vl.setSpacing(false);
        vl.setMargin(false);

        RadarChartFactory factory = new RadarChartFactory() ;
        DimensionE dim = new DimensionC(new DimensionD().read()).get(i) ;
        KpiValC kpis = selectedItem.kpis()
        		 .filterByDimension(dim.getName())
		 		 .filterByUnit("Percentage") ;

        factory.addLabel(new ArrayList<String>(kpis.narrowProjectionOverKpiName())) ;
        
        //RadarChartBean bean = new RadarChartBean() ;
        RDDataSet dSet = new RDDataSet() ;
        dSet.setName(selectedItem.getName());
            	
    	for(String lbl :kpis.narrowProjectionOverKpiName()) {
    		RDDataGroup grp = new RDDataGroup() ;
    		grp.setLabel(lbl);
    		
    		for(KpiValE tmp : kpis.filterByKpiName(lbl)) {
    			RDDataPoint pt = new RDDataPoint() ;
    			pt.setDate(tmp.getDSample());
    			pt.setName(lbl);
    			pt.setId(tmp.getId());
    			pt.setValue(Double.parseDouble(tmp.getValue()));
    			grp.getPoints().add(pt) ;
    		}
    		dSet.getPoints().add(grp) ;
    	}
    	factory.addBean(dSet) ;
        
    	vl.addComponent(factory.render(dim.getName(),new DataPointClickListener() {
			
			@Override
			public void onDataPointClick(int arg0, int arg1) {	
				String c = factory.getDataPoint(arg0,arg1) ;
				selectedLabel = c ;
				VUI.container.addWindow(new TemporalChart(dSet.findGroupByName(c)),c);
				
			}
		}));

        return vl;
    }
	
	protected class TemporalChart extends TimeSeriesWindow{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private RDDataGroup grp;
		private ChartJs chart;
		private CssLayout lay;
		private VerticalLayout vl;

		public TemporalChart(RDDataGroup grp) {
			
			this.grp = grp ;
			vl = new VerticalLayout() ;
			lay = new CssLayout();
			LineChartConfig config = new LineChartConfig();
			Data<LineChartConfig> dat = config.data().labelsAsList(grp.getDatesAsStrings()) ;
			
			LineDataset dSet = new LineDataset().label(grp.getLabel()) ;
			dSet.borderColor("#36a2eb").backgroundColor("#e7e9ed") ;
			dSet.dataAsList(grp.getValuesAsList()) ;
			dat.addDataset(dSet).and()
            .options()
            .responsive(true)
            .title()
                .display(true)
                .and()
            .elements()
                .line()
                    .fill(FillMode.START)
                    .and()
                .and()
            .done();
			
            chart = new ChartJs(config);
            //chart.setSizeFull();
            chart.setWidth("100%");
            chart.addStyleName("chart-container");
            chart.setJsLoggingEnabled(true);

            lay.addComponent(chart);
            lay.setSizeFull();
	        
	        setHeight("350px");
	        setWidth("500px");
	        center();
		
	        addContent(lay);
		}
		
		public void refresh() {
			LineChartConfig config = new LineChartConfig();
			Data<LineChartConfig> dat = config.data().labelsAsList(grp.getDatesAsStrings()) ;
			
			LineDataset dSet = new LineDataset().label(grp.getLabel()) ;
			dSet.borderColor("#36a2eb").backgroundColor("#e7e9ed") ;
			dSet.dataAsList(grp.getValuesAsList()) ;
			dat.addDataset(dSet).and()
            .options()
            .responsive(true)
            .title()
                .display(true)
                .and()
            .elements()
                .line()
                    .fill(FillMode.START)
                    .and()
                .and()
            .done();
			
            chart = new ChartJs(config);
		}

		@Override
		public void windowClosed(CloseEvent e) {
			
		}

		@Override
		public String caption_() {
			return selectedLabel;
		}

		@Override
		public String width_() {
			return "50%";
		}

		@Override
		public String height_() {
			return "50%";
		}

		@Override
		public void addValueAction() {
			
			menu.setEnabled(false);
			HorizontalLayout hznl = new HorizontalLayout() ;
			//hznl.setWidth("100%");
			
			Label lbl = new Label() ;
			lbl.setIcon(FontAwesome.KEY);
			hznl.addComponent(lbl);
			
			TextField tf = new TextField() ;
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			tf.setPlaceholder("Value");
			tf.setWidth("100%");
			hznl.addComponent(tf);
			
			Button btn = new Button("Add", new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					menu.setEnabled(true);
					lay.removeComponent(hznl);
					
					KpiValE c = selectedItem.kpis().findByKpiName(grp.getLabel()) ;
					KpiValE cc = new KpiValE() ;
					cc.copy(c) ;
					cc.setValue(tf.getValue());
					selectedItem.addKpis(cc);
					
					new CityD().hard_update(selectedItem);
				}
			});
			btn.addStyleName(ValoTheme.BUTTON_TINY);
			hznl.addComponent(btn);
			lay.addComponent(hznl);
			
		}

		@Override
		public void editValueAction() {			
		}
	}
}
