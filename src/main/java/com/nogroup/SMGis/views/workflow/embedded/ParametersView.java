package com.nogroup.SMGis.views.workflow.embedded;

import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ParametersView extends Panel implements View{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private HorizontalLayout header;
	
	public ParametersView() {
		
		addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        DashboardEventBus.register(this);
        
        
        VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setSpacing(false);
        root.addStyleName("dashboard-view");
        setContent(root);
        Responsive.makeResponsive(root);
        
        setContent(new Label("Dimensions View"));
      
	}
}
