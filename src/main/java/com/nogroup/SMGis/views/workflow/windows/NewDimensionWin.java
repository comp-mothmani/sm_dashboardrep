package com.nogroup.SMGis.views.workflow.windows;

import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewDimensionWin extends CrWindow {

	/**
	 * Author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	private DimensionE entity = new DimensionE();
	
	public NewDimensionWin() {
	
		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		Binder<DimensionE> binder = new Binder<>();
		binder.setBean(entity);
		
		TextField tf = new TextField() ;
		tf.setPlaceholder("Name");
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf);
		binder.bind(tf, DimensionE::getName, DimensionE::setName);
		
		
		TextArea tx = new TextArea();
		tx.setPlaceholder("Description");
		tx.setWidth("100%");
		tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx);
		binder.bind(tx, DimensionE::getDescription, DimensionE::setDescription);
		
		
	}

	@Override
	public void windowClosed(CloseEvent e) {
		

	}

	@Override
	public String caption_() {
		return "New Dimension";
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "30%" ;
	}

	@Override
	public void saveAction() {
		new DimensionD().create(entity);
	}

}
