package com.nogroup.SMGis.views.workflow.windows;

import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.business.utils.RandomUtils;
import com.nogroup.SMGis.data.daos.UserD;
import com.nogroup.SMGis.data.entities.ManagerE;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.nogroup.SMGis.views.ccmp.flds.imgUpld.ImageUpload;
import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class AddNewUserWin extends CrWindow {

	/**
	 * Author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private ManagerE entity = new ManagerE() ;

	private ImageUpload img;
	
	public  AddNewUserWin() {
		
		
		VerticalLayout vl = new VerticalLayout() ;
		vl.setSpacing(false);
		addContent(vl);
		
		Binder<UserE> binder = new Binder<>();
		binder.setBean(entity);
	    
		img = new ImageUpload(entity) ;
        vl.addComponent(img);

		TextField tf = new TextField(CResourceBundle.getLocal("editUserWinFname"));

		tf.setPlaceholder("First name");
		tf.setWidth("100%");
		tf.setSizeFull();
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf, UserE::getFName, UserE::setFName);
		vl.addComponent(tf);
		
		
	    TextField tf1 = new TextField(CResourceBundle.getLocal("editUserWinLname")) ;
		tf1.setPlaceholder("Last name");
		tf1.setWidth("100%");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf1, UserE::getLName, UserE::setLName);
		vl.addComponent(tf1);
		
	    TextField tf3 = new TextField(CResourceBundle.getLocal("editUserWinEmail")) ;
	    tf3.setPlaceholder("Email");
		tf3.setWidth("100%");
		tf3.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf3, UserE::getUName, UserE::setUName);
		vl.addComponent(tf3);
		
		TextField tf4 = new TextField(CResourceBundle.getLocal("editUserWinType")) ;
		tf4.setPlaceholder("Type");
		tf4.setEnabled(false);
		tf4.setWidth("100%");
		tf4.addStyleName(ValoTheme.TEXTFIELD_TINY);
		tf4.setValue(entity.getDecriminatorValue());
		vl.addComponent(tf4);
		
		HorizontalLayout hl = new HorizontalLayout();
		hl.setWidth("100%");
		hl.setCaption("Password");
		vl.addComponent(hl);
		
	
		Button btn = new Button("Generate");
		btn.addStyleName("tiny");
		btn.setWidth("100%");
		hl.addComponent(btn);
		
		PasswordField pass_txt = new PasswordField();
		pass_txt.addStyleName(ValoTheme.TEXTFIELD_TINY);
		pass_txt.setWidth("100%");
		binder.bind(pass_txt, UserE::getPwd, UserE::setPwd);
		hl.addComponent(pass_txt);	
		
		btn.addClickListener(e -> {

			pass_txt.setValue(RandomUtils.getAlphaNumericString(6));
		});
		
		vl.addComponent(new Label(""));
		

	}
	
	@Override
	public String caption_() {
		return CResourceBundle.getLocal("addNewUserWin");
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "50%";
	}

	@Override
	public void saveAction() {
		new UserD().create(entity);

	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub

	}

}
