package com.nogroup.SMGis.views.workflow.windows;

import java.util.Date;
import java.util.stream.Collectors;

import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.utils.RandomUtils;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.collections.CountryC;
import com.nogroup.SMGis.data.collections.GouvernorateC;
import com.nogroup.SMGis.data.daos.CityD;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.CountryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.GouvernorateE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.KpiValE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.CountryCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.GouvernorateCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.nogroup.SMGis.views.ccmp.flds.GeometryField;
import com.nogroup.SMGis.views.ccmp.flds.WindowVisibility;
import com.nogroup.SMGis.views.workflow.embedded.MapViewer;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.Setter;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewCityWin extends CrWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private CityE entity = new CityE() ;

	private GeometryField tf3;

	public NewCityWin() {
		
		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		Binder<CityE> binder = new Binder<>();
		binder.setBean(entity);
		
		TextField tf = new TextField() ;
		tf.setPlaceholder(CResourceBundle.getLocal("newCityWinName"));
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf);
		binder.bind(tf, CityE::getName, CityE::setName);
		
		CountryCbx tf1 = new CountryCbx() ;
		tf1.initDemo();
		
		tf1.setPlaceholder(CResourceBundle.getLocal("newCityWinCountry"));
		tf1.setWidth("100%");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf1);
		binder.forField(tf1).bind(new ValueProvider<CityE, CountryE>() {
			
			@Override
			public CountryE apply(CityE source) {
				return new CountryC(
						tf1.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())
						).findByName(source.getCountry()) ;
			}
		}, new Setter<CityE, CountryE>() {
			
			@Override
			public void accept(CityE bean, CountryE fieldvalue) {
				bean.setCountry(fieldvalue.getName());
			}
		});
		
		
		GouvernorateCbx tf2 = new GouvernorateCbx() ;
		
		tf2.setPlaceholder(CResourceBundle.getLocal("newCityWinGov"));
		tf2.setWidth("100%");
		tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf2);
		binder.forField(tf2).bind(new ValueProvider<CityE, GouvernorateE>() {
			
			@Override
			public GouvernorateE apply(CityE source) {
				return new GouvernorateC(
						tf2.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())
						).findByName(source.getGov()) ;
			}
		}, new Setter<CityE, GouvernorateE>() {
			
			@Override
			public void accept(CityE bean, GouvernorateE fieldvalue) {
				bean.setCountry(fieldvalue.getName());
			}
		});
		
		tf1.addSelectionListener(new SingleSelectionListener<CountryE>() {
			
			@Override
			public void selectionChange(SingleSelectionEvent<CountryE> event) {
				CountryE country = event.getSelectedItem().get() ;
				tf2.switch2(country.getName()) ;
			}
		});

		tf3 = new GeometryField(new WindowVisibility() {

			@Override
			public void visible() {
				NewCityWin.this.setVisible(true);
			}

			@Override
			public void invisible() {
				NewCityWin.this.setVisible(false);
			}
			
		}) ;
		tf3.setWidth("100%");
		vl.addComponent(tf3);
		
		
		
	}
	@Override
	public String caption_() {
		return CResourceBundle.getLocal("newCityBarMenu");
	}
	@Override
	public String width_() {
		return "30%";
	}
	@Override
	public String height_() {
		return "35%";
	}
	@Override
	public void saveAction() {
		entity.setGeometry(tf3.getPoly());
		int i = 0 ;
		for(DimensionE tmp : new DimensionD().read()) {
			for(SubDimensionE tmp2 : tmp.getSubDimenstions()) {
				for(CategoryE tmp3 : tmp2.getCategories()) {
					for(KpiE tmp4 : tmp3.getKpis()) {
						KpiValE val = new KpiValE() ;
						val.setDimension(tmp.getName());
						val.setSubDimension(tmp2.getName());
						val.setCategory(tmp3.getName());
						val.setKpiName(tmp4.getName());
						val.setDSample(new Date());
						val.setDCreated(new Date());
						val.setUnit(tmp4.getUnit());
						val.setDSample(new Date());
						val.setValue("" + RandomUtils.getRandomNumberInRange(10, 40));
						
						i++ ;
						entity.addKpis(val);
					}
				}
			}
		}
		new CityD().create(entity) ;
	}
	@Override
	public void windowClosed(CloseEvent e) {
		MapViewer viewer = (MapViewer)UI.getCurrent().getNavigator().getCurrentView() ;
		viewer.clearSelectionLayer() ;
	}

}
