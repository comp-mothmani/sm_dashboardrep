package com.nogroup.SMGis.views.workflow.windows;

import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.collections.UserC;
import com.nogroup.SMGis.data.daos.UserD;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.nogroup.SMGis.views.ccmp.flds.imgUpld.ImageUpload;
import com.vaadin.data.Binder;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class EditUserWin extends CrWindow{
	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private UserE entity = new UserE();
	private ImageUpload img;


	public EditUserWin() {

		entity =  new UserC().fromDB().findByUName(getCurrentUser());	
		System.out.println(entity.getUName());
		VerticalLayout vl = new VerticalLayout() ;
		vl.setSpacing(false);
		addContent(vl);
		
		Binder<UserE> binder = new Binder<>();
		binder.setBean(entity);
	    
		img = new ImageUpload(entity) ;
        vl.addComponent(img);

		TextField tf = new TextField(CResourceBundle.getLocal("editUserWinFname"));

		tf.setPlaceholder("First name");
		tf.setWidth("100%");
		tf.setSizeFull();
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf, UserE::getFName, UserE::setFName);
		vl.addComponent(tf);
		
		
	    TextField tf1 = new TextField(CResourceBundle.getLocal("editUserWinLname")) ;
		tf1.setPlaceholder("Last name");
		tf1.setWidth("100%");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf1, UserE::getLName, UserE::setLName);
		vl.addComponent(tf1);
		
	    TextField tf3 = new TextField(CResourceBundle.getLocal("editUserWinEmail")) ;
	    tf3.setPlaceholder("Email");
		tf3.setWidth("100%");
		tf3.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf3, UserE::getUName, UserE::setUName);
		vl.addComponent(tf3);
		
		TextField tf4 = new TextField(CResourceBundle.getLocal("editUserWinType")) ;
		tf4.setPlaceholder("Type");
		tf4.setEnabled(false);
		tf4.setWidth("100%");
		tf4.addStyleName(ValoTheme.TEXTFIELD_TINY);
		tf4.setValue(entity.getDecriminatorValue());
		vl.addComponent(tf4);
		
		vl.addComponent(new Label(""));
		
		

	}

	@Override
	public String caption_() {
		return CResourceBundle.getLocal("editUserBarMenu");
	}

	@Override
	public String width_() {
		return  "35%";
	}

	@Override
	public String height_() {
		return  "55%";

	}

	@Override
	public void saveAction() {
		entity.setImg(img.getEncodedString());
		new UserD().hard_update(entity);

	}

	@Override
	public void windowClosed(CloseEvent e) {
		
		
		
	}
	
	
	private String getCurrentUser() {
        return (String) VaadinSession.getCurrent()
                .getAttribute("usr");
    }
	
	

}
