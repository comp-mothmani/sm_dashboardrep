package com.nogroup.SMGis.views.workflow.windows;

import java.util.Date;

import com.nogroup.SMGis.data.daos.CategoryD;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.daos.SubDimensionD;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.CategoryCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.DimensionCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.KpiUnitCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.SubDimensionCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewKpiWin extends CrWindow {
	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private KpiE entity = new KpiE();
	private DimensionCbx cbx;
	private SubDimensionCbx cbx1;
	private CategoryCbx cbx2;
	
	public  NewKpiWin() {
		
		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		Binder<KpiE> binder = new Binder<>();
		binder.setBean(entity);
		
		TextField tf = new TextField() ;
		tf.setPlaceholder("Name");
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf);
		binder.bind(tf, KpiE::getName, KpiE::setName);
		
		TextField tf2 = new TextField() ;
		tf2.setPlaceholder("Code");
		tf2.setWidth("100%");
		tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf2);
		binder.bind(tf2, KpiE::getCode, KpiE::setCode);
		
		cbx = new DimensionCbx(new DimensionD().read());
		vl.addComponent(cbx);
		
		cbx1 = new SubDimensionCbx();
		vl.addComponent(cbx1);
		
		cbx2 = new CategoryCbx();
		cbx2.setPlaceholder("Category");
		vl.addComponent(cbx2);
		
		cbx.addValueChangeListener(new ValueChangeListener<DimensionE>() {
					
			@Override
			public void valueChange(ValueChangeEvent<DimensionE> event) {
				cbx1.setItems(event.getValue().getSubDimenstions());
				cbx1.setValue(null);
			}
		});
		
		cbx1.addValueChangeListener(new ValueChangeListener<SubDimensionE>() {
			
			@Override
			public void valueChange(ValueChangeEvent<SubDimensionE> event) {
				cbx2.setItems(event.getValue().getCategories());
				cbx2.setValue(null);
			}
		});
		
		TextArea tx = new TextArea();
		tx.setPlaceholder("Description");
		tx.setWidth("100%");
		tx.setHeight("55px");
		tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx);
		binder.bind(tx, KpiE::getDescription, KpiE::setDescription);

		KpiUnitCbx cbx3 = new KpiUnitCbx();
		vl.addComponent(cbx3);
		binder.bind(cbx3, KpiE::getUnit, KpiE::setUnit);
		
		TextArea tx2 = new TextArea();
		tx2.setPlaceholder("Methodology");
		tx2.setWidth("100%");
		tx2.setHeight("55px");
		tx2.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx2);
		binder.bind(tx2, KpiE::getMethodology, KpiE::setMethodology);
		
		TextArea tx3 = new TextArea();
		tx3.setPlaceholder("SDG");
		tx3.setWidth("100%");
		tx3.setHeight("55px");
		tx3.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx3);
		binder.bind(tx3, KpiE::getSdg, KpiE::setSdg);
		
		
		TextArea tx4 = new TextArea();
		tx4.setPlaceholder("Data Source");
		tx4.setWidth("100%");
		tx4.setHeight("55px");
		tx4.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx4);
		binder.bind(tx4, KpiE::getdSource, KpiE::setdSource);

	}

	@Override
	public String caption_() {
		return "New Kpi";
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "90%";
	}

	@Override
	public void saveAction() {
		
		entity.setDCreated(new Date());
		cbx2.getValue().addKpis(entity);
		new CategoryD().hard_update(cbx2.getValue());
		//new DimensionD().hard_update(cbx.getValue());
	}

	@Override
	public void windowClosed(CloseEvent e) {
		
	}
	
}