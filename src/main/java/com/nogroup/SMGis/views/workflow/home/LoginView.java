package com.nogroup.SMGis.views.workflow.home;

import com.google.common.eventbus.Subscribe;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.events.DashboardEvent.UserLoginRequestedEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.WrongCredentialsEvent;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class LoginView extends VerticalLayout {

    private CheckBox chbx;
	private TextField username;
	private PasswordField password;

	public LoginView() {

		DashboardEventBus.register(this);
		
        setSizeFull();
        setMargin(false);
        setSpacing(false);

        Component loginForm = buildLoginForm();
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

        Notification notification = new Notification(
                CResourceBundle.getLocal("welcomeNotificationTitle"));
        notification
                .setDescription("<span>This application is not real, it only demonstrates an application built with the <a href=\"https://vaadin.com\">Vaadin framework</a>.</span> <span>No username or password is required, just click the <b>Sign In</b> button to continue.</span>");
        notification.setHtmlContentAllowed(true);
        notification.setStyleName("tray dark small closable login-help");
        notification.setPosition(Position.BOTTOM_CENTER);
        notification.setDelayMsec(20000);
        notification.show(Page.getCurrent());
        
    }

    private Component buildLoginForm() {
        final VerticalLayout loginPanel = new VerticalLayout();
        loginPanel.setSizeUndefined();
        loginPanel.setMargin(false);
        Responsive.makeResponsive(loginPanel);
        loginPanel.addStyleName("login-panel");

        loginPanel.addComponent(buildLabels());
        loginPanel.addComponent(buildFields());
        
        chbx = new CheckBox(CResourceBundle.getLocal("remeberMeChbtn"), true) ;
        loginPanel.addComponent(chbx);
        
        ComboBox<String> lang = new ComboBox<String>(CResourceBundle.getLocal("langCBX")) ;
        lang.setEmptySelectionAllowed(false);
        lang.addSelectionListener(new SingleSelectionListener<String>() {
			
			@Override
			public void selectionChange(SingleSelectionEvent<String> event) {
				if(event.getValue().equals("English")) {
					VaadinSession.getCurrent().setAttribute("lang", "en");
				}else if(event.getValue().equals("Français")) {
					VaadinSession.getCurrent().setAttribute("lang", "fr");
				}else {
					VaadinSession.getCurrent().setAttribute("lang", "ar");
				}
				VUI ui = (VUI) getUI() ;
				if(ui != null) {
					ui.setContent(new LoginView());
				}
				
			}
		});
        lang.setItems("English","Français","العربية");
        
        if(VaadinSession.getCurrent().getAttribute("lang").equals("ar")) {
        	lang.setValue("العربية");
        }else if(VaadinSession.getCurrent().getAttribute("lang").equals("fr")) {
        	lang.setValue("Français");
        }else {
        	lang.setValue("English");
        }
        
        lang.setWidth("100%");
        lang.setIcon(FontAwesome.LANGUAGE);

        loginPanel.addComponent(lang);
        return loginPanel;
    }

    private Component buildFields() {
        HorizontalLayout fields = new HorizontalLayout();
        fields.addStyleName("fields");

        username = new TextField(CResourceBundle.getLocal("usernameTF"));
        username.setIcon(FontAwesome.USER);
        username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        username.setValue("medzied.arbi@admin.com");

        password = new PasswordField(CResourceBundle.getLocal("passwordTF"));
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        password.setValue("000");

        final Button signin = new Button(CResourceBundle.getLocal("loginBtn"));
        signin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        signin.setClickShortcut(KeyCode.ENTER);
        signin.focus();

        
        
        fields.addComponents(username, password, signin);
        fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);

        signin.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                DashboardEventBus.post(new UserLoginRequestedEvent(username
                        .getValue(), password.getValue()));
            }
        });
        
        
        return fields;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");

        Label welcome = new Label(CResourceBundle.getLocal("welcomeLBL"));
        welcome.setSizeUndefined();
        welcome.addStyleName(ValoTheme.LABEL_H4);
        welcome.addStyleName(ValoTheme.LABEL_COLORED);
        labels.addComponent(welcome);

        Label title = new Label(CResourceBundle.getLocal("appNameLBL"));
        title.setSizeUndefined();
        title.addStyleName(ValoTheme.LABEL_H3);
        title.addStyleName(ValoTheme.LABEL_LIGHT);
        labels.addComponent(title);
        return labels;
    }

    @Subscribe
    public void wrongCredentials(final WrongCredentialsEvent event) {
		username.clear();
		password.clear();
		try {
			Notification notification = new Notification(
					CResourceBundle.getLocal("wrongCredentials"),Notification.TYPE_ERROR_MESSAGE);
			/*
	        notification
	                .setDescription("<span>This application is not real, it only demonstrates an application built with the <a href=\"https://vaadin.com\">Vaadin framework</a>.</span> <span>No username or password is required, just click the <b>Sign In</b> button to continue.</span>");
	        */
			notification.setDescription(CResourceBundle.getLocal("wrongCredentialsDescription")) ;
			notification.setHtmlContentAllowed(true);
	        notification.setPosition(Position.MIDDLE_CENTER);
	        notification.show(Page.getCurrent());
	        
			//getUI().showNotification(CResourceBundle.getLocal("wrongCredentials"),Notification.TYPE_ERROR_MESSAGE);
		}catch (Exception e) {
		}
		
	}
}
