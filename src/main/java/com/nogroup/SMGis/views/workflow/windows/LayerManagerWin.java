package com.nogroup.SMGis.views.workflow.windows;

import org.vaadin.addon.vol3.layer.OLTileLayer;
import org.vaadin.addon.vol3.source.OLTileXYZSource;

import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.nogroup.SMGis.views.workflow.embedded.MapViewer;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.ItemClick;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;

public class LayerManagerWin extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private MapViewer viewer;

	public LayerManagerWin(MapViewer mapViewer) {
		this.viewer = mapViewer;
		
		caption("Layer Manager");
		center();
		setWidth("50%");
		setHeight("70%");
		VerticalLayout vl = new VerticalLayout() ;
		
		TabSheet tabs = new TabSheet() ;
		tabs.addTab(buildTilesTab(),"Tile Layers") ;
		
		vl.addComponent(tabs);
		addContent(vl);
	}
	
	public VerticalLayout buildTilesTab() {
		VerticalLayout vl = new VerticalLayout();
		
		Grid<String> grid = new Grid<String>() ;
		grid.setWidth("100%");
		grid.setHeight("300px");
		grid.setHeaderVisible(false);
		grid.addColumn(String::toUpperCase).setCaption("Name");
		grid.setSelectionMode(SelectionMode.SINGLE) ;
		
		//grid.setDataProvider(DataProvider.ofCollection(this.viewer.getTileLayers().values()));
		
		ContextMenu contextMenu = new ContextMenu(grid, true);
		contextMenu.addItem("Add Layer", e -> {
			VerticalLayout vl2 = new VerticalLayout() ;
			vl2.addStyleNames(ValoTheme.LAYOUT_WELL);
			
			TextField tf0 = new TextField() ;
			tf0.setPlaceholder("Name");
			tf0.setWidth("100%");
			tf0.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl2.addComponent(tf0);
			
			TextField tf1 = new TextField() ;
			tf1.setPlaceholder("Name");
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl2.addComponent(tf1);
			
			HorizontalLayout hznl = new HorizontalLayout() ;
			Button btn = new Button("Add") ;
			btn.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					OLTileXYZSource xyz = new OLTileXYZSource();
					xyz.setUrlTemplate(tf1.getValue());
					OLTileLayer layer = new OLTileLayer(xyz) ;
					layer.setTitle(tf0.getValue());
					
					viewer.getMap().addLayer(layer);
					viewer.getTileLayers().put(tf0.getValue(), layer) ;
					//grid.setDataProvider(DataProvider.ofCollection(viewer.getTileLayers().values()));
					vl.removeComponent(vl2);
				}
			});
			btn.addStyleNames(ValoTheme.BUTTON_FRIENDLY,ValoTheme.BUTTON_TINY);
			hznl.addComponent(btn);
			
			Button btn1 = new Button("Cancel") ;
			btn1.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					vl.removeComponent(vl2);
				}
			});
			btn1.addStyleNames(ValoTheme.BUTTON_DANGER,ValoTheme.BUTTON_TINY);
			hznl.addComponent(btn1);
			vl2.addComponent(hznl);
			
			vl.addComponent(vl2, 0);
		});
		
		contextMenu.addItem("Remove Layer", e -> {
			
		});
		
		vl.addComponent(grid);
		
		return vl ;
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

}
