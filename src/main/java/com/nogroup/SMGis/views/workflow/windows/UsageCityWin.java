package com.nogroup.SMGis.views.workflow.windows;

import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class UsageCityWin extends CWindow {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	public  UsageCityWin() {
		
		VerticalLayout vl = new VerticalLayout();
		addContent(vl);
		
		Label lbl = new Label(
			    "In HTML mode, all HTML formatting tags, such as \n" +
			    "<ul>"+
			    "  <li><b>bold</b></li>"+
			    "  <li>itemized lists</li>"+
			    "  <li>etc.</li>"+
			    "</ul> "+
			    "are preserved.",
			    ContentMode.HTML);
		
		lbl.setSizeFull();
		vl.addComponent(lbl);
	}
	
	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
