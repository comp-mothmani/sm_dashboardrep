package com.nogroup.SMGis.views.workflow.windows;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.vaadin.simplefiledownloader.SimpleFileDownloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.SMGis.business.server.FullHttpResponse;
import com.nogroup.SMGis.business.server.HttpRequester;
import com.nogroup.SMGis.data.daos.KpiD;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.serialization.pojos.kpi.KpiSet;
import com.nogroup.SMGis.data.serialization.pojos.kpi.KpiReport;
import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.qkyrie.markdown2pdf.Markdown2PdfConverter;
import com.qkyrie.markdown2pdf.internal.exceptions.ConversionException;
import com.qkyrie.markdown2pdf.internal.exceptions.Markdown2PdfLogicException;
import com.qkyrie.markdown2pdf.internal.writing.Markdown2PdfWriter;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ExportPdfKpiWin extends CWindow{
	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	private Button btn;
	private TextArea txtar; 
	private TextField txt_name;
	
	
	public ExportPdfKpiWin() {

		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		
		caption("Generate PDF");
		setWidth("35%");
		setHeight("45%");
		
		txt_name = new TextField("Report Name");
		txt_name.addStyleName("tiny");
		txt_name.setWidth("100%");
		vl.addComponent(txt_name);
		
		
		txtar = new TextArea("Report Description");
		txtar.addStyleName("tiny");
		txtar.setWidth("100%");
		vl.addComponent(txtar);
		
		btn = new Button("GENERATE PDF");
		btn.addStyleName("tiny");
		vl.addComponent(btn);
		vl.setComponentAlignment(btn, Alignment.BOTTOM_RIGHT);
		
		btn.addClickListener(e -> {
			generateReport();
			
		});

		
	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private String getCurrentUser() {
        return (String) VaadinSession.getCurrent()
                .getAttribute("usr");
    }
	
	public void generateReport() {
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		String strDate = dateFormat.format(date);
		
		KpiReport creport = new KpiReport();
		creport.setDate(strDate);
		creport.setDescription(txtar.getValue());
		creport.setReportName(txt_name.getValue());
		creport.setLogo("");
		creport.setQrCode("");
		creport.setUserName(getCurrentUser());
		
		for (KpiE tmp : new KpiD().read()) {
			
			KpiSet kpi = new KpiSet();
			
			kpi.setName(tmp.getName());
			kpi.setDescription(tmp.getDescription());
			kpi.setDimension(tmp.getCategory().getSubDimension().getDimension().getName());
			kpi.setSubDimension(tmp.getCategory().getSubDimension().getName());
			kpi.setCategory(tmp.getCategory().getName());
			kpi.setUnit(tmp.getUnit());
			kpi.setMethodology(tmp.getMethodology() + "");
			kpi.setSdg(tmp.getSdg());
			kpi.setSource(tmp.getdSource());
			
			creport.getKpiSet().add(kpi);
		}
		
		// Creating Object of ObjectMapper define in Jackson Api 
        ObjectMapper Obj = new ObjectMapper(); 
        try { 
            String jsonStr = Obj.writeValueAsString(creport); 
            HttpRequester httpRequester = new HttpRequester();
            
            System.out.println(jsonStr);

    		String url = "http://localhost:5000/exportKpiReport";

    		Map<String, String> params = new LinkedHashMap<String, String>();
    		params.put("data", jsonStr) ;
    		FullHttpResponse response = httpRequester.call(url, "POST", params);
    		String resp = response.getResponse();
    		
    		HashMap<String,String> hash = Obj.readValue(resp, HashMap.class) ;
    		
    		try {
				Markdown2PdfConverter
				.newConverter()
				.readFrom(() -> hash.get("md"))
				.writeTo(new Markdown2PdfWriter() {
					
					@Override
					public void write(byte[] out) {
						SimpleFileDownloader downloader = new SimpleFileDownloader();
						addExtension(downloader);
						
						final StreamResource resource = new StreamResource(() -> {
							
							return new ByteArrayInputStream(out);
						}, "export.pdf");
						
						downloader.setFileDownloadResource(resource);
						downloader.download();
						
						
					}
				})
				.doIt();
			} catch (ConversionException | Markdown2PdfLogicException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
        } 
        catch (IOException e) { 
            e.printStackTrace(); 
        } 


        
	}

}
