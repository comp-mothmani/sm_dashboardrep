package com.nogroup.SMGis.views.workflow.embedded;

import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.data.collections.CityC;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.views.ccmp.cbxs.CityCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.nogroup.SMGis.views.ccmp.flds.GeometryField;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.ItemClick;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;

public class CitiesView extends MapViewer implements View{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private HorizontalLayout header;
	private CityCbx cityCbx;
	private CityC cities;
	
	public CitiesView() {
		
		initData();
		
		HorizontalLayout hznl = new HorizontalLayout() ;
		//hznl.setWidth("100%");
		hznl.setHeight("35px");
		addComponent(hznl,0);
		setComponentAlignment(hznl, Alignment.MIDDLE_RIGHT);
		//setExpandRatio(hznl, 1);
		
		setExpandRatio(getMap(), 1);
		
		Button btn = new Button() ;
		btn.setIcon(FontAwesome.PLUS);
		btn.addStyleNames(ValoTheme.BUTTON_FRIENDLY,ValoTheme.BUTTON_TINY);
		hznl.addComponent(btn);
		btn.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VUI.container.addWindow(new CreateCityView(), "CREATE CITY");
				
			}
		});
		
		btn = new Button() ;
		btn.setIcon(FontAwesome.MINUS);
		btn.addStyleNames(ValoTheme.BUTTON_DANGER,ValoTheme.BUTTON_TINY);
		hznl.addComponent(btn);
		
		btn = new Button() ;
		btn.setIcon(FontAwesome.SEARCH);
		btn.addStyleNames(ValoTheme.BUTTON_PRIMARY,ValoTheme.BUTTON_TINY);
		hznl.addComponent(btn);
		btn.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VUI.container.addWindow(new ListCityView(), "CITIES");
			}
		});
		
		cityCbx = new CityCbx(cities) ;
		cityCbx.addValueChangeListener(new ValueChangeListener<CityE>() {
			
			@Override
			public void valueChange(ValueChangeEvent<CityE> event) {
				CitiesView.this.center(event.getValue().getCentroid()) ;
			}
		});
		hznl.addComponent(cityCbx);
	}
	
	public void initData() {
		cities = new CityC() ;
		for(int i = 0 ;i < 10 ; i++) {
			CityE city = new CityE() ;
			VPoint vp = new VPoint() ;
			vp.setLat(34.025848 + i*0.01);
			vp.setLon(19.2588+ i*0.01);
			city.setCentroid(vp);
			city.setName("City " + i);
			cities.add(city) ;
		}
	}
	protected class ListCityView extends CWindow{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private Grid<CityE> grid = new Grid<>() ;
		
		public ListCityView() {
			
			setHeight("75%");
			layout.setCaption("CITIES");
			VerticalLayout vl = new VerticalLayout() ;
			
			TextField tf = new TextField() ;
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			tf.setWidth("100%");
			tf.setPlaceholder("Search ...");
			vl.addComponent(tf);
			
			tf.addValueChangeListener(new ValueChangeListener<String>() {
				
				@Override
				public void valueChange(ValueChangeEvent<String> event) {
					
				}
			});
			
			grid.setSizeFull();
			grid.addColumn(CityE::getName).setCaption("Name");
			grid.setSelectionMode(SelectionMode.SINGLE) ;
			
			grid.setDataProvider(DataProvider.ofCollection(cities));
			grid.addItemClickListener(new ItemClickListener<CityE>() {

				@Override
				public void itemClick(ItemClick<CityE> event) {
					CitiesView.this.center(event.getItem().getCentroid()) ;
					cityCbx.setValue(event.getItem()) ;
				}
			});
			vl.addComponent(grid);
			
			vl.addComponent(new Label());
			addContent(vl);
			
		}
		@Override
		public void windowClosed(CloseEvent e) {
			
		}
		
	}

	protected class CreateCityView extends CWindow{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private CityE city;
		
		public CreateCityView() {
			setHeight("75%");
			layout.setCaption("CREATE CITY");
			VerticalLayout vl = new VerticalLayout() ;

			city = new CityE() ;
			addContent(vl);
			Binder<CityE> binder = new Binder<>();
			binder.setBean(city);
			
			
			TextField tf = new TextField() ;
			tf.setPlaceholder("Name");
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl.addComponent(tf);
			binder.bind(tf, CityE::getName, CityE::setName);
			
			TextField tf1 = new TextField() ;
			tf1.setPlaceholder("Country");
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl.addComponent(tf1);
			binder.bind(tf1, CityE::getCountry, CityE::setCountry);
			
			TextField tf2 = new TextField() ;
			tf2.setPlaceholder("Gouvernorate");
			tf2.setWidth("100%");
			tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl.addComponent(tf2);
			binder.bind(tf2, CityE::getGov, CityE::setGov);
			
			GeometryField tf3 = new GeometryField(CitiesView.this) ;
			tf3.setWidth("100%");
			tf3.addStyleName(ValoTheme.TEXTFIELD_TINY);
			vl.addComponent(tf3);
			//binder.bind(tf3, CityE::getGov, CityE::setGov);
			
		}
		@Override
		public void windowClosed(CloseEvent e) {
			
		}
		
	}
}
