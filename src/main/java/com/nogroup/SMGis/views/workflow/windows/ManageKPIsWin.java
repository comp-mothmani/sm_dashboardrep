package com.nogroup.SMGis.views.workflow.windows;

import com.vaadin.ui.Label;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.CategoryCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.DimensionCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.KpiUnitCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.SubDimensionCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.MaWindow;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.data.TreeData;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ManageKPIsWin extends MaWindow implements ItemClickListener<TreeBean> {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	
	
	public ManageKPIsWin() {
		initData();
		initTree(treeData,this) ;
		getPane().setCaption(CResourceBundle.getLocal("maWindowKpis"));

	}
	
	
	@Override
	public String caption_() {
		return "Manage KPIs";
	}



	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "50%";
	}

	@Override
	public void saveAction() {
		if(selectedItem instanceof DimensionE) {
			new DimensionD().hard_update((DimensionE) selectedItem);
		}else if(selectedItem instanceof SubDimensionE) {
			new DimensionD().hard_update(((SubDimensionE)selectedItem).getDimension());
		}else if(selectedItem instanceof CategoryE) {
			new DimensionD().hard_update(((CategoryE)selectedItem).getSubDimension().getDimension());
		}else if(selectedItem instanceof KpiE) {
			new DimensionD().hard_update(((KpiE)selectedItem).getCategory().getSubDimension().getDimension());
		}
		
		initData();
		initTree(treeData,this) ;
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

	@Override
	public void deleteAction() {
		if(selectedItem instanceof DimensionE) {
			new DimensionD().delete(((DimensionE) selectedItem).getId());
		}else if(selectedItem instanceof SubDimensionE) {
			DimensionE dm = ((SubDimensionE)selectedItem).getDimension() ;
			dm.removeSubDimenstions((SubDimensionE) selectedItem);
			new DimensionD().hard_update(dm);
		}else if(selectedItem instanceof CategoryE) {
			SubDimensionE dm = ((CategoryE)selectedItem).getSubDimension() ;
			dm.removeCategories((CategoryE) selectedItem);
			new DimensionD().hard_update(dm.getDimension());
		}else if(selectedItem instanceof KpiE) {
			CategoryE dm = ((KpiE)selectedItem).getCategory() ;
			dm.removeKpis((KpiE) selectedItem);
			new DimensionD().hard_update(dm.getSubDimension().getDimension());
		}
		
		initData();
		initTree(treeData,this) ;
	}


	@Override
	public void initData() {
		treeData.clear() ;
		for(DimensionE tmp : new DimensionD().read()) {
			treeData.addItem(null,tmp);
			for(SubDimensionE tmp2 : tmp.getSubDimenstions()) {
				treeData.addItem(tmp,tmp2);
				for(CategoryE tmp3 : tmp2.getCategories()) {
					treeData.addItem(tmp2,tmp3);
					for(KpiE tmp4 : tmp3.getKpis()) {
						treeData.addItem(tmp3,tmp4);
					}
				}
			}
		}
	}


	@Override
	public void switch2() {
		setDetails(new EmbeddedView());
	}
	
	protected class EmbeddedView extends VerticalLayout{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		
		public EmbeddedView() {
			if(selectedItem == null) {
				return ;
			}else {
				if(selectedItem instanceof DimensionE) {
					new DimensionView(this) ;
				}else if(selectedItem instanceof SubDimensionE) {
					new SubDimensionView(this) ;
				}else if(selectedItem instanceof CategoryE) {
					new CategoryView(this) ;
				}else if(selectedItem instanceof KpiE) {
					new KPIView(this) ;
				}
			}
		}
		
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {
		selectedItem = event.getItem() ;
		switch2();
	}
	
	protected class DimensionView{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;

		public DimensionView(EmbeddedView root) {
			
			Binder<DimensionE> binder = new Binder<>();
			binder.setBean((DimensionE) selectedItem);
			
			TextField tf = new TextField("Name") ;
			tf.setPlaceholder("Name");
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf);
			binder.bind(tf, DimensionE::getName, DimensionE::setName);
			
			
			TextArea tx = new TextArea("Description");
			tx.setPlaceholder("Description");
			tx.setWidth("100%");
			tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx);
			binder.bind(tx, DimensionE::getDescription, DimensionE::setDescription);
		}
	}
	
	protected class SubDimensionView{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;

		public SubDimensionView(EmbeddedView root) {
			
			Binder<SubDimensionE> binder = new Binder<>();
			binder.setBean((SubDimensionE) selectedItem);
			
			TextField tf = new TextField("Name") ;
			tf.setPlaceholder("Name");
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf);
			binder.bind(tf, SubDimensionE::getName, SubDimensionE::setName);
		
			DimensionCbx cbx = new DimensionCbx("Dimension").fromDB();
			binder.forField(cbx).bind(new ValueProvider<SubDimensionE, DimensionE>() {
				
				@Override
				public DimensionE apply(SubDimensionE source) {
					return source.getDimension() ;
				}
			}, new Setter<SubDimensionE, DimensionE>() {
				
				@Override
				public void accept(SubDimensionE bean, DimensionE fieldvalue) {
					DimensionE dim = bean.getDimension() ;
					dim.removeSubDimenstions(bean);
					fieldvalue.addSubDimenstions(bean);
				}
			});
			root.addComponent(cbx);

			TextArea tx = new TextArea("Description");
			tx.setPlaceholder("Description");
			tx.setWidth("100%");
			tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx);
			binder.bind(tx, SubDimensionE::getDescription, SubDimensionE::setDescription);
			
		}
	}
	
	protected class CategoryView{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private DimensionCbx cbx1;
		private SubDimensionCbx cbx2;

		public CategoryView(EmbeddedView root) {
			
			Binder<CategoryE> binder = new Binder<>();
			binder.setBean((CategoryE) selectedItem);
			
			TextField tf = new TextField("Name") ;
			tf.setPlaceholder("Name");
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf);
			binder.bind(tf, CategoryE::getName, CategoryE::setName);
		
			cbx1 = new DimensionCbx("Dimension").fromDB();
			//root.addComponent(cbx1);
			cbx1.addValueChangeListener(new ValueChangeListener<DimensionE>() {
				
				@Override
				public void valueChange(ValueChangeEvent<DimensionE> event) {
					cbx2.setItems(event.getValue().getSubDimenstions());
					cbx2.setValue(null);
				}
			});
			
			
			cbx2 = new SubDimensionCbx("Sub-Dimension");
			binder.forField(cbx2).bind(new ValueProvider<CategoryE, SubDimensionE>() {
				
				@Override
				public SubDimensionE apply(CategoryE source) {
					return source.getSubDimension() ;
				}
			}, new Setter<CategoryE, SubDimensionE>() {
				
				@Override
				public void accept(CategoryE bean, SubDimensionE fieldvalue) {
					SubDimensionE dim = bean.getSubDimension() ;
					dim.removeCategories(bean);
					fieldvalue.addCategories(bean);
				}
			});
			root.addComponent(cbx2);

			TextArea tx = new TextArea("Description");
			tx.setPlaceholder("Description");
			tx.setWidth("100%");
			tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx);
			binder.bind(tx, CategoryE::getDescription, CategoryE::setDescription);
			
		}
	}

	protected class KPIView{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private SubDimensionCbx cbx1;
		private CategoryCbx cbx2;
		private DimensionCbx cbx;

		public KPIView(EmbeddedView root) {

			Binder<KpiE> binder = new Binder<>();
			binder.setBean((KpiE) selectedItem);
			
			TextField tf = new TextField("Name") ;
			tf.setPlaceholder("Name");
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf);
			binder.bind(tf, KpiE::getName, KpiE::setName);
			
			TextField tf2 = new TextField() ;
			tf2.setPlaceholder("Code");
			tf2.setCaption("Code");
			tf2.setWidth("100%");
			tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf2);
			binder.bind(tf2, KpiE::getCode, KpiE::setCode);
			
			cbx = new DimensionCbx(new DimensionD().read());
			cbx.setCaption("Dimension");
			//root.addComponent(cbx);

			cbx1 = new SubDimensionCbx("Sub-Dimension");
			//root.addComponent(cbx1);

			cbx2 = new CategoryCbx("Category");
			root.addComponent(cbx2);
			binder.forField(cbx2).bind(new ValueProvider<KpiE, CategoryE>() {
				
				@Override
				public CategoryE apply(KpiE source) {
					return source.getCategory() ;
				}
			}, new Setter<KpiE, CategoryE>() {
				
				@Override
				public void accept(KpiE bean, CategoryE fieldvalue) {
					CategoryE dim = bean.getCategory() ;
					dim.removeKpis(bean);
					fieldvalue.addKpis(bean);
				}
			});
			
			
			cbx.addValueChangeListener(new ValueChangeListener<DimensionE>() {
						
				@Override
				public void valueChange(ValueChangeEvent<DimensionE> event) {
					cbx1.setItems(event.getValue().getSubDimenstions());
					cbx1.setValue(null);
				}
			});

			cbx1.addValueChangeListener(new ValueChangeListener<SubDimensionE>() {
				
				@Override
				public void valueChange(ValueChangeEvent<SubDimensionE> event) {
					cbx2.setItems(event.getValue().getCategories());
					cbx2.setValue(null);
				}
			});
			
			TextArea tx = new TextArea("Description");
			tx.setPlaceholder("Description");
			tx.setCaption("Description");
			tx.setWidth("100%");
			tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx);
			binder.bind(tx, KpiE::getDescription, KpiE::setDescription);

			KpiUnitCbx cbx3 = new KpiUnitCbx();
			cbx3.setCaption("Unit");
			root.addComponent(cbx3);
			binder.bind(cbx3, KpiE::getUnit, KpiE::setUnit);
			
			TextArea tx2 = new TextArea();
			tx2.setCaption("Methodology");
			tx2.setPlaceholder("Methodology");
			tx2.setWidth("100%");
			tx2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx2);
			binder.bind(tx2, KpiE::getMethodology, KpiE::setMethodology);
			
			TextArea tx3 = new TextArea();
			tx3.setCaption("SDG");
			tx3.setPlaceholder("SDG");
			tx3.setWidth("100%");
			tx3.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx3);
			binder.bind(tx3, KpiE::getSdg, KpiE::setSdg);
			
			
			TextArea tx4 = new TextArea();
			tx4.setCaption("Data Source");
			tx4.setPlaceholder("Data Source");
			tx4.setWidth("100%");
			tx4.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tx4);
			binder.bind(tx4, KpiE::getdSource, KpiE::setdSource);
			
			Label  label = new Label();
			root.addComponent(label);
			
		}
	}
	
	

}
