package com.nogroup.SMGis.views.workflow.home;

import com.google.common.eventbus.Subscribe;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.events.DashboardEvent.MapViewVisited;
import com.nogroup.SMGis.business.events.DashboardEvent.PostViewChangeEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.ProfileUpdatedEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.ReportsCountUpdatedEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.UserLoggedOutEvent;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.views.ccmp.cntnrs.TabContainer;
import com.nogroup.SMGis.views.workflow.embedded.CitiesView;
import com.nogroup.SMGis.views.workflow.embedded.DimensionView;
import com.nogroup.SMGis.views.workflow.embedded.KPIsView;
import com.nogroup.SMGis.views.workflow.embedded.ParametersView;
import com.nogroup.SMGis.views.workflow.windows.NewCityWin;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ControlView  extends HorizontalLayout {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	public ControlView() {

		setSizeFull();
        addStyleName("mainview");
        setSpacing(false);

        addComponent(new DashboardMenu());

        VerticalLayout v = new VerticalLayout() ;
        v.setMargin(false);
        v.setSpacing(false);
        v.setSizeFull();
        
        VUI.container = new TabContainer() ;
        VUI.container.setWidth("100%");
        VUI.container.setHeight("30px");
		v.addComponent(VUI.container);
		
        ComponentContainer content = new CssLayout();
        content.addStyleName("view-content");
        content.setSizeFull();
        
        v.addComponent(content);
        v.setExpandRatio(content, 1.0f);
        addComponent(v);
        setExpandRatio(v, 1.0f);
        
        //new DashboardNavigator(content);
        UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(),content));
        UI.getCurrent().getNavigator().addView("Cities", CitiesView.class);
        UI.getCurrent().getNavigator().addView("Dimensions", DimensionView.class);
        UI.getCurrent().getNavigator().addView("KPIs", KPIsView.class);
        UI.getCurrent().getNavigator().addView("Parameters", ParametersView.class);
        
        DashboardEventBus.post(new MapViewVisited());
        UI.getCurrent().getNavigator().navigateTo("Cities");
        
       
    }

	
	protected class DashboardMenu extends CustomComponent{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		
		public static final String ID = "dashboard-menu";
	    public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
	    public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
	    private static final String STYLE_VISIBLE = "valo-menu-visible";
	    private Label notificationsBadge;
	    private Label reportsBadge;
	    private MenuItem settingsItem;	
	    
	    public DashboardMenu() {
	        setPrimaryStyleName("valo-menu");
	        setId(ID);
	        setSizeUndefined();

	        DashboardEventBus.register(this);

	        setCompositionRoot(buildContent());
	    }
	    
	    private Component buildContent() {
	        final CssLayout menuContent = new CssLayout();
	        menuContent.addStyleName("sidebar");
	        menuContent.addStyleName(ValoTheme.MENU_PART);
	        menuContent.addStyleName("no-vertical-drag-hints");
	        menuContent.addStyleName("no-horizontal-drag-hints");
	        menuContent.setWidth(null);
	        menuContent.setHeight("100%");

	        menuContent.addComponent(buildTitle());
	        menuContent.addComponent(buildUserMenu());
	        menuContent.addComponent(buildToggleButton());
	        menuContent.addComponent(buildMenuItems());

	        return menuContent;
	    }

	    private Component buildTitle() {
	        Label logo = new Label("SMART <strong>KPI</strong>",
	                ContentMode.HTML);
	        logo.setSizeUndefined();
	        HorizontalLayout logoWrapper = new HorizontalLayout(logo);
	        logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
	        logoWrapper.addStyleName("valo-menu-title");
	        logoWrapper.setSpacing(false);
	        return logoWrapper;
	    }

	    private String getCurrentUser() {
	        return (String) VaadinSession.getCurrent()
	                .getAttribute("usr");
	    }

	    private Component buildUserMenu() {
	        final MenuBar settings = new MenuBar();
	        settings.addStyleName("user-menu");
	        final String user = getCurrentUser();
	        settingsItem = settings.addItem("",
	                new ThemeResource("img/profile-pic-300px.jpg"), null);
	        updateUserName(null);
	        settingsItem.addItem("Edit Profile", new Command() {
	            @Override
	            public void menuSelected(final MenuItem selectedItem) {
	                //ProfilePreferencesWindow.open(user, false);
	            	Notification.show("Edit Profile");
	            }
	        });
	        
	        settingsItem.addItem("Preferences", new Command() {
	            @Override
	            public void menuSelected(final MenuItem selectedItem) {
	                //ProfilePreferencesWindow.open(user, true);
	            }
	        });
	        settingsItem.addSeparator();
	        settingsItem.addItem("Sign Out", new Command() {
	            @Override
	            public void menuSelected(final MenuItem selectedItem) {
	                DashboardEventBus.post(new UserLoggedOutEvent());
	            }
	        });
	        return settings;
	    }

	    private Component buildToggleButton() {
	        Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
	            @Override
	            public void buttonClick(final ClickEvent event) {
	                if (getCompositionRoot().getStyleName()
	                        .contains(STYLE_VISIBLE)) {
	                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
	                } else {
	                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
	                }
	            }
	        });
	        valoMenuToggleButton.setIcon(FontAwesome.LIST);
	        valoMenuToggleButton.addStyleName("valo-menu-toggle");
	        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
	        return valoMenuToggleButton;
	    }

	    private Component buildMenuItems() {
	        CssLayout menuItemsLayout = new CssLayout();
	        menuItemsLayout.addStyleName("valo-menuitems");
	        
	        Component menuItemComponent = new ValoMenuItemButton("Cities",FontAwesome.MAP, new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					
					DashboardEventBus.post(new MapViewVisited());
					UI.getCurrent().getNavigator().navigateTo("Cities");
				}
			});
        	menuItemsLayout.addComponent(menuItemComponent);
        	
        	menuItemComponent = new ValoMenuItemButton("Dimensions",FontAwesome.MAP, new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					
					DashboardEventBus.post(new MapViewVisited());
					UI.getCurrent().getNavigator().navigateTo("Dimensions");
				}
			});
        	menuItemsLayout.addComponent(menuItemComponent);
        	
        	menuItemComponent = new ValoMenuItemButton("KPIs",FontAwesome.BUILDING, new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					UI.getCurrent().getNavigator().navigateTo("KPIs");
				}
			});
        	menuItemsLayout.addComponent(menuItemComponent);
        	
        	menuItemComponent = new ValoMenuItemButton("Parameters",FontAwesome.BUILDING, new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					UI.getCurrent().getNavigator().navigateTo("Parameters");
				}
			});
        	menuItemsLayout.addComponent(menuItemComponent);
        	
        	
        	/*
	        for(int i = 0 ; i < 5 ;i++) {
	        	Component menuItemComponent = new ValoMenuItemButton(i);
	        	menuItemsLayout.addComponent(menuItemComponent);
	        }*/
	        return menuItemsLayout;

	    }

	    private Component buildBadgeWrapper(final Component menuItemButton,
	            final Component badgeLabel) {
	        CssLayout dashboardWrapper = new CssLayout(menuItemButton);
	        dashboardWrapper.addStyleName("badgewrapper");
	        dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
	        badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
	        badgeLabel.setWidthUndefined();
	        badgeLabel.setVisible(false);
	        dashboardWrapper.addComponent(badgeLabel);
	        return dashboardWrapper;
	    }

	    @Override
	    public void attach() {
	        super.attach();
	        //updateNotificationsCount(null);
	    }
	    
	    @Subscribe
	    public void postViewChange(final PostViewChangeEvent event) {
	        getCompositionRoot().removeStyleName(STYLE_VISIBLE);
	    }
		
	    /*
	    @Subscribe
	    public void updateNotificationsCount(
	            final NotificationsCountUpdatedEvent event) {
	        int unreadNotificationsCount = DashboardUI.getDataProvider()
	                .getUnreadNotificationsCount();
	        notificationsBadge.setValue(String.valueOf(unreadNotificationsCount));
	        notificationsBadge.setVisible(unreadNotificationsCount > 0);
	    }*/

	    @Subscribe
	    public void updateReportsCount(final ReportsCountUpdatedEvent event) {
	        reportsBadge.setValue(String.valueOf(event.getCount()));
	        reportsBadge.setVisible(event.getCount() > 0);
	    }

	    @Subscribe
	    public void updateUserName(final ProfileUpdatedEvent event) {
	        String user = getCurrentUser();
	        settingsItem.setText(user);
	    }

	    public final class ValoMenuItemButton extends Button {

	        private static final String STYLE_SELECTED = "selected";

	        public ValoMenuItemButton(final int i) {
	            setPrimaryStyleName("valo-menu-item");
	            setCaption("View " + i);
	            setIcon(FontAwesome.HOME);
	            DashboardEventBus.register(this);
	            addClickListener(new ClickListener() {
	                @Override
	                public void buttonClick(final ClickEvent event) {
	                    /*UI.getCurrent().getNavigator()
	                            .navigateTo(view.getViewName());*/
	                }
	            });

	        }
	        public ValoMenuItemButton(String caption,Resource icon, ClickListener listener) {
	            setPrimaryStyleName("valo-menu-item");
	            setCaption(caption);
	            setIcon(icon);
	            DashboardEventBus.register(this);
	            addClickListener(listener) ;
	        }
	    }
		
	}
}
