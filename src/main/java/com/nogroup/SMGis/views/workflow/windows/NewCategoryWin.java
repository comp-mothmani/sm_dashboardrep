package com.nogroup.SMGis.views.workflow.windows;

import java.util.Date;

import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.DimensionCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.SubDimensionCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewCategoryWin extends CrWindow {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	private CategoryE entity = new CategoryE();
	private DimensionCbx cbx1;
	private SubDimensionCbx cbx2;
	
	public  NewCategoryWin() {
		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		Binder<CategoryE> binder = new Binder<>();
		binder.setBean(entity);
		
		TextField tf = new TextField() ;
		tf.setPlaceholder("Name");
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf);
		binder.bind(tf, CategoryE::getName, CategoryE::setName);
	
		cbx1 = new DimensionCbx().fromDB();
		vl.addComponent(cbx1);
		cbx1.addValueChangeListener(new ValueChangeListener<DimensionE>() {
			
			@Override
			public void valueChange(ValueChangeEvent<DimensionE> event) {
				cbx2.setItems(event.getValue().getSubDimenstions());
				cbx2.setValue(null);
			}
		});
		
		cbx2 = new SubDimensionCbx();
		vl.addComponent(cbx2);

		TextArea tx = new TextArea();
		tx.setPlaceholder("Description");
		tx.setWidth("100%");
		tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx);
		binder.bind(tx, CategoryE::getDescription, CategoryE::setDescription);
	
	}
	
	@Override
	public String caption_() {
		
		return "New Category";
	}

	@Override
	public String width_() {
		
		return "35%";
	}

	@Override
	public String height_() {
		
		return "43%";
	}

	@Override
	public void saveAction() {
		entity.setDCreated(new Date());
		cbx2.getValue().addCategories(entity);
		new DimensionD().hard_update(cbx1.getValue());
	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
