package com.nogroup.SMGis.views.workflow.windows;

import java.util.Date;

import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.DimensionCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewSubDimensionWin extends CrWindow {
	
	/**
	 * Author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	private SubDimensionE entity = new SubDimensionE();

	private DimensionCbx cbx;
	
	public  NewSubDimensionWin() {
		VerticalLayout vl = new VerticalLayout() ;
		addContent(vl);
		Binder<SubDimensionE> binder = new Binder<>();
		binder.setBean(entity);
		
		TextField tf = new TextField() ;
		tf.setPlaceholder("Name");
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tf);
		binder.bind(tf, SubDimensionE::getName, SubDimensionE::setName);
	
		cbx = new DimensionCbx().fromDB();
		vl.addComponent(cbx);

		TextArea tx = new TextArea();
		tx.setPlaceholder("Description");
		tx.setWidth("100%");
		tx.addStyleName(ValoTheme.TEXTFIELD_TINY);
		vl.addComponent(tx);
		binder.bind(tx, SubDimensionE::getDescription, SubDimensionE::setDescription);
	}

	@Override
	public String caption_() {
		return "New Sub-Dimension";
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "37%";
	}

	@Override
	public void saveAction() {
		entity.setDCreated(new Date());
		cbx.getValue().addSubDimenstions(entity);
		new DimensionD().hard_update(cbx.getValue());
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

}
