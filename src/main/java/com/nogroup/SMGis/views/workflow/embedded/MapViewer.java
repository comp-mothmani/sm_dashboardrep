package com.nogroup.SMGis.views.workflow.embedded;

import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.vaadin.addon.vol3.OLMap;
import org.vaadin.addon.vol3.OLMap.ClickListener;
import org.vaadin.addon.vol3.OLMap.OLClickEvent;
import org.vaadin.addon.vol3.OLView;
import org.vaadin.addon.vol3.OLView.ViewChangeListener;
import org.vaadin.addon.vol3.OLViewOptions;
import org.vaadin.addon.vol3.VaadinOverlay;
import org.vaadin.addon.vol3.client.OLCoordinate;
import org.vaadin.addon.vol3.client.OLExtent;
import org.vaadin.addon.vol3.client.OLOverlay;
import org.vaadin.addon.vol3.client.Projections;
import org.vaadin.addon.vol3.client.control.OLAttributionControl;
import org.vaadin.addon.vol3.client.control.OLLayerSwitcherControl;
import org.vaadin.addon.vol3.client.control.OLMousePositionControl;
import org.vaadin.addon.vol3.client.format.OLFeatureFormat;
import org.vaadin.addon.vol3.feature.OLFeature;
import org.vaadin.addon.vol3.feature.OLPolygon;
import org.vaadin.addon.vol3.interaction.OLDrawInteraction;
import org.vaadin.addon.vol3.interaction.OLDrawInteractionOptions;
import org.vaadin.addon.vol3.layer.OLTileLayer;
import org.vaadin.addon.vol3.layer.OLVectorLayer;
import org.vaadin.addon.vol3.source.OLOSMSource;
import org.vaadin.addon.vol3.source.OLSource;
import org.vaadin.addon.vol3.source.OLTileWMSSource;
import org.vaadin.addon.vol3.source.OLTileWMSSourceOptions;
import org.vaadin.addon.vol3.source.OLTileXYZSource;
import org.vaadin.addon.vol3.source.OLVectorSource;
import org.vaadin.addon.vol3.source.OLVectorSource.FeatureSetChangeListener;
import org.vaadin.addon.vol3.source.OLVectorSourceOptions;
import org.vaadin.simplefiledownloader.SimpleFileDownloader;

import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.data.embedded.VPoint;
import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.views.ccmp.cntnrs.knv.LegendEditorV;
import com.nogroup.SMGis.views.ccmp.flds.DrawPolygonHelper;
import com.nogroup.SMGis.views.workflow.windows.GeolocationWin;
import com.nogroup.SMGis.views.workflow.windows.LayerManagerWin;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.UI;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

import elemental.json.JsonArray;

@com.vaadin.annotations.JavaScript(value = { "mapviewercallbacks.js" })
public class MapViewer extends VerticalLayout implements View, ViewChangeListener{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private OLMap map;
	private OLTileLayer osm;
	private OLVectorLayer selectionLayer;
	private HashMap<String, OLTileLayer> tileLayers = new HashMap<>() ;
	
	public MapViewer() {
		
        setSizeFull();
        addStyleName("sales");
		setMargin(false);
		setSpacing(false);
        
		initMap();
		initMenus() ;
		
		
		Map<String, String> params = new HashMap<>() ;
		params.put("format","image/png") ;
		params.put("VERSION","1.1.1") ;
		params.put("LAYERS","CERT:tn") ;
		params.put("tiled","true") ;
		params.put("exceptions", "application/vnd.ogc.se_inimage") ;
		
		OLTileWMSSourceOptions options = new OLTileWMSSourceOptions() ;
		options.setUrl("http://0.0.0.0:8080/geoserver/CERT/wms") ;
		options.setParams(params) ;
		OLTileWMSSource src2 = new OLTileWMSSource(options ) ;
		
		OLTileLayer tiles= new OLTileLayer(src2) ;
		tiles.setTitle("TILES");
		map.addLayer(tiles);
		
		params = new HashMap<>() ;
		params.put("format","image/png") ;
		params.put("VERSION","1.1.1") ;
		params.put("LAYERS","CERT:municipalities") ;
		params.put("tiled","true") ;
		params.put("exceptions", "application/vnd.ogc.se_inimage") ;
		
		options = new OLTileWMSSourceOptions() ;
		options.setUrl("http://0.0.0.0:8080/geoserver/CERT/wms") ;
		options.setParams(params) ;
		src2 = new OLTileWMSSource(options ) ;
		
		tiles= new OLTileLayer(src2) ;
		tiles.setTitle("MUNICIPALITIES");
		map.addLayer(tiles);
		
		params = new HashMap<>() ;
		params.put("format","image/png") ;
		params.put("VERSION","1.1.1") ;
		params.put("LAYERS","CERT:municipalitiesCentroid") ;
		params.put("tiled","true") ;
		params.put("exceptions", "application/vnd.ogc.se_inimage") ;
		
		options = new OLTileWMSSourceOptions() ;
		options.setUrl("http://0.0.0.0:8080/geoserver/CERT/wms") ;
		options.setParams(params) ;
		src2 = new OLTileWMSSource(options ) ;
		
		
		tiles= new OLTileLayer(src2) ;
		
		
		tiles.setTitle("CENTROIDS");
		map.addLayer(tiles);
		
		
		
		OLVectorSourceOptions vectorOptions=new OLVectorSourceOptions();
		// this is proxied by the TestServer
		vectorOptions.setUrl("http://0.0.0.0:8080/geoserver/topp/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=topp%3Atasmania_state_boundaries&maxFeatures=50&outputFormat=application%2Fjson");
		vectorOptions.setFormat(OLFeatureFormat.GEOJSON);
		OLVectorSource vectorSource=new OLVectorSource(vectorOptions);
		OLVectorLayer vectorLayer=new OLVectorLayer(vectorSource);
		vectorLayer.setLayerVisible(true);
		map.addLayer(vectorLayer);
		vectorLayer.setTitle("jjkjkj");
		for(OLFeature tmp : vectorSource.getFeatures()) {
			System.out.println(tmp.getProperties());
		}

		VaadinOverlay overlay = new VaadinOverlay(new OLCoordinate(0, 0), "id") ;
		map.addVaadinOverlay(overlay);
		
		map.getView().addViewChangeListener(this);
		
		
		map.addClickListener(new ClickListener() {
			
			@Override
			public void onClick(OLClickEvent clickEvent) {
				
				//map.
				clickEvent.getPixel() ;
				
			}
		});
		
		map.setAttributionControl(new OLAttributionControl()) ;
		map.setMousePositionControl(new OLMousePositionControl());
		
    }
	
	
	protected void initMenus() {
		ContextMenu contextMenu = new ContextMenu(this, true);
		contextMenu.addItem("Layer Manager", e -> {
			VUI.container.addWindow(new LayerManagerWin(this), "Layers");
		});
		contextMenu.addItem("Go to ..", e -> {
	        VUI.container.addWindow(new GeolocationWin(this), "Go To");
		});
		
		MenuItem mn = contextMenu.addItem("Export ..");
		
		mn.addItem("Map Raster", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				exportMap(new exportCallBack() {
					
					@Override
					public void back(String img) {
						
						SimpleFileDownloader downloader = new SimpleFileDownloader();
						addExtension(downloader);
						img = img.replace("data:image/png;base64,", "") ;
						byte[] bytes = Base64.getDecoder().decode(img) ;
						final StreamResource resource = new StreamResource(() -> {
							return new ByteArrayInputStream(bytes);
						}, "export.png");
						
						downloader.setFileDownloadResource(resource);
						downloader.download();
					}
				});
			}
		});
		
		mn.addItem("Map Legend", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				exportMap(new exportCallBack() {

					@Override
					public void back(String img) {
						UI.getCurrent().addWindow(new LegendEditorV(img));
					}
					
				});
				
			}
		});

	}

	protected OLSource createTileSource() {		
		return new OLOSMSource();
	}
	
	protected OLView createView() {
		OLViewOptions options = new OLViewOptions();
		options.setInputProjection(Projections.EPSG4326);
		OLView view = new OLView(options);
		
		view.setCenter(new OLCoordinate(9.5375,33.8869));
		view.setZoom(7);
		return view;
	}
	
	
	protected void initMap() {
		
		map = new OLMap();
		map.setId("canvas");
		map.setView(createView());
		map.setSizeFull();
		addComponent(map);
		map.setLayerSwitcherControl(new OLLayerSwitcherControl());
		
		osm = new OLTileLayer(createTileSource());
		osm.setTitle("OSM");
		map.addLayer(osm);
		tileLayers.put("OSM", osm) ;
		
		OLTileXYZSource xyz = new OLTileXYZSource();
		xyz.setUrlTemplate("https://a.tile.openstreetmap.org/${z}/${x}/${y}.png");
		xyz.setVisible(true);
		
		OLTileLayer layer = new OLTileLayer(xyz) ;
		layer.setTitle("openptmap");
		
		map.addLayer(layer);
		tileLayers.put("openptmap", layer) ;
		
		
		selectionLayer = new OLVectorLayer(new OLVectorSource()) ;
		map.addLayer(selectionLayer);
		
	}

	public void center(double lat,double lon) {
		map.getView().setCenter(lon,lat);
	}
	protected void center(VPoint vp) {
		map.getView().setCenter(vp.getLon(), vp.getLat());
	}
	
	public void clearSelectionLayer() {
		OLVectorSource src = (OLVectorSource) selectionLayer.getSource() ;
		src.removeFeatureById("polySelected");
	}
	public void drawPolygon(DrawPolygonHelper helper) {
		OLDrawInteraction inter = new OLDrawInteraction(selectionLayer,
				OLDrawInteractionOptions.DrawingType.POLYGON);
		map.addInteraction(inter);
		OLVectorSource src = (OLVectorSource) selectionLayer.getSource() ;
		src.removeFeatureById("polySelected");
		src.addFeatureSetChangeListener(new FeatureSetChangeListener() {
			
			@Override
			public void featureDeleted(OLFeature feature) {
				
			}
			
			@Override
			public void featureAdded(OLFeature feature) {
				feature.setId("polySelected");
				OLPolygon poly = (OLPolygon) feature.getGeometry() ;
				
				VPolygon vp = new VPolygon().fromOLPolygon(poly) ;
				helper.back(vp) ;
				map.removeInteraction(inter);
			}
		});
	}
	
	@Override
    public void enter(final ViewChangeEvent event) {
    }

	public OLMap getMap() {
		return map;
	}

	public OLVectorLayer getSelectionLayer() {
		return selectionLayer;
	}

	public HashMap<String, OLTileLayer> getTileLayers() {
		return tileLayers;
	}

	public void setTileLayers(HashMap<String, OLTileLayer> tileLayers) {
		this.tileLayers = tileLayers;
	}

	@Override
	public void resolutionChanged(Double newResolution) {
		
	}

	@Override
	public void rotationChanged(Double rotation) {
		
	}

	@Override
	public void centerChanged(OLCoordinate centerPoint) {
		OLOverlay overlay = new OLOverlay();
        overlay.position = centerPoint;
        overlay.insertFirst = true ;
        overlay.htmlContent="<h1>4444444444</h1>";
        overlay.id = "idsss";
        overlay.classNames="ol-popup";
        
        map.addOverlay(overlay);
	}

	@Override
	public void zoomChanged(Integer zoom) {
		
	}

	@Override
	public void extentChanged(OLExtent extent) {
		
	}
	
	public interface exportCallBack {
		public void back(String img) ;
	}
	public void exportMap(exportCallBack callback) {
		String s = ""
				+ "var canvas = document.getElementById(\"canvas\"); \n" + 
				"canvas = canvas.getElementsByTagName(\"div\")[0] ;\n" + 
				"canvas = canvas.getElementsByTagName(\"canvas\")[0] ;\n" + 
				"var context = canvas.getContext(\"2d\");\n" + 
				"context.fillStyle = \"green\";\n" + 
				"// no argument defaults to image/png; image/jpeg, etc also work on some\\n\" + \n" + 
				"// implementations -- image/png is the only one that must be supported per spec.\\n\" + \n" + 
				"var p1 = canvas.toDataURL(\"image/png\");" +
				"exportCallback(p1);" ;
		
		JavaScript.getCurrent().execute(s);
		
		JavaScript.getCurrent().addFunction("exportCallback", new JavaScriptFunction() {
			
			@Override
			public void call(JsonArray arguments) {
				callback.back(arguments.asString());
				
			}
		});
	}
	
	
}
