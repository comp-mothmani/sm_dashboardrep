package com.nogroup.SMGis.views.workflow.windows;

import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.business.utils.RandomUtils;
import com.nogroup.SMGis.data.daos.UserD;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.views.ccmp.cntnrs.MaWindow;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;
import com.nogroup.SMGis.views.ccmp.flds.imgUpld.ImageUpload;
import com.vaadin.data.Binder;
import com.vaadin.data.TreeData;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ManageUsersWin extends MaWindow implements ItemClickListener<TreeBean> {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	private UserE entity = new UserE();
	private ImageUpload img;

	
	public ManageUsersWin() {
		initData();
		initTree(treeData,this) ;
		getPane().setCaption(CResourceBundle.getLocal("maWindowManageUserDetails"));

	}

	@Override
	public void switch2() {	
		setDetails(new EmbeddedView());
	}

	@Override
	public void initData() {
		treeData.clear();
		//new UserC(new UserD().read()).filterByDiscrimatorValue("MANAGER")
		for (UserE tmp : new UserD().read() ) {
			if(!tmp.getUName().contains("admin")) {
				treeData.addItem(null, tmp);
				System.out.print(" USERS FROUND: "+ tmp.getUName() + "\n");
			}
		}	
	}

	@Override
	public String caption_() {
		return "Manage Users Profile";
	}

	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "65%";
	}

	@Override
	public void saveAction() {
		((UserE) selectedItem).setImg(img.getEncodedString());
		new UserD().hard_update((UserE) selectedItem);
	}

	@Override
	public void deleteAction() {
		new UserD().delete(((UserE) selectedItem).getId());
	}

	@Override
	public void windowClosed(CloseEvent e) {
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {	
		selectedItem = event.getItem() ;
		switch2();
	}
	
	protected class EmbeddedView extends VerticalLayout{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		
		public EmbeddedView() {
			if(selectedItem == null) {
				return ;
			}else {
				new UserDetailsView(this);
			}
		}		
	}
	
	protected class UserDetailsView {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID =  1L;
		public UserDetailsView(EmbeddedView root) {
			
			Binder<UserE> binder = new Binder<>();
			binder.setBean((UserE) selectedItem);
			
			img = new ImageUpload(entity) ;
	        root.addComponent(img);

			
			TextField tf = new TextField(CResourceBundle.getLocal("editUserWinFname"));
			tf.setPlaceholder("First name");
			tf.setWidth("100%");
			tf.setSizeFull();
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf, UserE::getFName, UserE::setFName);
			root.addComponent(tf);
			
			TextField tf1 = new TextField(CResourceBundle.getLocal("editUserWinLname")) ;
			tf1.setPlaceholder("Last name");
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf1, UserE::getLName, UserE::setLName);
			root.addComponent(tf1);
			
		    TextField tf3 = new TextField(CResourceBundle.getLocal("editUserWinEmail")) ;
		    tf3.setPlaceholder("Email");
			tf3.setWidth("100%");
			tf3.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf3, UserE::getUName, UserE::setUName);
			root.addComponent(tf3);
			
			TextField tf4 = new TextField(CResourceBundle.getLocal("editUserWinType")) ;
			tf4.setPlaceholder("Type");
			tf4.setEnabled(false);
			tf4.setWidth("100%");
			tf4.addStyleName(ValoTheme.TEXTFIELD_TINY);
			tf4.setValue("Manager");
			root.addComponent(tf4);
			
			HorizontalLayout hl = new HorizontalLayout();
			hl.setWidth("100%");
			hl.setCaption("Password");
			root.addComponent(hl);
			
		
			Button btn = new Button("Generate");
			btn.addStyleName("tiny");
			btn.setWidth("100%");
			hl.addComponent(btn);
			
			PasswordField pass_txt = new PasswordField();
			pass_txt.addStyleName(ValoTheme.TEXTFIELD_TINY);
			pass_txt.setWidth("100%");
			binder.bind(pass_txt, UserE::getPwd, UserE::setPwd);
			hl.addComponent(pass_txt);	
			
			btn.addClickListener(e -> {

				pass_txt.setValue(RandomUtils.getAlphaNumericString(6));
			});
			

		}
		
	}
	
	

}
