package com.nogroup.SMGis.views.workflow.embedded;

import com.google.common.eventbus.Subscribe;
import com.nogroup.SMGis.business.codegraph.CGraphExplorer;
import com.nogroup.SMGis.business.codegraph.NodeImpl;
import com.nogroup.SMGis.business.codegraph.SimpleGraphRepositoryImpl;
import com.nogroup.SMGis.business.events.DashboardEvent.GraphNodeToggled;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.graph.GraphExplorer;
import com.vaadin.graph.layout.JungCircleLayoutEngine;
import com.vaadin.graph.layout.JungFRLayoutEngine;
import com.vaadin.graph.layout.JungISOMLayoutEngine;
import com.vaadin.navigator.View;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.PopupView.Content;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class KpiView extends VerticalLayout implements View{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	private SimpleGraphRepositoryImpl graphRepo;
    private GraphExplorer<?, ?> graph;
    private CssLayout layout;

	private PopupView popup;
    
	public KpiView() {
		
		setSizeFull();	
		DashboardEventBus.register(this);
		//addComponent(new Label("KPI"));
		graphRepo = createGraphRepository2();    	
    	VerticalLayout content = new VerticalLayout();
    	layout = new CssLayout();
    	layout.setSizeFull();
    	ComboBox<String> select = createLayoutSelect();
    	//content.addComponent(select);
    	//content.addComponent(layout);
    	//content.setExpandRatio(layout, 1);
    	content.setSizeFull();
    	refreshGraph();
    	addComponent(layout);
    	
    	VerticalLayout popupContent = new VerticalLayout();
    	popupContent.addComponent(new TextField("Textfield"));
    	popupContent.addComponent(new Button("Button"));

    	// The component itself
    	popup = new PopupView("Pop it up", popupContent);
    	layout.addComponent(popup);
        
	}
	
	private SimpleGraphRepositoryImpl createGraphRepository2() {
    	SimpleGraphRepositoryImpl repo = new SimpleGraphRepositoryImpl();
    	repo.addNode("root", "KPIs").style("root");;
    	repo.setHomeNodeId("root");
    	
    	for(DimensionE tmp : new DimensionD().read()) {
    		String nodeName = tmp.getName() + "Node" ;
    		repo.addNode(nodeName, tmp.getName()).style("red").setData(tmp).setStereotype("DIMENSION") ;
    		repo.joinNodes("root", nodeName, "root" + nodeName,"").style("thick-blue");
    		
    		for(SubDimensionE tmp2 : tmp.getSubDimenstions()) {
    			String nodeName1 = tmp2.getName() + "Node" ;
    			repo.addNode(nodeName1, tmp2.getName()).style("blue").setData(tmp2).setStereotype("SUBDIMENSION") ;
        		repo.joinNodes(nodeName, nodeName1, nodeName + nodeName1,"").style("thick-blue");
        		for(CategoryE tmp3 : tmp2.getCategories()) {
        			String nodeName2 = tmp3.getName() + "Node" ;
        			repo.addNode(nodeName2, tmp3.getName()).style("blue").setData(tmp3).setStereotype("CATEGORY") ;
            		repo.joinNodes(nodeName1, nodeName2, nodeName1 + nodeName2,"").style("thick-blue");
            		for(KpiE tmp4 : tmp3.getKpis()) {
            			String nodeName3 = tmp4.getName() + "Node" ;
            			repo.addNode(nodeName3, tmp4.getName()).style("blue").setData(tmp4).setStereotype("KPI") ;
                		repo.joinNodes(nodeName2, nodeName3, nodeName2 + nodeName3,"").style("thick-blue");
            		}
        		}
    		}
    	}
    	
    	
    	return repo;
    }
	
	private SimpleGraphRepositoryImpl createGraphRepository() {
    	SimpleGraphRepositoryImpl repo = new SimpleGraphRepositoryImpl();
    	repo.addNode("node1", "Node 1").style("root");;
    	repo.setHomeNodeId("node1");
    	
    	repo.addNode("node2", "Node 2").style("blue");
    	repo.addNode("node3", "Node 3");
    	repo.addNode("node4", "Node 4").setIcon(new ThemeResource("icons/48x48/cat_1.png"));

    	repo.addNode("node10", "Node 10");
    	repo.addNode("node11", "Node 11");
    	repo.addNode("node12", "Node 12").setIcon(new ThemeResource("icons/48x48/cat_2.png"));
    	repo.addNode("node13", "Node 13");
    	repo.addNode("node14", "Node 14").setIcon(new ThemeResource("icons/48x48/cat_3.png"));
    	repo.addNode("node15", "Node 15");
    	repo.addNode("node16", "Node 16").setIcon(new ThemeResource("icons/64x64/cat_4.png"));
    	repo.addNode("node17", "Node 17");
    	repo.addNode("node18", "Node 18").setIcon(new ThemeResource("icons/64x64/cat_5.png"));
    	repo.addNode("node19", "Node 19");
    	repo.addNode("node20", "Node 20");
    	repo.addNode("node21", "Node 21");
    	repo.addNode("node22", "Node 22");
    	repo.addNode("node23", "Node 23");
    	repo.addNode("node24", "Node 24");
    	repo.addNode("node25", "Node 25");

    	repo.joinNodes("node1", "node2", "edge12", "Edge 1-2").style("thick-blue");
    	repo.joinNodes("node1", "node3", "edge13", "Edge 1-3").style("thin-red");
    	repo.joinNodes("node3", "node4", "edge34", "Edge 3-4");

    	repo.joinNodes("node2", "node10", "edge210", "Edge type A");
    	repo.joinNodes("node2", "node11", "edge211", "Edge type A");
    	repo.joinNodes("node2", "node12", "edge212", "Edge type A");
    	repo.joinNodes("node2", "node13", "edge213", "Edge type A");
    	repo.joinNodes("node2", "node14", "edge214", "Edge type A");
    	repo.joinNodes("node2", "node15", "edge215", "Edge type A");
    	repo.joinNodes("node2", "node16", "edge216", "Edge type A");
    	repo.joinNodes("node2", "node17", "edge217", "Edge type A");
    	repo.joinNodes("node2", "node18", "edge218", "Edge type A");
    	repo.joinNodes("node2", "node19", "edge219", "Edge type A");
    	repo.joinNodes("node2", "node20", "edge220", "Edge type A");
    	repo.joinNodes("node2", "node21", "edge221", "Edge type A");
    	repo.joinNodes("node2", "node22", "edge222", "Edge type B");
    	repo.joinNodes("node2", "node23", "edge223", "Edge type B");
    	repo.joinNodes("node2", "node24", "edge224", "Edge type B");
    	repo.joinNodes("node2", "node25", "edge225", "Edge type C");
    	
    	return repo;
    }

	final ComboBox<String> select = new ComboBox<String>("Select layout algorithm");

	private ComboBox<String> createLayoutSelect() {
		select.setItems("FR", "Circle", "ISOM");
		select.addValueChangeListener(new ValueChangeListener<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent<String> event) {
				if ("FR".equals(select.getValue())) {
					graph.setLayoutEngine(new JungFRLayoutEngine());
				} else if ("Circle".equals(select.getValue())) {
					graph.setLayoutEngine(new JungCircleLayoutEngine());
				}
				if ("ISOM".equals(select.getValue())) {
					graph.setLayoutEngine(new JungISOMLayoutEngine());
				}
				refreshGraph();
			}
		});
		return select;
    }
    
    private void refreshGraph() {
    	layout.removeAllComponents();
        graph = new CGraphExplorer(graphRepo);
        
        graph.setSizeFull();
        layout.addComponent(graph);
    }
    
    @Subscribe
    public void ToggleEvent(GraphNodeToggled event) {
    	
    	NodeImpl nd = (NodeImpl) graph.getRepository().getNodeById(event.getNodeId()) ;
    	

    	// The component itself
    	popup.setContent(new Content() {
			
			@Override
			public Component getPopupComponent() {
				VerticalLayout ctn = new VerticalLayout();
				ctn.setCaption(nd.getLabel());
				ctn.addComponent(new Button("Details"));
		    	return ctn ;
			}
			
			@Override
			public String getMinimizedValueAsHTML() {
				return "hello";
			}
		});
    	popup.setPopupVisible(true) ;
    	
    }
}
