package com.nogroup.SMGis.views.workflow.windows;

import java.util.stream.Collectors;

import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.collections.CategoryC;
import com.nogroup.SMGis.data.collections.CountryC;
import com.nogroup.SMGis.data.collections.GouvernorateC;
import com.nogroup.SMGis.data.collections.KpiValC;
import com.nogroup.SMGis.data.collections.SubDimensionC;
import com.nogroup.SMGis.data.daos.CategoryD;
import com.nogroup.SMGis.data.daos.CityD;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.daos.SubDimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.CityE;
import com.nogroup.SMGis.data.entities.CountryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.GouvernorateE;
import com.nogroup.SMGis.data.entities.KpiValE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cbxs.CategoryCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.CountryCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.GouvernorateCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.KpiUnitCbx;
import com.nogroup.SMGis.views.ccmp.cbxs.SubDimensionCbx;
import com.nogroup.SMGis.views.ccmp.cntnrs.MaWindow;
import com.nogroup.SMGis.views.ccmp.flds.CityGridBean;
import com.nogroup.SMGis.views.ccmp.flds.GeometryField;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;
import com.nogroup.SMGis.views.ccmp.flds.WindowVisibility;
import com.vaadin.data.Binder;
import com.vaadin.data.TreeData;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.Setter;
import com.vaadin.ui.StyleGenerator;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.EditorOpenEvent;
import com.vaadin.ui.components.grid.EditorOpenListener;
import com.vaadin.ui.themes.ValoTheme;

public class ManageCitiesWin extends MaWindow implements ItemClickListener<TreeBean> {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	
	public ManageCitiesWin() {
		initData();
		initTree(treeData,this) ;
		getPane().setCaption(CResourceBundle.getLocal("manageCitiesWinCities"));
	}
	
	
	@Override
	public String caption_() {
		return CResourceBundle.getLocal("manageCitiesWin");
	}

	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "50%";
	}

	@Override
	public void saveAction() {
		new CityD().hard_update((CityE) selectedItem);
		initData();
		initTree(treeData,this) ;
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

	@Override
	public void deleteAction() {
		new CityD().delete(((CityE)selectedItem).getId());
		initData();
		initTree(treeData,this) ;
	}


	@Override
	public void initData() {
		treeData.clear() ;
		for(CityE tmp : new CityD().read()) {
			treeData.addItem(null,tmp);
		}
	}


	@Override
	public void switch2() {
		setDetails(new EmbeddedView());
	}
	
	protected class EmbeddedView extends VerticalLayout{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		
		public EmbeddedView() {
			if(selectedItem == null) {
				return ;
			}else {
				new ValuesView(this) ;
			}
		}
		
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {
		selectedItem = event.getItem() ;
		switch2();
	}
	
	protected class CityView{
		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		private GeometryField tf3;

		public CityView(EmbeddedView root) {

			Binder<CityE> binder = new Binder<>();
			binder.setBean((CityE) selectedItem);
			
			TextField tf = new TextField() ;
			tf.setPlaceholder(CResourceBundle.getLocal("manageCitiesWinCityViewName"));
			tf.setWidth("100%");
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf);
			binder.bind(tf, CityE::getName, CityE::setName);
			
			CountryCbx tf1 = new CountryCbx() ;
			tf1.initDemo();
			tf1.setPlaceholder(CResourceBundle.getLocal("manageCitiesWinCityViewCountry"));
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf1);
			binder.forField(tf1).bind(new ValueProvider<CityE, CountryE>() {
				
				@Override
				public CountryE apply(CityE source) {
					return new CountryC(
							tf1.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())
							).findByName(source.getCountry()) ;
				}
			}, new Setter<CityE, CountryE>() {
				
				@Override
				public void accept(CityE bean, CountryE fieldvalue) {
					bean.setCountry(fieldvalue.getName());
				}
			});
			
			
			GouvernorateCbx tf2 = new GouvernorateCbx() ;
			
			tf2.setPlaceholder(CResourceBundle.getLocal("manageCitiesWinCityViewGov"));
			tf2.setWidth("100%");
			tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			root.addComponent(tf2);
			binder.forField(tf2).bind(new ValueProvider<CityE, GouvernorateE>() {
				
				@Override
				public GouvernorateE apply(CityE source) {
					return new GouvernorateC(
							tf2.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())
							).findByName(source.getGov()) ;
				}
			}, new Setter<CityE, GouvernorateE>() {
				
				@Override
				public void accept(CityE bean, GouvernorateE fieldvalue) {
					bean.setCountry(fieldvalue.getName());
				}
			});
			
			tf1.addSelectionListener(new SingleSelectionListener<CountryE>() {
				
				@Override
				public void selectionChange(SingleSelectionEvent<CountryE> event) {
					CountryE country = event.getSelectedItem().get() ;
					tf2.switch2(country.getName()) ;
				}
			});

			tf3 = new GeometryField(new WindowVisibility() {

				@Override
				public void visible() {
					ManageCitiesWin.this.setVisible(true);
				}

				@Override
				public void invisible() {
					ManageCitiesWin.this.setVisible(false);
				}
				
			}) ;
			tf3.setWidth("100%");
			root.addComponent(tf3);
		}
	}
	
	protected class ValuesView {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;
		private TreeGrid<CityGridBean> grid;
		private TreeData<CityGridBean> treeGridData = new TreeData<CityGridBean>() ;
		private SubDimensionCbx subDimCbx;
		private CategoryCbx catCbx;
		
		public ValuesView(EmbeddedView root) {
			
			root.setSizeFull();
			grid = new TreeGrid<CityGridBean>() ;
			grid.addStyleName("v-treegrid-cell-content");
			grid.setWidth("100%");
			grid.setHeight("90%");
			
			
			
			
			//Column<CityGridBean, String> colName = grid.addColumn(CityGridBean::fetchName).setCaption("Name") ;
			//colName.setEditorBinding(binding) ;
			/*			            .withConverter(new StringToDoubleConverter(
			                    "Could not convert value to Double"))
			            .withValidator(i -> i >= 0 && i < 100,
				    			"Percentages needs to be in range [0..100)") */
			grid.getEditor().setEnabled(true);
			grid.addColumn(CityGridBean::fetchName)
			.setCaption(CResourceBundle.getLocal("manageCityGridColName"))
			.setEditorBinding(
					grid.getEditor().getBinder()
					    .forField(new TextField())
			            .bind(CityGridBean::fetchName, new Setter<CityGridBean, String>() {
							
							@Override
							public void accept(CityGridBean bean, String fieldvalue) {
								if (bean instanceof DimensionE) {
									//DimensionE new_name = (DimensionE) binding;
								}else if (bean instanceof KpiValE) {
									KpiValE val = (KpiValE) bean;
									val.setKpiName(fieldvalue);
								}
							}
						})
		            );
			
			
			subDimCbx = new SubDimensionCbx() ;
			catCbx = new CategoryCbx() ;
			grid.getEditor().addOpenListener(new EditorOpenListener<CityGridBean>() {
				
				@Override
				public void onEditorOpen(EditorOpenEvent<CityGridBean> event) {
					if(event.getBean() instanceof DimensionE) {
						
					}else if(event.getBean() instanceof KpiValE) {
						KpiValE v = (KpiValE) event.getBean() ;
						subDimCbx.setItems(new SubDimensionC(new SubDimensionD().read()));
						catCbx.setItems(new CategoryC(new CategoryD().read()));
					}
				}
			});
			grid.addColumn(
					CityGridBean::fetchSubDimension)
					.setCaption(CResourceBundle.getLocal("manageCityGridColSubD"))
					.setStyleGenerator(new StyleGenerator<CityGridBean>() {
						
						@Override
						public String apply(CityGridBean item) {
							
							return "v-treegrid-cell-content";
						}
					}).setEditorBinding(
							grid.getEditor().getBinder()
							    .forField(subDimCbx)
					            .bind(new ValueProvider<CityGridBean, SubDimensionE>() {
									
									@Override
									public SubDimensionE apply(CityGridBean source) {
										return new SubDimensionD().findByName(source.fetchSubDimension()) ;
									}
								}, new Setter<CityGridBean, SubDimensionE>() {
									
									@Override
									public void accept(CityGridBean bean, SubDimensionE fieldvalue) {
										if (bean instanceof DimensionE) {
											//DimensionE new_name = (DimensionE) binding;
										}else if (bean instanceof KpiValE) {
											KpiValE val = (KpiValE) bean;
											val.setSubDimension(fieldvalue.getName());
										}
									}
								})
				            );
			grid.addColumn(
					CityGridBean::fetchCategory)
					.setCaption(CResourceBundle.getLocal("manageCityGridColCategory"))
					.setStyleGenerator(new StyleGenerator<CityGridBean>() {
				
				@Override
				public String apply(CityGridBean item) {
					
					return "v-treegrid-cell-content";
				}
			}).setEditorBinding(
					grid.getEditor().getBinder()
				    .forField(catCbx)
		            .bind(new ValueProvider<CityGridBean, CategoryE>() {
						
						@Override
						public CategoryE apply(CityGridBean source) {
							return new CategoryD().findByName(source.fetchCategory()) ;
						}
					}, new Setter<CityGridBean, CategoryE>() {
						
						@Override
						public void accept(CityGridBean bean, CategoryE fieldvalue) {
							if (bean instanceof DimensionE) {
								//DimensionE new_name = (DimensionE) binding;
							}else if (bean instanceof KpiValE) {
								KpiValE val = (KpiValE) bean;
								val.setCategory(fieldvalue.getName());
							}
						}
					})
	            );
			grid.addColumn(
					CityGridBean::fetchUnit)
					.setCaption(CResourceBundle.getLocal("manageCityGridColUnit"))
					.setStyleGenerator(new StyleGenerator<CityGridBean>() {
						
						@Override
						public String apply(CityGridBean item) {
							
							return "v-treegrid-cell-content";
						}
					}).setEditorBinding(
							grid.getEditor().getBinder()
						    .forField(new KpiUnitCbx())
				            .bind(new ValueProvider<CityGridBean, String>() {
								
								@Override
								public String apply(CityGridBean source) {
									return source.fetchUnit() ;
								}
							}, new Setter<CityGridBean, String>() {
								
								@Override
								public void accept(CityGridBean bean, String fieldvalue) {
									if (bean instanceof DimensionE) {
										//DimensionE new_name = (DimensionE) binding;
									}else if (bean instanceof KpiValE) {
										KpiValE val = (KpiValE) bean;
										val.setUnit(fieldvalue);
									}
								}
							})
			            );
			grid.addColumn(
					CityGridBean::fetchValue)
					.setCaption(CResourceBundle.getLocal("manageCityGridColCVal"))
					.setStyleGenerator(new StyleGenerator<CityGridBean>() {
						
						@Override
						public String apply(CityGridBean item) {
							
							return "v-treegrid-cell-content";
						}
					}).setEditorBinding(
							grid.getEditor().getBinder()
						    .forField(new TextField())
				            .bind(new ValueProvider<CityGridBean, String>() {
								
								@Override
								public String apply(CityGridBean source) {
									return source.fetchValue() ;
								}
							}, new Setter<CityGridBean, String>() {
								
								@Override
								public void accept(CityGridBean bean, String fieldvalue) {
									if (bean instanceof DimensionE) {
										//DimensionE new_name = (DimensionE) binding;
									}else if (bean instanceof KpiValE) {
										KpiValE val = (KpiValE) bean;
										val.setValue(fieldvalue);
									}
								}
							})
			            );
			//grid.addColumn(KpiValE::getUnit).setCaption("Unit") ;
			//grid.addColumn(KpiValE::getValue).setCaption("Value") ;

			root.addComponent(grid);
			
			refresh();
		}
		
		public void refresh() {
			treeGridData.clear() ;
			for(DimensionE tmp : new DimensionD().read()) {
				treeGridData.addItem(null,tmp);
				KpiValC kpis = new KpiValC(
						((CityE)selectedItem).getKpis()
						).filterByDimension(tmp.getName()).filterByLatest() ;
				for(KpiValE tmp2 : kpis) {
					treeGridData.addItem(tmp,tmp2);
				}
				
			}
			TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeGridData);
			grid.setDataProvider(inMemoryDataProvider);
		}
		
	}
}
