package com.nogroup.SMGis.views.workflow.home;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class SwitcherView  extends VerticalLayout {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public SwitcherView() {
		setSizeFull(); 
        setMargin(true);
        setSpacing(true);
        
        VerticalLayout vl = new VerticalLayout() ;
        vl.addStyleName(ValoTheme.LAYOUT_WELL);
        addComponent(vl);
        setComponentAlignment(vl, Alignment.MIDDLE_CENTER);
        
        Button btn = new Button("DASHBOARD") ;
        btn.addStyleName(ValoTheme.BUTTON_HUGE);
        vl.addComponent(btn);
        vl.setComponentAlignment(btn, Alignment.MIDDLE_CENTER);
        btn.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().setContent(new MainView());
			}
		});
        
        Button btn1 = new Button("CONTROL PANEL") ;
        btn1.addStyleName(ValoTheme.BUTTON_HUGE);
        vl.addComponent(btn1);
        vl.setComponentAlignment(btn1, Alignment.MIDDLE_CENTER);
        btn1.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().setContent(new ControlView());
			}
		});
	}
}
