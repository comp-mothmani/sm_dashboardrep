package com.nogroup.SMGis.views.workflow.windows;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.SMGis.data.collections.DimensionC;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.nogroup.SMGis.data.entities.KpiE;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class UploadJSONWin extends CWindow implements Receiver, FinishedListener, FailedListener {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private File file;
	private String filename;
	private TreeGrid<TreeBean> grid ;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private DimensionC dims;
	public UploadJSONWin() {
		Panel pane = new Panel() ;
		pane.setWidth("100%");
		pane.setHeight("90%");
		VerticalLayout vl = new VerticalLayout();
		pane.setContent(vl);
		Upload upload = new Upload();
		upload.addStyleName(ValoTheme.BUTTON_TINY);
		upload.setId("myupload");
		upload.setReceiver(this);
		upload.addFinishedListener(this);
		upload.addFailedListener(this);

		vl.addComponents(upload);
		
		Button btn = new Button("Save") ;
		vl.addComponent(btn);
		btn.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				for(DimensionE d : dims) {
					for(SubDimensionE d1 : d.getSubDimenstions()) {
						d1.setDimension(d);
						for(CategoryE d2 : d1.getCategories()) {
							d2.setSubDimension(d1);
							for(KpiE d3 : d2.getKpis()) {
								d3.setCategory(d2);
							}
						}
					}
				}
				
				for(DimensionE d : dims) {
					d.setDCreated(new Date());
					for(SubDimensionE d1 : d.getSubDimenstions()) {
						d1.setDCreated(new Date());
						for(CategoryE d2 : d1.getCategories()) {
							d2.setDCreated(new Date());
							for(KpiE d3 : d2.getKpis()) {
								d3.setDCreated(new Date());
							}
						}
					}
					new DimensionD().create(d) ;
				}
			}
		});
		
		grid = new TreeGrid<TreeBean>() ;
		grid.addStyleName("v-treegrid-cell-content");
		grid.setWidth("100%");
		grid.setHeight("300px");
		grid.setHeaderVisible(false);
		grid.addColumn(TreeBean::fetchName).setCaption("Name") ;
		TreeDataProvider<TreeBean> inMemoryDataProvider = new TreeDataProvider<>(treeData);
		grid.setDataProvider(inMemoryDataProvider);
		vl.addComponent(grid);
		
		addContent(pane);

	}

	@Override
	public void windowClosed(CloseEvent e) {

	}

	@SuppressWarnings("deprecation")
	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fos = null;
		this.filename = filename;
		try {
			// Open the file for writing.
			file = new File(filename);
			fos = new FileOutputStream(file);
		} catch (final java.io.FileNotFoundException e) {
			getUI().showNotification("Could not open file<br/>", e.getMessage(), Notification.TYPE_ERROR_MESSAGE);
			return null;
		}
		return fos;
	}

	@Override
	public void uploadFailed(FailedEvent event) {

	}

	@Override
	public void uploadFinished(FinishedEvent event) {
		String data = "";
		try {
			data = new String(Files.readAllBytes(Paths.get(this.filename)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		dims = new DimensionC() ;
		try {
			dims = new ObjectMapper().readValue(data, DimensionC.class) ;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		treeData.clear() ;
		for(DimensionE tmp : dims) {
			treeData.addItem(null,tmp);
			for(SubDimensionE tmp2 : tmp.getSubDimenstions()) {
				treeData.addItem(tmp,tmp2);
				for(CategoryE tmp3 : tmp2.getCategories()) {
					treeData.addItem(tmp2,tmp3);
					for(KpiE tmp4 : tmp3.getKpis()) {
						treeData.addItem(tmp3,tmp4);
					}
				}
			}
		}
		TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeData);
		grid.setDataProvider(inMemoryDataProvider);
		
	}

}
