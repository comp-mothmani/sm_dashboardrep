package com.nogroup.SMGis.views.workflow.home;

import com.nogroup.SMGis.AppContext;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.events.DashboardEvent.MapViewVisited;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.collections.UserC;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.views.ccmp.btn.ValoMenuItemButton;
import com.nogroup.SMGis.views.ccmp.cntnrs.DashboardMenu;
import com.nogroup.SMGis.views.ccmp.cntnrs.TabContainer;
import com.nogroup.SMGis.views.workflow.embedded.CityView;
import com.nogroup.SMGis.views.workflow.embedded.ExportView;
import com.nogroup.SMGis.views.workflow.embedded.KpiView;
import com.nogroup.SMGis.views.workflow.embedded.MapViewer;
import com.nogroup.SMGis.views.workflow.windows.AddNewUserWin;
import com.nogroup.SMGis.views.workflow.windows.ExportPdfBenchMarking;
import com.nogroup.SMGis.views.workflow.windows.ExportPdfCityWin;
import com.nogroup.SMGis.views.workflow.windows.ExportPdfKpiWin;
import com.nogroup.SMGis.views.workflow.windows.GeolocationWin;
import com.nogroup.SMGis.views.workflow.windows.ManageCitiesWin;
import com.nogroup.SMGis.views.workflow.windows.ManageKPIsWin;
import com.nogroup.SMGis.views.workflow.windows.ManageUsersWin;
import com.nogroup.SMGis.views.workflow.windows.NewCategoryWin;
import com.nogroup.SMGis.views.workflow.windows.NewCityWin;
import com.nogroup.SMGis.views.workflow.windows.NewDimensionWin;
import com.nogroup.SMGis.views.workflow.windows.NewKpiWin;
import com.nogroup.SMGis.views.workflow.windows.NewSubDimensionWin;
import com.nogroup.SMGis.views.workflow.windows.UploadJSONWin;
import com.nogroup.SMGis.views.workflow.windows.UsageCityWin;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class MainView extends HorizontalLayout {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private DashboardMenu menu;

	@SuppressWarnings("deprecation")
	public MainView() {

		setSizeFull();
		addStyleName("mainview");
		setSpacing(false);

		menu = new DashboardMenu();
		addComponent(menu);

		VerticalLayout v = new VerticalLayout();
		v.setMargin(false);
		v.setSpacing(false);
		v.setSizeFull();

		VUI.container = new TabContainer();
		VUI.container.setWidth("100%");
		VUI.container.setHeight("30px");
		v.addComponent(VUI.container);

		ComponentContainer content = new CssLayout();
		content.addStyleName("view-content");
		content.setSizeFull();

		v.addComponent(content);
		v.setExpandRatio(content, 1.0f);
		addComponent(v);
		setExpandRatio(v, 1.0f);

		// new DashboardNavigator(content);
		UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(), content));
		UI.getCurrent().getNavigator().addView("MapViewer", MapViewer.class);
		UI.getCurrent().getNavigator().addView("Cities", CityView.class);
		UI.getCurrent().getNavigator().addView("KPIs", KpiView.class);

		DashboardEventBus.post(new MapViewVisited());
		UI.getCurrent().getNavigator().navigateTo("MapViewer");

		
		ValoMenuItemButton mp = menu.addMenuItem(CResourceBundle.getLocal("mapItem"), FontAwesome.MAP, "MapViewer", MapViewer.class);
		builMapContextMenu(mp);
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("cityItem"), FontAwesome.BUILDING, "Cities", CityView.class);
		buildCityContextMenu(mp);
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("kpiItem"), FontAwesome.DESKTOP, "KPIs", KpiView.class);
		buildKPIContextMenu(mp) ;
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("simulationItem"), FontAwesome.CALCULATOR, "Simulations", MapViewer.class);
		//buildKPIContextMenu(mp) ;
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("exportItem"), FontAwesome.FILE_PDF_O, "Exports", ExportView.class);
		buildExportContextMenu(mp);
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("usersItem"), FontAwesome.USER, "Users", MapViewer.class);
		buildUsersContextMenu(mp) ;
		
		mp = menu.addMenuItem(CResourceBundle.getLocal("settingsItem"), FontAwesome.ADJUST, "Settings", MapViewer.class);
		buildKPIContextMenu(mp) ;
		
	}
	
	private void builMapContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(this, true);
		contextMenu.addItem(CResourceBundle.getLocal("layerMangerBarMenu"), e -> {
			Notification.show("Layer Manager");
		});
		contextMenu.addItem(CResourceBundle.getLocal("goToBarMenu"), e -> {
			if(!(UI.getCurrent().getNavigator().getCurrentView() instanceof MapViewer)){
				UI.getCurrent().getNavigator().navigateTo("MapViewer");
			}
			MapViewer viewer = (MapViewer) UI.getCurrent().getNavigator().getCurrentView() ;
			VUI.container.addWindow(new GeolocationWin(viewer), "Go To");
		});
		contextMenu.addSeparator() ;
		contextMenu.addItem(CResourceBundle.getLocal("usageBarMenu"), e -> {
			Notification.show("Usage");
		});
	}
	private void buildCityContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);

		contextMenu.addItem(CResourceBundle.getLocal("newCityBarMenu"), e -> {
			//Notification.show("New City");
			UI.getCurrent().getNavigator().navigateTo("MapViewer");
			VUI.container.addWindow(new NewCityWin(), "Create City");
		});
		contextMenu.addItem(CResourceBundle.getLocal("cityManagerBarMenu"), e -> {
			UI.getCurrent().getNavigator().navigateTo("MapViewer");
			VUI.container.addWindow(new ManageCitiesWin(), "Manage Cities");
			//Notification.show("City Manager");
		});
		contextMenu.addItem(CResourceBundle.getLocal("ExportBarMenu"), e -> {
			VUI.container.addWindow(new ExportPdfCityWin(), "Export PDF");
			//Notification.show("City Manager");
		});
		contextMenu.addSeparator() ;
		contextMenu.addItem(CResourceBundle.getLocal("cityUsageBarMenu"), e -> {
			Notification.show("Usage");
			VUI.container.addWindow(new UsageCityWin(), "Export PDF");
		});
	}
	
	private void buildKPIContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);
		MenuItem mn = contextMenu.addItem(CResourceBundle.getLocal("newKpi"));
		
		mn.addItem(CResourceBundle.getLocal("newKpiDimension"), e -> {
			Notification.show("KPI Dimension");
			VUI.container.addWindow(new NewDimensionWin(), "");

		}) ;
		mn.addItem(CResourceBundle.getLocal("newKpiSubDimension"), e -> {
			Notification.show("KPI Sub Dimension");
			VUI.container.addWindow(new NewSubDimensionWin(), "Create New Sub-Dimension");

		}) ;
		mn.addItem(CResourceBundle.getLocal("newKpiCategory"), e -> {
			Notification.show("KPI Category");
			VUI.container.addWindow(new NewCategoryWin(), "Create New Category");

		}) ;
		mn.addItem(CResourceBundle.getLocal("newKpiKpi"), e-> {
			Notification.show("KPI ");
			VUI.container.addWindow(new NewKpiWin(), "Create New KPI");

		}) ;
		
		contextMenu.addItem(CResourceBundle.getLocal("kpiManagerItem"),e-> {
			Notification.show("KPI ");
			AppContext.panel = "";
			VUI.container.addWindow(new ManageKPIsWin(), "Manage KPIs");

		}) ;
		
		mn = contextMenu.addItem(CResourceBundle.getLocal("kpiImportItem")) ;
		mn.addItem("JSON",e-> {
			VUI.container.addWindow(new UploadJSONWin(), "Import JSON");
		}) ;
		mn.addItem("XML",e-> {

		}) ;
		
		
		contextMenu.addItem(CResourceBundle.getLocal("ExportBarMenu"),e-> {
			VUI.container.addWindow(new ExportPdfKpiWin(), "Export PDF");

		}) ;
		
		
		contextMenu.addSeparator() ;
		

		contextMenu.addItem(CResourceBundle.getLocal("kpiUsageItem"), e -> {
			Notification.show("Usage");
		});
		
	}
	
	private void buildExportContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);
		
		contextMenu.addItem(CResourceBundle.getLocal("ExportBarMenu"),e-> {
			VUI.container.addWindow(new ExportPdfBenchMarking(), "Export PDF");

		}) ;
		
		contextMenu.addSeparator() ;
		
		contextMenu.addItem(CResourceBundle.getLocal("kpiUsageItem"), e -> {
			Notification.show("Usage");
		});
		
	}
	
	private void buildUsersContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);
		
		UserE user = new UserC().fromDB().findByUName(getCurrentUser());	

		contextMenu.addItem(CResourceBundle.getLocal("addNewUserWin"),e-> {
				VUI.container.addWindow(new AddNewUserWin(), "");
				
		}) ;
		
		contextMenu.addItem(CResourceBundle.getLocal("manageUsersBarMenu"),e-> {
			if(user.getDecriminatorValue().equals("ADMIN")) {
				VUI.container.addWindow(new ManageUsersWin(), "");
			}else {
				Notification.show("You are not ALLOWED to edit Users Profile");
			}

		}) ;
		
		contextMenu.addSeparator() ;
		
		contextMenu.addItem(CResourceBundle.getLocal("kpiUsageItem"), e -> {
			Notification.show("Usage");
		});
		
	}
	
	private String getCurrentUser() {
        return (String) VaadinSession.getCurrent()
                .getAttribute("usr");
    }

}
