
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.SubDimensionD;
import com.nogroup.SMGis.data.entities.SubDimensionE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class SubDimensionCbx
    extends ComboBox<SubDimensionE>
    implements ItemCaptionGenerator<SubDimensionE>
{


    public SubDimensionCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setPlaceholder("Sub-Dimension");
        this.setItemCaptionGenerator(this);
    }

    public SubDimensionCbx(Collection<SubDimensionE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SUBDIMENSION");
        this.setPlaceholder("Sub-Dimension");

    }

    public SubDimensionCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SUBDIMENSION");
        this.setPlaceholder("Sub-Dimension");

    }

    public SubDimensionCbx(String caption, Collection<SubDimensionE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SUBDIMENSION");
        this.setPlaceholder("Sub-Dimension");

    }

    public SubDimensionCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SUBDIMENSION");
        this.setItems(new SubDimensionD().read());
        this.setPlaceholder("Sub-Dimension");
        return this;
    }

    @Override
    public String apply(SubDimensionE item) {
        return item.getName();
    }

}
