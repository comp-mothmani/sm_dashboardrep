package com.nogroup.SMGis.views.ccmp.flds.imgUpld;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class UploadDirectoryManager {
	
	/**
	 * @author mahdiothmani
	 */
	public static void initCache() {
		
		createUploadDirectory();
		createImageUploadDirectory();
		 
	}
	
	public static void createUploadDirectory() {
	
		/// return home path
		String path = System.getProperty("user.home");
		File dir = new File(path + "/smgis");
		
		if(!dir.exists()) {
			try{
				dir.mkdir(); 
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		 
	}
	
	public static void createImageUploadDirectory() {
		/// return home path
		String path = System.getProperty("user.home");
		File dir = new File(path + "/smgis/img");
		
		if(!dir.exists()) {
			try{
				dir.mkdir(); 
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void clearCache() {
		String path = System.getProperty("user.home");
		File dir = new File(path + "/smgis/img");
		File[] files = dir.listFiles();
		
		for (File tmp : files) {
			tmp.delete();
		}
	}
	
	public static String file2Text(String path) {
		String b64 = "";
		try {
			InputStream st = new FileInputStream(path) ;
			byte[] targetArray = new byte[st.available()];
			st.read(targetArray);
			b64 = Base64.getEncoder().encodeToString(targetArray) ;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return b64;
	}
	
	public static byte[] file2ByteArray(String path) {
		String b64 = "";
		try {
			InputStream st = new FileInputStream(path) ;
			byte[] targetArray = new byte[st.available()];
			st.read(targetArray);
			return targetArray ;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
