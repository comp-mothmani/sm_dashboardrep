package com.nogroup.SMGis.views.ccmp.btn;

import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;

public class ValoMenuItemButton extends Button {

    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private static final String STYLE_SELECTED = "selected";

    public ValoMenuItemButton(final int i) {
        setPrimaryStyleName("valo-menu-item");
        setCaption("View " + i);
        setIcon(FontAwesome.HOME);
        DashboardEventBus.register(this);

    }
    public ValoMenuItemButton(String caption,Resource icon, ClickListener listener) {
        setPrimaryStyleName("valo-menu-item");
        setCaption(caption);
        setIcon(icon);
        DashboardEventBus.register(this);
        addClickListener(listener) ;

    }
	public ValoMenuItemButton(String caption, FontAwesome icon) {
		setPrimaryStyleName("valo-menu-item");
        setCaption(caption);
        setIcon(icon);
        DashboardEventBus.register(this);
	}
}
