package com.nogroup.SMGis.views.ccmp.chrts;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RDDataGroup {
	
	private List<RDDataPoint> points = new ArrayList<>() ;
	private String label ;
	
	public void setLabel(String s) {
		this.label = s ;
	}
	public String getLabel() {
		return this.label ;
	}
	
	
	public List<RDDataPoint> getPoints() {
		return points;
	}


	public void setPoints(List<RDDataPoint> points) {
		this.points = points;
	}


	public RDDataPoint getLatest() {
		 Collections.sort(points, new Comparator<RDDataPoint>() {
	        @Override
	        public int compare(RDDataPoint o1, RDDataPoint o2) {
	        	return o1.getId().compareTo(o2.getId()) * (-1);
	        }
	    });
		
		return points.get(0) ;
	}
	
	public double meanValue() {
		return 0 ;
	}
	public List<Date> getDates() {
		List<Date> dates = new ArrayList<>() ;
		for(RDDataPoint tmp : points) {
			dates.add(tmp.getDate()) ;
		}
		
		Collections.sort(dates, new Comparator<Date>() {
	        @Override
	        public int compare(Date o1, Date o2) {
	        	return o1.compareTo(o2);
	        }
	    });
		
		
		return dates ;
	}
	
	public List<String> getDatesAsStrings() {
		List<String> dates = new ArrayList<>() ;
		for(Date tmp : getDates()) {
			dates.add(new SimpleDateFormat("dd/MM/yyyy").format(tmp)) ;
		}
		return dates ;
	}
	public List<Double> getValuesAsList() {
		List<Double> vals = new ArrayList<>() ;
		for(RDDataPoint tmp : points) {
			vals.add(tmp.getValue()) ;
		}
		return vals ;
	}
}
