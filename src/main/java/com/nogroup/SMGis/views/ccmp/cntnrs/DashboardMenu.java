package com.nogroup.SMGis.views.ccmp.cntnrs;

import com.github.appreciated.app.layout.builder.entities.NotificationHolder.Notification;
import com.google.common.eventbus.Subscribe;
import com.nogroup.SMGis.business.events.DashboardEvent.PostViewChangeEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.ProfileUpdatedEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.ReportsCountUpdatedEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.UserLoggedOutEvent;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.views.ccmp.btn.ValoMenuItemButton;
import com.nogroup.SMGis.views.workflow.windows.EditUserWin;
import com.nogroup.SMGis.views.workflow.windows.NewDimensionWin;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("deprecation")
public class DashboardMenu extends CustomComponent{

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ID = "dashboard-menu";
    public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
    public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
    private static final String STYLE_VISIBLE = "valo-menu-visible";
    private Label notificationsBadge;
    private Label reportsBadge;
    private MenuItem settingsItem;

	private CssLayout menuItemsLayout;	
    
    public DashboardMenu() {
        setPrimaryStyleName("valo-menu");
        setId(ID);
        setSizeUndefined();

        DashboardEventBus.register(this);

        setCompositionRoot(buildContent());
    }
    
    private Component buildContent() {
        final CssLayout menuContent = new CssLayout();
        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("no-vertical-drag-hints");
        menuContent.addStyleName("no-horizontal-drag-hints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");

        menuContent.addComponent(buildTitle());
        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        
        menuItemsLayout = new CssLayout();
        menuItemsLayout.addStyleName("valo-menuitems");
        menuContent.addComponent(menuItemsLayout);

        return menuContent;
    }

    private Component buildTitle() {
        Label logo = new Label("SMART <strong>KPI</strong>",
                ContentMode.HTML);
        logo.setSizeUndefined();
        HorizontalLayout logoWrapper = new HorizontalLayout(logo);
        logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
        logoWrapper.addStyleName("valo-menu-title");
        logoWrapper.setSpacing(false);
        return logoWrapper;
    }

    private String getCurrentUser() {
        return (String) VaadinSession.getCurrent()
                .getAttribute("usr");
    }

    private Component buildUserMenu() {
        final MenuBar settings = new MenuBar();
        settings.addStyleName("user-menu");
        final String user = getCurrentUser();
        settingsItem = settings.addItem("",
                new ThemeResource("img/profile-pic-300px.jpg"), null);
        updateUserName(null);
        /*settingsItem.addItem("Edit Profile", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                //ProfilePreferencesWindow.open(user, false);
            	
            }
        });*/
        
        settingsItem.addItem(CResourceBundle.getLocal("editUserBarMenu"), e -> {
        	
        	VUI.container.addWindow(new EditUserWin(), "");
        	
        });
        settingsItem.addItem(CResourceBundle.getLocal("preferencesBarMenu"), new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                //ProfilePreferencesWindow.open(user, true);
            }
        });
        settingsItem.addSeparator();
        settingsItem.addItem(CResourceBundle.getLocal("signOutBarMenu"), new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                DashboardEventBus.post(new UserLoggedOutEvent());
            }
        });
        return settings;
    }

   

    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                if (getCompositionRoot().getStyleName()
                        .contains(STYLE_VISIBLE)) {
                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
                } else {
                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
                }
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    @SuppressWarnings("deprecation")
	public ValoMenuItemButton addMenuItem(String caption, FontAwesome icon) {
    	ValoMenuItemButton menuItemComponent = new ValoMenuItemButton(caption,icon);
    	menuItemsLayout.addComponent(menuItemComponent);
    	return menuItemComponent ;
	}
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public  ValoMenuItemButton addMenuItem(String caption, Resource icon,ClickListener listener, String viewName, Class class1) {
    	ValoMenuItemButton menuItemComponent = new ValoMenuItemButton(caption,icon, listener);
    	menuItemsLayout.addComponent(menuItemComponent);
    	UI.getCurrent().getNavigator().addView(viewName, class1);
    	return menuItemComponent ;
    }
    
    @SuppressWarnings({ "rawtypes", "serial", "unchecked" })
	public  ValoMenuItemButton addMenuItem(String caption, Resource icon, String viewName, Class class1) {
    	ValoMenuItemButton menuItemComponent = new ValoMenuItemButton(caption,icon, new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				UI.getCurrent().getNavigator().navigateTo(viewName);
				
			}
		});
    	menuItemsLayout.addComponent(menuItemComponent);
    	UI.getCurrent().getNavigator().addView(viewName, class1);
    	return menuItemComponent ;
    }
    
    public ValoMenuItemButton addMenuItem(String caption, Resource icon,ClickListener listener) {
    	ValoMenuItemButton menuItemComponent = new ValoMenuItemButton(caption,icon, listener);
    	menuItemsLayout.addComponent(menuItemComponent);
    	return menuItemComponent ;
    }

    @Override
    public void attach() {
        super.attach();
        //updateNotificationsCount(null);
    }
    
    @Subscribe
    public void postViewChange(final PostViewChangeEvent event) {
        getCompositionRoot().removeStyleName(STYLE_VISIBLE);
    }

    @Subscribe
    public void updateReportsCount(final ReportsCountUpdatedEvent event) {
        reportsBadge.setValue(String.valueOf(event.getCount()));
        reportsBadge.setVisible(event.getCount() > 0);
    }

    @Subscribe
    public void updateUserName(final ProfileUpdatedEvent event) {
        String user = getCurrentUser();
        settingsItem.setText(user);
    }

}
