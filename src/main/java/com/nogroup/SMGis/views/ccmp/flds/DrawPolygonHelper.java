package com.nogroup.SMGis.views.ccmp.flds;

import com.nogroup.SMGis.data.embedded.VPolygon;

public interface DrawPolygonHelper {

	public void back(VPolygon vp) ;
}
