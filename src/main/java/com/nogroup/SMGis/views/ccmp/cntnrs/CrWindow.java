package com.nogroup.SMGis.views.ccmp.cntnrs;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("deprecation")
public abstract class CrWindow extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public CrWindow() {
		
		setResizable(true);
		addStyleName("v-window-outerheader");
		
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		
		MenuBar menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.FLOPPY_O,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				saveAction() ;
				CrWindow.this.close();
			}
		}) ;

		addCaptionComponent(menu);
		
	}
	
	public abstract String caption_() ;
	public abstract String width_() ;
	public abstract String height_() ;
	public abstract void saveAction() ;

}
