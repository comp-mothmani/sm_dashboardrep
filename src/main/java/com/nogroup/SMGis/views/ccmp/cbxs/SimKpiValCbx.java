
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.SimKpiValD;
import com.nogroup.SMGis.data.entities.SimKpiValE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class SimKpiValCbx
    extends ComboBox<SimKpiValE>
    implements ItemCaptionGenerator<SimKpiValE>
{


    public SimKpiValCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public SimKpiValCbx(Collection<SimKpiValE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SIMKPIVAL");
    }

    public SimKpiValCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SIMKPIVAL");
    }

    public SimKpiValCbx(String caption, Collection<SimKpiValE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SIMKPIVAL");
    }

    public SimKpiValCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE SIMKPIVAL");
        this.setItems(new SimKpiValD().read());
        return this;
    }

    @Override
    public String apply(SimKpiValE item) {
        return null;
    }

}
