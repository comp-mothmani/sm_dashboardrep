package com.nogroup.SMGis.views.ccmp.chrts;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.ChartJs.DataPointClickListener;
import com.byteowls.vaadin.chartjs.config.RadarChartConfig;
import com.byteowls.vaadin.chartjs.data.RadarDataset;
import com.nogroup.SMGis.VUI;
import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window.CloseEvent;

public class RadarChartFactory {

	private ChartJs chart;
	private RadarChartConfig config;

	public RadarChartFactory() {
		config = new RadarChartConfig();
		config.options().maintainAspectRatio(true).elements().line().tension(0.000001d).and().and().title()
				.display(false).text("Chart.js Radar Chart Fill options").and().done();
	}

	public RadarChartFactory addLabel(List<String> lbls) {
		config.data().labelsAsList(lbls);
		return this;
	}

	public RadarChartFactory addLabel(String[] lbls) {

		config.data().labelsAsList(Arrays.asList(lbls));
		return this;
	}

	public RadarChartFactory addDataSet(RadarDataset dset) {
		config.data().addDataset(dset.fill(true));
		return this;
	}

	public RadarChartFactory addBean(RDDataSet dSet) {
		RadarDataset dset = new RadarDataset();
		dset.label(dSet.getName());
		Color c = randomColor() ;
		int[] rgb = new int[] { c.getRed(), c.getGreen(), c.getBlue() };
		
		String hex = "#"+Integer.toHexString(c.getRGB()).substring(2);
		
		
		Color ca = new Color(c.getRed(), c.getGreen(), c.getBlue(), 50) ;
		dset.borderColor(hex);
		dset.backgroundColor("rgba(" + c.getRed() + ", " + c.getGreen() + ", " + c.getBlue() + ", " + 0.5 + " )");
				
		dset.dataAsList(dSet.getLatestPointsAsList());

		config.data().addDataset(dset.fill(true));
		return this;
	}
	
	public RadarChartFactory addBean(RadarChartBean bean) {

		RadarDataset dset = new RadarDataset();
		dset.label(bean.getName());
		Color c = randomColor() ;
		int[] rgb = new int[] { c.getRed(), c.getGreen(), c.getBlue() };
		
		String hex = "#"+Integer.toHexString(c.getRGB()).substring(2);
		System.out.println(String.format("#%d%d%d", rgb[0], rgb[1], rgb[2]));
		
		
		Color ca = new Color(c.getRed(), c.getGreen(), c.getBlue(), 50) ;
		dset.borderColor(hex);
		dset.backgroundColor("rgba(" + c.getRed() + ", " + c.getGreen() + ", " + c.getBlue() + ", " + 0.5 + " )");

		dset.dataAsList(bean.getData());

		config.data().addDataset(dset.fill(true));
		return this;
	}

	public ChartJs render(String caption, DataPointClickListener listener) {
		chart = new ChartJs(config);
		chart.setCaption("<h1>" + caption + "</h1>");
		chart.setCaptionAsHtml(true);
		
		
		chart.setShowDownloadAction(true);
        chart.setDownloadActionText("Download");
        chart.setDownloadSetWhiteBackground(true);
        chart.setDownloadActionFilename("vaadin-chartsjs-demo.png");
        chart.addMenuEntry("Scatter", new Runnable() {
			
			@Override
			public void run() {
				VUI.container.addWindow(new FullScreen(config), "" + caption);
			}
		});
        chart.addMenuEntry("Edit",
            () -> Notification.show("Notification triggered by from menu entry", Notification.Type.ERROR_MESSAGE));

        
		chart.addClickListener(listener);
		chart.setHeight("100%");
		chart.setWidth("100%");
		chart.setJsLoggingEnabled(true);
		return chart;
	}

	public Color randomColor() {
		
		Random random = new Random(); // Probably really put this somewhere where it gets executed only once
		int red = random.nextInt(256);
		int green = random.nextInt(256);
		int blue = random.nextInt(256);
		return new Color(red, green, blue);
	}

	public int toHex(Color color) {
	    String alpha = pad(Integer.toHexString(color.getAlpha()));
	    String red = pad(Integer.toHexString(color.getRed()));
	    String green = pad(Integer.toHexString(color.getGreen()));
	    String blue = pad(Integer.toHexString(color.getBlue()));
	    String hex = "0x" + alpha + red + green + blue;
	    return Integer.parseInt(hex, 16);
	}
	
	public Color color(int i ) {
		return new Color(i, true) ;
	}

	private static final String pad(String s) {
	    return (s.length() == 1) ? "0" + s : s;
	}

	public String getDataPoint(int arg0, int arg1) {
		config.data().getDatasetAtIndex(arg0) ;
		return config.data().getLabels().get(arg1) ;	
	}
	
	protected class FullScreen extends CWindow{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		FullScreen(RadarChartConfig config){
			
			ChartJs chart2 = new ChartJs(config);
			chart2.setCaptionAsHtml(true);

			chart2.addMenuEntry("Edit",
	            () -> Notification.show("Notification triggered by from menu entry", Notification.Type.ERROR_MESSAGE));

	        
			chart2.setHeight("100%");
			chart2.setWidth("100%");
			chart2.setJsLoggingEnabled(true);
			
			Panel pane = new Panel() ;
			pane.setContent(chart2);
			addContent(pane);
		}
		@Override
		public void windowClosed(CloseEvent e) {
			
		}
		
	}

	
}
