
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.CityParamD;
import com.nogroup.SMGis.data.entities.CityParamE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CityParamCbx
    extends ComboBox<CityParamE>
    implements ItemCaptionGenerator<CityParamE>
{


    public CityParamCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public CityParamCbx(Collection<CityParamE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITYPARAM");
    }

    public CityParamCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITYPARAM");
    }

    public CityParamCbx(String caption, Collection<CityParamE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITYPARAM");
    }

    public CityParamCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITYPARAM");
        this.setItems(new CityParamD().read());
        return this;
    }

    @Override
    public String apply(CityParamE item) {
        return null;
    }

}
