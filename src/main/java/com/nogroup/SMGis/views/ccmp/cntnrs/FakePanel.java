package com.nogroup.SMGis.views.ccmp.cntnrs;

import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.ValoTheme;

public class FakePanel extends CssLayout {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	private final HorizontalLayout caption = new HorizontalLayout();
	private final HorizontalLayout captionLayout = new HorizontalLayout();
	private final Panel content = new Panel();
	private final Label captionText = new Label("");
	private Image iconContainer;

	public FakePanel() {
		setSizeFull();
		//addStyleName(ValoTheme.LAYOUT_CARD);

		//this.caption.addStyleName("v-panel-caption");
		this.caption.setWidth("100%");
		this.caption.setSpacing(true);
		addComponent(this.caption);
		
		this.captionText.setWidth("100%");
		this.caption.addComponent(this.captionText);
		this.caption.setExpandRatio(this.captionText, 1);

		this.content.setSizeFull();
		addComponent(this.content);
		
		this.caption.addComponent(this.captionLayout);
		
		this.captionLayout.setSpacing(false);
	}

	@Override
	public void setIcon(final Resource icon) {
		if (this.iconContainer != null) {
			this.caption.removeComponent(this.iconContainer);
		}
		this.iconContainer = new Image(null, icon);
		this.caption.addComponent(this.iconContainer, 0);
		this.caption.setComponentAlignment(this.iconContainer, Alignment.MIDDLE_LEFT);
		this.caption.setExpandRatio(this.iconContainer, 0);
	}

	@Override
	public void setCaption(final String caption) {
		this.captionText.setValue(caption);
		this.captionText.addStyleName(ValoTheme.LABEL_TINY);
	}
	
	public void addCaptionComponent(Component cmp) {
		this.captionLayout.addComponent(cmp,0);
	}
	
	public void addContent(Component cmp) {
		this.content.setSizeFull();
		//this.content.removeAllComponents();
		//this.content.setMargin(false);
		//this.content.addComponent(cmp);
		this.content.setContent(cmp);
	}

}