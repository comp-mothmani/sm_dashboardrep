
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.CategoryD;
import com.nogroup.SMGis.data.entities.CategoryE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CategoryCbx
    extends ComboBox<CategoryE>
    implements ItemCaptionGenerator<CategoryE>
{


    public CategoryCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);

    }

    public CategoryCbx(Collection<CategoryE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx(String caption, Collection<CategoryE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
        this.setItems(new CategoryD().read());
        return this;
    }

    @Override
    public String apply(CategoryE item) {
        return item.getName();
    }

}
