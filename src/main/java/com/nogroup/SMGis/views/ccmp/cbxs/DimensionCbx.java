
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.DimensionD;
import com.nogroup.SMGis.data.entities.DimensionE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class DimensionCbx
    extends ComboBox<DimensionE>
    implements ItemCaptionGenerator<DimensionE>
{


    public DimensionCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setPlaceholder("Dimension");
    }

    public DimensionCbx(Collection<DimensionE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE DIMENSION");
        this.setPlaceholder("Dimension");

    }

    public DimensionCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE DIMENSION");
        this.setPlaceholder("Dimension");

    }

    public DimensionCbx(String caption, Collection<DimensionE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE DIMENSION");
        this.setPlaceholder("Dimension");

    }

    public DimensionCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE DIMENSION");
        this.setItems(new DimensionD().read());
        this.setPlaceholder("Dimension");
        return this;
    }

    @Override
    public String apply(DimensionE item) {
        return  item.getName();
    }

}
