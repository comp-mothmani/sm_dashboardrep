package com.nogroup.SMGis.views.ccmp.cntnrs;

import com.nogroup.SMGis.VUI;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

public abstract class CWindow extends Window {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	public FakePanel layout;
	
	public CWindow() {
		addStyleName("yellowsub");
        center();
        setWidth("50%");
        setHeight("50%");
        setResizable(false);
        setClosable(false);
        setModal(false);
        setDraggable(true);
        
        layout = new FakePanel();
        layout.setCaption("");
        layout.setIcon(FontAwesome.ANDROID);
        
        MenuBar mn = new MenuBar() ;
        mn.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
        
        MenuItem mnn = mn.addItem("",FontAwesome.ADJUST,null) ;
        mnn.addItem("",FontAwesome.PLUS,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				if(selectedItem.getIcon().equals(FontAwesome.PLUS)) {
					setWindowMode(WindowMode.MAXIMIZED);
					selectedItem.setIcon(FontAwesome.MINUS);
				}else {
					setWindowMode(WindowMode.NORMAL);
					selectedItem.setIcon(FontAwesome.PLUS);
				}				
			}
		}) ;
        
        mnn.addItem("",FontAwesome.LOCK,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				CWindow.this.setVisible(false);
				VUI.container.setSelectedTab(0);
			}
		}) ;

        mnn.addItem("",FontAwesome.CLOSE,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				close();
			}
		}) ;
        layout.addCaptionComponent(mn);
        setContent(layout);
        
		addCloseListener(new CloseListener() {
			
			@Override
			public void windowClose(CloseEvent e) {
				windowClosed(e) ;
			}
		});
	}
	
	public void addCaptionComponent(Component cmp) {
		layout.addCaptionComponent(cmp);
	}
	public void addContent(Component cmp) {
		this.layout.addContent(cmp);
	}
	
	public void caption(String s) {
		layout.setCaption(s);
	}
	public abstract void windowClosed(CloseEvent e) ;
	
}
