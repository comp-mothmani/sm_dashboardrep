package com.nogroup.SMGis.views.ccmp.cntnrs;

import java.util.HashMap;
import java.util.Map;

import com.nogroup.SMGis.VUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class TabContainer extends TabSheet {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private Map<Tab,Window> wins = new HashMap<>() ;
	
	public TabContainer() {
		
		VerticalLayout vl = new VerticalLayout() ;
		Tab tab = addTab(vl,"") ;
		
		setCloseHandler(new CloseHandler() {
			
			@Override
			public void onTabClose(TabSheet tabsheet, Component tabContent) {
				Tab tab = (Tab) ((VerticalLayout)tabContent).getData() ;
				wins.get(tab).close();
				wins.remove(tab) ;
				tabsheet.removeTab(tab);
				
			}
		});
		
		addSelectedTabChangeListener(new SelectedTabChangeListener() {
			
			@Override
			public void selectedTabChange(SelectedTabChangeEvent event) {
				try {
					Tab tab = (Tab) ((VerticalLayout)getSelectedTab()).getData() ;
					if(!wins.get(tab).isVisible()) {
						wins.get(tab).setVisible(true);
						wins.get(tab).bringToFront();
					}else {
						wins.get(tab).setVisible(false);
					}
					setSelectedTab(0);
				}catch (Exception e) {
				}
				
			}
		});
	}
	
	public void addWindow(Window win, String caption) {
		VUI ui = (VUI) getUI() ;
		ui.addWindow(win);
		
		VerticalLayout vl = new VerticalLayout() ;
		Tab tab = addTab(vl,caption) ;
		vl.setData(tab);
		tab.setClosable(true);
		
		wins.put(tab, win) ;
		
		win.addCloseListener(new CloseListener() {
			
			@Override
			public void windowClose(CloseEvent e) {
				removeTab(tab);
				wins.remove(tab);
			}
		});
	}

}
