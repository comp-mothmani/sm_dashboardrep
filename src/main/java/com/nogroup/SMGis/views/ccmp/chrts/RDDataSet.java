package com.nogroup.SMGis.views.ccmp.chrts;

import java.util.ArrayList;
import java.util.List;

public class RDDataSet {
	
	private String name ;
	private List<RDDataGroup> points = new ArrayList<>() ;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RDDataGroup> getPoints() {
		return points;
	}
	public void setPoints(List<RDDataGroup> points) {
		this.points = points;
	}
	public List<RDDataPoint> getLatestPoints() {
		List<RDDataPoint> grps = new ArrayList<>() ;
		for(RDDataGroup tmp : points) {
			grps.add(tmp.getLatest()) ;
		}
		return grps ;
	}
	public List<Double> getLatestPointsAsList() {
		List<Double> grps = new ArrayList<>() ;
		for(RDDataPoint tmp : getLatestPoints()) {
			grps.add(tmp.getValue()) ;
		}
		return grps ;
	}
	public RDDataGroup findGroupByName(String c) {
		for(RDDataGroup tmp : points) {
			if(tmp.getLabel().equals(c)) {
				return tmp ;
			}
		}
		return null ;
	}
	
	
}
