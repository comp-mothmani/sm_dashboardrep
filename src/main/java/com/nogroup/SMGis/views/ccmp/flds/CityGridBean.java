package com.nogroup.SMGis.views.ccmp.flds;

public interface CityGridBean {
	public String fetchName() ;
	public String fetchSubDimension() ;
	public String fetchCategory() ;
	public String fetchUnit() ;
	public String fetchValue() ;
}
