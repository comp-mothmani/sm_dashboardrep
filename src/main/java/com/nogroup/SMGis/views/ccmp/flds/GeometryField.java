package com.nogroup.SMGis.views.ccmp.flds;

import com.nogroup.SMGis.data.embedded.VPolygon;
import com.nogroup.SMGis.views.workflow.embedded.MapViewer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

public class GeometryField extends HorizontalLayout {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	private VPolygon poly = new VPolygon();
	private TextField tf;
	private TextField tf1;
	private Button btn;
	private Button btn1;
	private MapViewer viewer;

	public GeometryField(WindowVisibility windowVisibility) {
		viewer = (MapViewer)UI.getCurrent().getNavigator().getCurrentView() ;
		//this.viewer = viewer;
		init();

		btn.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				windowVisibility.invisible();
				viewer.drawPolygon(new DrawPolygonHelper() {
					
					@Override
					public void back(VPolygon vp) {
						poly = vp;
						tf.setValue("" + vp.center().getLat());
						tf1.setValue("" + vp.center().getLon());
						windowVisibility.visible();
					}
				});
			}
		});
		
		btn1.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				tf.setValue("");
				tf1.setValue("");
				poly = new VPolygon() ;
				viewer.clearSelectionLayer();
			}
		});
	}
	

	public void init() {
		setWidth("100%");

		tf = new TextField();
		tf.setEnabled(false);
		tf.setWidth("100%");
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		tf.setPlaceholder("LATTITUDE");
		addComponent(tf);

		tf1 = new TextField();
		tf1.setEnabled(false);
		tf1.setWidth("100%");
		tf1.setPlaceholder("LONGITUDE");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		addComponent(tf1);

		btn = new Button();
		btn.setWidth("100%");
		btn.addStyleName(ValoTheme.BUTTON_TINY);
		btn.setIcon(FontAwesome.MAP_MARKER);
		addComponent(btn);

		btn1 = new Button();
		btn1.setWidth("100%");
		btn1.addStyleName(ValoTheme.BUTTON_TINY);
		btn1.setIcon(FontAwesome.CUT);
		addComponent(btn1);

	}


	public VPolygon getPoly() {
		return poly ;
	}
	
}
