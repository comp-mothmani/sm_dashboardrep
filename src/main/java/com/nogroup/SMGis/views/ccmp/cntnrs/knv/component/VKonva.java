package com.nogroup.SMGis.views.ccmp.cntnrs.knv.component;

import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractJavaScriptComponent;

@JavaScript({"vkonvalib.js", "vkonva-connector.js"})
public class VKonva extends AbstractJavaScriptComponent {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;

	public VKonva() {
		
	}

	public void addLegend() {
		callFunction("render");
	}
	
	public void download() {
		callFunction("download");
	}
	
	public void addImage() {
		callFunction("insertImage");
	}
	
	public void addImage2(String img) {
		callFunction("insertImage2",img);
	}
}
