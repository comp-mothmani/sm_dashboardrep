
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.CountryD;
import com.nogroup.SMGis.data.entities.CountryE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CountryCbx
    extends ComboBox<CountryE>
    implements ItemCaptionGenerator<CountryE>
{


    public CountryCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public CountryCbx(Collection<CountryE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRY");
    }

    public CountryCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRY");
    }

    public CountryCbx(String caption, Collection<CountryE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRY");
    }

    public CountryCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRY");
        this.setItems(new CountryD().read());
        return this;
    }

    public void initDemo() {
    	this.setItemCaptionGenerator(this);
    	this.setItems(new CountryE("Tunisia"),new CountryE("France"),new CountryE("Algeria"));
    }
    @Override
    public String apply(CountryE item) {
        return item.getName();
    }

}
