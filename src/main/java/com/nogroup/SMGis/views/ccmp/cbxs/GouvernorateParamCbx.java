
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.GouvernorateParamD;
import com.nogroup.SMGis.data.entities.GouvernorateParamE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class GouvernorateParamCbx
    extends ComboBox<GouvernorateParamE>
    implements ItemCaptionGenerator<GouvernorateParamE>
{


    public GouvernorateParamCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public GouvernorateParamCbx(Collection<GouvernorateParamE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATEPARAM");
    }

    public GouvernorateParamCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATEPARAM");
    }

    public GouvernorateParamCbx(String caption, Collection<GouvernorateParamE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATEPARAM");
    }

    public GouvernorateParamCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATEPARAM");
        this.setItems(new GouvernorateParamD().read());
        return this;
    }

    @Override
    public String apply(GouvernorateParamE item) {
        return null;
    }

}
