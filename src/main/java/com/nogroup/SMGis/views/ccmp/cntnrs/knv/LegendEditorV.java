package com.nogroup.SMGis.views.ccmp.cntnrs.knv;

import com.nogroup.SMGis.views.ccmp.cntnrs.CWindow;
import com.nogroup.SMGis.views.ccmp.cntnrs.knv.component.VKonva;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class LegendEditorV extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	VKonva knv = null ;
	private String img;
	
	
	public LegendEditorV() {
		setResizable(true);
		addStyleName("v-window-outerheader");
		
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		
		MenuBar menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.DOWNLOAD,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				downloadAction() ;
				LegendEditorV.this.close();
			}
		}) ;

		addCaptionComponent(menu);
		
		addContent(new VContent());
	}

	public LegendEditorV(String img) {
		this.img = img ;
		
		setResizable(true);
		addStyleName("v-window-outerheader");
		
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		
		MenuBar menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.DOWNLOAD,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				downloadAction() ;
				LegendEditorV.this.close();
			}
		}) ;

		addCaptionComponent(menu);
		
		addContent(new VContent());
		
		
	}

	@Override
	public void windowClosed(CloseEvent e) {

	}
	
	public String caption_() {
		return "Legend Editor" ;
	}
	public String width_() {
		return "80%" ;
	}
	public String height_() {
		return "80%" ;
	}
	public void downloadAction(){
		
	}
	
	protected class VContent extends VerticalLayout{

		/**
		 * @author medzied
		 */
		private static final long serialVersionUID = 1L;
		
		public VContent() {
			setSizeFull();
			HorizontalLayout hznl = new HorizontalLayout() ;
			addComponent(hznl);
			hznl.setHeight("50px");
			//setExpandRatio(hznl, 1);
			
			Button btn = new Button("Download", new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					knv.download();
				}
			}) ;
			hznl.addComponent(btn);
			
			Button btn1 = new Button("Legend", new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					knv.addLegend() ;
					
				}
			}) ;
			hznl.addComponent(btn1);
			
			Button btn2 = new Button("Base", new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					knv.addImage();
					
				}
			}) ;
			hznl.addComponent(btn2);
			
			Button btn3 = new Button("Base", new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					//img = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBoZWlnaHQ9IjI0cHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyLjAwMDA0IiB3aWR0aD0iMjRweCI+PHBhdGggZD0ibTUxMS4xMzI4MTIgNzkuOTI5Njg4Yy0uMDE5NTMxLTIxLjM5MDYyNi04LjM2NzE4Ny00MS40ODgyODItMjMuNTA3ODEyLTU2LjU5Mzc1LTMxLjIyNjU2Mi0zMS4xNTYyNS04MS45OTIxODgtMzEuMTEzMjgyLTExMy4xODM1OTQuMTE3MTg3bC0zMjIuMjA3MDMxIDMyMy41MDM5MDZjLTEwLjQ4MDQ2OSAxMC40NzI2NTctMTguNDgwNDY5IDIzLjQzNzUtMjMuMTM2NzE5IDM3LjQ5NjA5NGwtLjMwMDc4MS45MTQwNjMtMjguNzk2ODc1IDEyNi42MzI4MTIgMTI2Ljk4NDM3NS0yOC40Mjk2ODguOTQ1MzEzLS4zMTI1YzE0LjA2MjUtNC42NTYyNSAyNy4wMzUxNTYtMTIuNjQ4NDM3IDM3LjU0Mjk2OC0yMy4xNTIzNDNsMzIyLjI1LTMyMy41NDI5NjljMTUuMTEzMjgyLTE1LjEzMjgxMiAyMy40Mjk2ODgtMzUuMjQ2MDk0IDIzLjQxMDE1Ni01Ni42MzI4MTJ6bS00NDAuNzE0ODQzIDM3NS4zNDM3NS0xMy40NjQ4NDQtMTMuNDcyNjU3IDkuNzIyNjU2LTQyLjc2NTYyNSA0Ni42MTMyODEgNDYuNjQwNjI1em0zODkuMDAzOTA2LTM0Ni45Mzc1LTMxMi44NDc2NTYgMzE0LjEwNTQ2OC01Ni42NTIzNDQtNTYuNjg3NSAyMTQuMzAwNzgxLTIxNS4xNjAxNTYgMzIuNjMyODEzIDMyLjYzMjgxMiAyOC4yNjE3MTktMjguMjYxNzE4LTMyLjY5MTQwNy0zMi42OTE0MDYgMzAuNDAyMzQ0LTMwLjUxOTUzMiAzMi43NSAzMi43NSAyOC4yNjE3MTktMjguMjYxNzE4LTMyLjgwODU5NC0zMi44MDg1OTQgMTEuNzA3MDMxLTExLjc1MzkwNmMxNS42MDU0NjktMTUuNjI1IDQxLjAyMzQzOC0xNS42NDg0MzggNTYuNjU2MjUtLjA1MDc4MiA3LjU3ODEyNSA3LjU2MjUgMTEuNzU3ODEzIDE3LjYyNSAxMS43Njk1MzEgMjguMzMyMDMyLjAwNzgxMyAxMC43MTA5MzctNC4xNTIzNDMgMjAuNzc3MzQzLTExLjc0MjE4NyAyOC4zNzV6bS0yNDkuMTY0MDYzIDM2My4yNjE3MThoMzAwLjg3NXYzOS45Njg3NWgtMzQwLjcwNzAzMXptMCAwIiBmaWxsPSIjMDAwMDAwIi8+PC9zdmc+Cg==" ;

					knv.addImage2(img);
					
				}
			}) ;
			hznl.addComponent(btn3);
			
			
			addComponent(knv = new VKonva());
			knv.setSizeFull();
			setExpandRatio(knv, 1);
		}
	}

}
