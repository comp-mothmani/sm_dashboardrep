package com.nogroup.SMGis.views.ccmp.flds.imgUpld;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.entities.UserE;
import com.vaadin.server.Page;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.VerticalLayout;

public class ImageUpload extends VerticalLayout implements Receiver, FinishedListener  {
	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private String filename;
	private String mimeType;
	private File file;
	private String encodedString;

	public ImageUpload() {
		
		setMargin(false);
		
		UploadDirectoryManager.clearCache();
		CustomLayout cl = new CustomLayout("ImageUpload");
        Upload upload = new Upload(null,null);
        upload.setButtonCaption("Upload");	
        upload.setReceiver(this);
        upload.addFinishedListener(this);
        cl.addComponent(upload, "okBtn");
        
        addComponent(cl);
	}

	public ImageUpload(UserE entity) {
		setMargin(false);
		
		UploadDirectoryManager.clearCache();
		CustomLayout cl = new CustomLayout("ImageUpload");
        Upload upload = new Upload(null,null);
        upload.setButtonCaption(CResourceBundle.getLocal("editUserWinUpload"));	
        upload.setReceiver(this);
        upload.addFinishedListener(this);
        cl.addComponent(upload, "okBtn");
        addComponent(cl);
        
        cl.addAttachListener(new AttachListener() {
			
			@Override
			public void attach(AttachEvent event) {
				if(entity.getImg() == null) {
					Page.getCurrent().getJavaScript().execute( 
							"document.getElementById('imageuploadtag').setAttribute('src', 'data:image/svg+xml;base64," + ImgStatic.usrProfile + "');" 
							);
				}else {
					Page.getCurrent().getJavaScript().execute( 
							"document.getElementById('imageuploadtag').setAttribute('src', 'data:image/svg+xml;base64," + entity.getImg() + "');" 
							);
				}
			}
		});
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		FileOutputStream fos = null;
		try {
			String path = System.getProperty("user.home");
			this.file = new File(path + "/smgis/img/" + filename );
			fos = new FileOutputStream(this.file);
			return fos;
		}catch (Exception e) {
			e.printStackTrace();
			UI.getCurrent().showNotification("Could not upload", Notification.TYPE_ERROR_MESSAGE);
		}
		return null;
	}

	@Override
	public void uploadFinished(FinishedEvent event) {
		String path = System.getProperty("user.home");
		try{
			this.encodedString = UploadDirectoryManager.file2Text(path + "/smgis/img/" + this.filename);
		}catch (Exception e) {
			e.printStackTrace();
			UI.getCurrent().showNotification("Could not encode file", Notification.TYPE_ERROR_MESSAGE);
		}
		
		Page.getCurrent().getJavaScript().execute( 
				"document.getElementById('imageuploadtag').setAttribute('src', 'data:image/svg+xml;base64," + this.encodedString + "');" 
				);
		
	}

	public String getEncodedString() {
		return encodedString;
	}

	public void setEncodedString(String encodedString) {
		this.encodedString = encodedString;
	}
	
	
	

}
