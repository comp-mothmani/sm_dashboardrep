
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import java.util.List;

import com.nogroup.SMGis.data.daos.CityD;
import com.nogroup.SMGis.data.entities.CityE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CityCbx
    extends ComboBox<CityE>
    implements ItemCaptionGenerator<CityE>
{


    public CityCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public CityCbx(Collection<CityE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITY");
    }

    public CityCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITY");
    }

    public CityCbx(String caption, Collection<CityE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITY");
    }

    public CityCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CITY");
        List<CityE> cities = new CityD().read() ;
        this.setItems(cities);
        
        if(cities.size() != 0) {
        	this.setValue(cities.get(0));
        }
        return this;
    }

    @Override
    public String apply(CityE item) {
        return item.getName();
    }

}
