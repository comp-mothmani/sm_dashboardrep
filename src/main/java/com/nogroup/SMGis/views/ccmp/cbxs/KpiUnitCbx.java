package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.nogroup.SMGis.data.entities.DimensionE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class KpiUnitCbx extends ComboBox<String>  implements ItemCaptionGenerator<String> {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	public KpiUnitCbx() {
		
		List<String> units = new ArrayList<String>();
		units.add("Percentage");
		units.add("Value");
		
		this.setItems(units);
	    this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setPlaceholder("Unit");
	
	}

	@Override
	public String apply(String item) {
		return item;
	}

}
