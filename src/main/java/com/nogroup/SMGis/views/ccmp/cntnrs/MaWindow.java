package com.nogroup.SMGis.views.ccmp.cntnrs;

import org.vaadin.dialogs.ConfirmDialog;

import com.nogroup.SMGis.AppContext;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.views.ccmp.flds.TreeBean;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public abstract class MaWindow extends CWindow {

	
	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	private Panel pane;
	private Panel pane1;

	public MaWindow() {
		
		setResizable(true);
		addStyleName("v-window-outerheader");
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		
		MenuBar menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.FLOPPY_O,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				saveAction() ;
				//MaWindow.this.close();
			}
		}) ;

		addCaptionComponent(menu);
		
		menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.TRASH_O,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				
				ConfirmDialog.show(UI.getCurrent(), "Please Confirm:", "Are you really sure?",
		        "I am", "Cancel", new ConfirmDialog.Listener() {

		            public void onClose(ConfirmDialog dialog) {
		                if (dialog.isConfirmed()) {
		                	deleteAction() ;
		                } else {
		                }
		            }
		        });
			}
		}) ;

		addCaptionComponent(menu);
		
		VerticalLayout vl = new VerticalLayout() ;
		vl.setHeight("100%");
		vl.setMargin(false);
		addContent(vl);
		
		HorizontalLayout hznl = new HorizontalLayout() ;
		vl.addComponent(hznl);
		hznl.setWidth("100%");
		hznl.setHeight("100%");
		
		VerticalLayout vl1 = new VerticalLayout() ;
		vl1.setHeight("100%");
		vl1.setMargin(false);
	
		pane = new Panel();
		pane.setHeight("90%");
		pane.addStyleName(ValoTheme.PANEL_BORDERLESS);
		vl1.addComponent(pane);
		hznl.addComponent(vl1);
		hznl.setExpandRatio(vl1, (float) 2);
		
		VerticalLayout vl2 = new VerticalLayout() ;
		vl2.setHeight("100%");
		vl2.setMargin(false);
		pane1 = new Panel(CResourceBundle.getLocal("maWindowDetails")) ;
		pane1.addStyleName(ValoTheme.PANEL_BORDERLESS);
		pane1.setHeight("90%");
		vl2.addComponent(pane1);
		hznl.addComponent(vl2);
		hznl.setExpandRatio(vl2, 4);
		
	}
	
	public  void initTree(TreeData<TreeBean> treeData,ItemClickListener<TreeBean> listener) {
		// An initial planet tree
		Tree<TreeBean> tree = new Tree<>();
		tree.setHeight("100%");
		tree.setWidth("90%");

		tree.setItemCaptionGenerator(new ItemCaptionGenerator<TreeBean>() {
			
			@Override
			public String apply(TreeBean item) {
				return item.fetchName();
			}
		});
		
		
		tree.addStyleName("v-captiontext");

		TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeData);
		tree.setDataProvider(inMemoryDataProvider);
		tree.addItemClickListener(listener) ;
		pane.setContent(tree);
		
	}

	public void setDetails(Component cmp) {
		pane1.setContent(cmp);
	}
	public abstract void switch2() ;
	public abstract void initData() ;
	public abstract String caption_() ;
	public abstract String width_() ;
	public abstract String height_() ;
	public abstract void saveAction() ;
	public abstract void deleteAction() ;
	
	public Panel getPane() {
		return pane;
	}

	public void setPane(Panel pane) {
		this.pane = pane;
	}
}
