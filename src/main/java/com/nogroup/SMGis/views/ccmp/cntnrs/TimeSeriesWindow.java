package com.nogroup.SMGis.views.ccmp.cntnrs;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

public abstract class TimeSeriesWindow extends CWindow {

	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	public MenuBar menu;

	public TimeSeriesWindow() {
		setResizable(true);
		addStyleName("v-window-outerheader");
		
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.PLUS,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				addValueAction() ;
			}
		}) ;

		addCaptionComponent(menu);
		
		/*
		menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.EDIT,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				editValueAction() ;
			}
		}) ;

		addCaptionComponent(menu);*/
		
	}
	public abstract void addValueAction() ;
	public abstract void editValueAction() ;
	public abstract String caption_() ;
	public abstract String width_() ;
	public abstract String height_() ;
	@Override
	public void windowClosed(CloseEvent e) {

	}

}
