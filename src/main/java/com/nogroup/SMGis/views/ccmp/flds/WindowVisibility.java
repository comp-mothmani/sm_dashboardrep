package com.nogroup.SMGis.views.ccmp.flds;

public interface WindowVisibility {

	public void visible() ;
	public void invisible() ;
}
