
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.GouvernorateD;
import com.nogroup.SMGis.data.entities.GouvernorateE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class GouvernorateCbx
    extends ComboBox<GouvernorateE>
    implements ItemCaptionGenerator<GouvernorateE>
{


    public GouvernorateCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public GouvernorateCbx(Collection<GouvernorateE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATE");
    }

    public GouvernorateCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATE");
    }

    public GouvernorateCbx(String caption, Collection<GouvernorateE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATE");
    }

    public GouvernorateCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE GOUVERNORATE");
        this.setItems(new GouvernorateD().read());
        return this;
    }

    @Override
    public String apply(GouvernorateE item) {
        return item.getName();
    }

	public void switch2(String name) {
		this.setItemCaptionGenerator(this);
		if(name.equals("Tunisia")) {
			setItems(new GouvernorateE("Bizerte"),new GouvernorateE("Tunis"),new GouvernorateE("Sousse"));
		}else if(name.equals("France")) {
			setItems(new GouvernorateE("Paris"),new GouvernorateE("Nice"),new GouvernorateE("Toulon"));
		}else if(name.equals("Algeria")){
			setItems(new GouvernorateE("Alger"),new GouvernorateE("Guellma"),new GouvernorateE("Annaba"));
		}
	}

}
