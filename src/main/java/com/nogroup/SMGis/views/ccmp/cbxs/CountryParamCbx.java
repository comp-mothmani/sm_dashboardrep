
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.CountryParamD;
import com.nogroup.SMGis.data.entities.CountryParamE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CountryParamCbx
    extends ComboBox<CountryParamE>
    implements ItemCaptionGenerator<CountryParamE>
{


    public CountryParamCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public CountryParamCbx(Collection<CountryParamE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRYPARAM");
    }

    public CountryParamCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRYPARAM");
    }

    public CountryParamCbx(String caption, Collection<CountryParamE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRYPARAM");
    }

    public CountryParamCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE COUNTRYPARAM");
        this.setItems(new CountryParamD().read());
        return this;
    }

    @Override
    public String apply(CountryParamE item) {
        return null;
    }

}
