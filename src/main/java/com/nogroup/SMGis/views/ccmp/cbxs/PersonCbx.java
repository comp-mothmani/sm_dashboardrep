
package com.nogroup.SMGis.views.ccmp.cbxs;

import java.util.Collection;
import com.nogroup.SMGis.data.daos.PersonD;
import com.nogroup.SMGis.data.entities.PersonE;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class PersonCbx
    extends ComboBox<PersonE>
    implements ItemCaptionGenerator<PersonE>
{


    public PersonCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public PersonCbx(Collection<PersonE> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE PERSON");
    }

    public PersonCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE PERSON");
    }

    public PersonCbx(String caption, Collection<PersonE> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE PERSON");
    }

    public PersonCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE PERSON");
        this.setItems(new PersonD().read());
        return this;
    }

    @Override
    public String apply(PersonE item) {
        return null;
    }

}
