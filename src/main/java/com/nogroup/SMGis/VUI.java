package com.nogroup.SMGis;

import java.util.Locale;

import javax.servlet.annotation.WebServlet;

import com.google.common.eventbus.Subscribe;
import com.nogroup.SMGis.business.events.DashboardEvent.BrowserResizeEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.MapViewVisited;
import com.nogroup.SMGis.business.events.DashboardEvent.UserLoggedOutEvent;
import com.nogroup.SMGis.business.events.DashboardEvent.UserLoginRequestedEvent;
import com.nogroup.SMGis.business.events.DashboardEventBus;
import com.nogroup.SMGis.business.internationalization.CResourceBundle;
import com.nogroup.SMGis.data.collections.UserC;
import com.nogroup.SMGis.data.entities.UserE;
import com.nogroup.SMGis.data.utils.InitDB;
import com.nogroup.SMGis.views.ccmp.cntnrs.TabContainer;
import com.nogroup.SMGis.views.ccmp.cntnrs.knv.LegendEditorV;
import com.nogroup.SMGis.views.ccmp.flds.imgUpld.UploadDirectoryManager;
import com.nogroup.SMGis.views.workflow.home.LoginView;
import com.nogroup.SMGis.views.workflow.home.MainView;
import com.nogroup.SMGis.views.workflow.home.SwitcherView;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("VTheme")
@JavaScript(value = { "https://unpkg.com/konva@3.2.7/konva.min.js" })
public class VUI extends UI {

    /**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	public static TabContainer container;
	private final DashboardEventBus dashboardEventbus = new DashboardEventBus();
	boolean mapViewVisited = false ;
	public CResourceBundle bundle;
	
	@Override
    protected void init(VaadinRequest vaadinRequest) {
		
		bundle = new CResourceBundle() ;
		bundle.init();
		
		UploadDirectoryManager.initCache();
		
		VaadinSession.getCurrent().setAttribute("lang", "en");
		setLocale(Locale.US);
		DashboardEventBus.register(this);
		Responsive.makeResponsive(this);
		addStyleName(ValoTheme.UI_WITH_MENU);

		Page.getCurrent().addBrowserWindowResizeListener(new BrowserWindowResizeListener() {
			/**
			 * @author medzied
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void browserWindowResized(final BrowserWindowResizeEvent event) {
				DashboardEventBus.post(new BrowserResizeEvent());
			}
		});

		setTheme("VTheme");
		
		InitDB.initUsers();
		startApp();
		
		
	}

	public static DashboardEventBus getDashboardEventbus() {
		return ((VUI) getCurrent()).dashboardEventbus;
	}

	@Subscribe
	public void MapViewVisit(final MapViewVisited event) {
		if(!mapViewVisited) {
			//container.addWindow(new WelcomeWindow(),"WELCOME");
			mapViewVisited = true;
		}
	}
	
	@Subscribe
	public void userLoggedOut(final UserLoggedOutEvent event) {
		VaadinSession.getCurrent().close();
		Page.getCurrent().reload();
	}

	@Subscribe
	public void userLoginRequested(final UserLoginRequestedEvent event) {
		UserE usr = new UserC().fromDB().auth(event.getUserName(),event.getPassword()) ;
		if(usr != null) {
			VaadinSession.getCurrent().setAttribute("usr", event.getUserName());
			VaadinSession.getCurrent().setAttribute("stereotype",usr.getDecriminatorValue());
			startApp();
		}else {
			Notification notification = new Notification(
					CResourceBundle.getLocal("wrongCredentials"),Notification.TYPE_ERROR_MESSAGE);
			notification.setDescription(CResourceBundle.getLocal("wrongCredentialsDescription")) ;
			notification.setHtmlContentAllowed(true);
	        notification.setPosition(Position.MIDDLE_CENTER);
	        notification.show(Page.getCurrent());
			//DashboardEventBus.post(new WrongCredentialsEvent());
		}
		
	}

	public void startApp() {
		String user = (String) VaadinSession.getCurrent().getAttribute("usr");
		
		if (user != null) {
			if(VaadinSession.getCurrent().getAttribute("stereotype").equals("ADMIN")) {
				setContent(new SwitcherView());
			}else if(VaadinSession.getCurrent().getAttribute("stereotype").equals("MANAGER")) {
				setContent(new MainView());
			}else {
				setContent(new MainView());
			}
			removeStyleName("loginview");
		} else {
			setContent(new LoginView());
			addStyleName("loginview");
		}
	}

    //@WebServlet(urlPatterns = {"/ui/*","/VAADIN/*"}, name = "VUIServlet", asyncSupported = true)
    @WebServlet(urlPatterns = {"/*"}, name = "VUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = VUI.class, productionMode = false)
    public static class VUIServlet extends VaadinServlet {
    }
}
