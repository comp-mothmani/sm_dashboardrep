// Define the namespace
var vkonvalib = vkonvalib || {};

vkonvalib.VKonva = function(element) {
    element.innerHTML = "<div id=\"vkcontainer\" ></div>";

    // Style it
    element.style.border = "thin solid red";
    //element.style.display = "inline-block";
    element.style.width = "1000px";
    element.style.height = "400px";


    var width = "1000";
    var height = "400";

	/*
		INIT KONVA
	*/
    stage = new Konva.Stage({
        container: 'vkcontainer',
        width: width,
        height: height
    });

    layer = new Konva.Layer();
    stage.add(layer);

	/*
		ADD IMAGE
	*/
    this.addImage = function() {

        function drawImage(imageObj) {

            var darthVaderImg = new Konva.Image({
                image: imageObj,
                x: stage.width() / 2,
                y: stage.height() / 2,

                draggable: true
            });

            // add cursor styling
            darthVaderImg.on('mouseover', function() {
                document.body.style.cursor = 'pointer';
            });
            darthVaderImg.on('mouseout', function() {
                document.body.style.cursor = 'default';
            });

            layer.add(darthVaderImg);
            stage.add(layer);
        }
        var imageObj = new Image();
        imageObj.onload = function() {
            drawImage(this);
        };

        imageObj.src = 'https://www.limetorrents.info/static/images/verified16.png';
        var MAX_WIDTH = 200;
        // create new transformer
        var tr = new Konva.Transformer({
            boundBoxFunc: function(oldBoundBox, newBoundBox) {

                if (Math.abs(newBoundBox.width) > MAX_WIDTH) {
                    return oldBoundBox;
                }

                return newBoundBox;
            }
        });
        layer.add(tr);

    }


	this.addImage2 = function(img) {

        function drawImage(imageObj) {

            var darthVaderImg = new Konva.Image({
                image: imageObj,
                x: stage.width() / 2,
                y: stage.height() / 2,

                draggable: true
            });

            // add cursor styling
            darthVaderImg.on('mouseover', function() {
                document.body.style.cursor = 'pointer';
            });
            darthVaderImg.on('mouseout', function() {
                document.body.style.cursor = 'default';
            });

            layer.add(darthVaderImg);
            stage.add(layer);
        }
        var imageObj = new Image();
        imageObj.onload = function() {
            drawImage(this);
        };

        imageObj.src = img;
        var MAX_WIDTH = 200;
        // create new transformer
        var tr = new Konva.Transformer({
            boundBoxFunc: function(oldBoundBox, newBoundBox) {

                if (Math.abs(newBoundBox.width) > MAX_WIDTH) {
                    return oldBoundBox;
                }

                return newBoundBox;
            }
        });
        layer.add(tr);

    }


    this.render2 = function() {

        var dataURL = stage.toDataURL({
            pixelRatio: 3
        });
        this.downloadURI(dataURL, 'stage.png');
    }

    this.downloadURI = function(uri, name) {
        var link = document.createElement('a');
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        delete link;
    }
	
    this.render = function() {
        var rect = new Konva.Rect({
            x: 0,
            y: 0,
            width: 100,
            height: 90,
            fill: 'red',
            name: 'rect',
            stroke: 'black',
            draggable: true
        });
        layer.add(rect);

        var MAX_WIDTH = 200;
        // create new transformer
        var tr = new Konva.Transformer({
            boundBoxFunc: function(oldBoundBox, newBoundBox) {
                // "boundBox" is an object with
                // x, y, width, height and rotation properties
                // transformer tool will try to fit node into that box
                // "width" property here is a visible width of the object
                // so it equals to rect.width() * rect.scaleX()

                // the logic is simple, if new width is too big
                // we will return previous state
                if (Math.abs(newBoundBox.width) > MAX_WIDTH) {
                    return oldBoundBox;
                }

                return newBoundBox;
            }
        });
        layer.add(tr);
        tr.attachTo(rect);
        layer.draw();

    }
    // Getter and setter for the value property
    this.getValue = function() {
        return element.
        getElementsByTagName("input")[0].value;
    };
    this.setValue = function(value) {
        element.getElementsByTagName("input")[0].value =
            value;
    };

    // Default implementation of the click handler
    this.click = function() {
        alert("Error: Must implement click() method");
    };


};